<?php
namespace misd\security;

# imports
require_once 'inc/misc-functions.php';
use inc\data\UserDAO;
use inc\models\UserModel;
use misd\data\MySqlCriterion;
use misd\data\MySqlOperator;
use misd\data\QueryObject;
use misd\web\Controller;
use inc\security\CurrentUser;

/**
 * A security service class used for authentication (logging in), logging out,
 * and checking that the current user is logged in.
 * @author Michael Mason
 * @copyright 2019 Mason Innovative Software Design
 */
class SecurityService
{
    // CONSTANTS
    private const PRINCIPAL = 'principal';
    private const ADMIN_ROLE = 1; // <- change this value to whatever value your admin role is
    
    // INSTANCE VARIABLES
    private $username, $password, $conn;
    
    // CONSTRUCTOR
    public function __construct(string $username, string $password)
    {        
        // first, sanitize the user input [SANITIZE IN SQL CLASS]
        //$username = InputSanitizer::sanitizeMySQL($this->conn, $username);
        //$password = InputSanitizer::sanitizeMySQL($this->conn, $password);
        
        // store the user input as the service's private variables
        $this->username = $username;
        $this->password = $password;
    }
    
    public function __destruct()
    {
        // close any open connections to the database
        if ($this->conn != null)
            $this->conn->close();
    }
    
    /**
     * @todo Modify this function to check the MySQL database for username and password.
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return boolean
     */
    public function login() {
        
        // premature exit -- both un & pw must be set
        if (empty($this->username) || empty($this->password))
            return false;
        
        // TODO: Modify this to use hashing and salting to check password
            
        // create a new query object
        $query = new QueryObject(new UserModel());
        $query->addCriterion("username", MySqlOperator::EQUALS(), $this->username);
        $query->addCriterion("password", MySqlOperator::EQUALS(), $this->password);
        
        $dao = new UserDAO;
        $result = $dao->find($query);
        
        // check to if a user was returned
        if (!empty($result) && count($result) == 1)
        {
            // get the single user from the array
            /** @var $user UserModel */
            $user = $result[0];
            
            // initiate a user session
            $_SESSION[self::PRINCIPAL] = new CurrentUser($user);
            return true;
        }
        else
        {
            // no such user found
            $_SESSION[self::PRINCIPAL] = false;
        }
        
        // problem authenticating
        return false;
    }
    
    /**
     * Checks the $_SESSSION['principal'] variable to determine whether or not the
     * currrent user is logged in
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return boolean Indicating whether or not the current user is logged in.
     */
    static function isCurrUserLoggedIn()
    {
        if (!isset($_SESSION[self::PRINCIPAL]) || 
            !($_SESSION[self::PRINCIPAL] instanceof CurrentUser)
        )
            return false;
        return true;
    }
    
    static function isCurrUserAdmin()
    {
        if (self::isCurrUserLoggedIn())
        {
            /** @var $user CurrentUser */
            $user = $_SESSION[self::PRINCIPAL];
            if ($user->getUserRole() === self::ADMIN_ROLE)
                return true;
        }
        return false;
    }
    
    /**
     * Returns the current session's currently logged in user,
     * if applicable
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return CurrentUserInterface
     */
    static function getCurrentUser() : CurrentUserInterface
    {
        if (
            isset($_SESSION[self::PRINCIPAL]) &&
            ($_SESSION[self::PRINCIPAL] instanceof CurrentUserInterface)
        )
        {
            return $_SESSION[self::PRINCIPAL];
        }
        return false;
    }
    
    /**
     * Erases all session variables, destroys the session cookie set by the server as well as the session itself,
     * then redirects to the home page.
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return void
     */
    static function logout()
    {
        //session_start();
        $_SESSION = array();
        setcookie(session_name(), '', time() - 2592000, '/');
        session_destroy();
        Controller::redirect('index');
    }
}

