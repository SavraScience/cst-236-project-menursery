<?php
namespace misd\security;

interface CurrentUserInterface
{
    function getUserId();
    function getUsername() : string;
    function getFirstName() : string;
    function getLastName() : string;
    function getUserRole();
}

