<?php
namespace misd\data;

use Exception;
use misd\models\AbstractObjectModel;

// imports
require_once 'autoloader.php';

// TODO: Remove all debugging statements
// TODO: Should this class be renamed to MySqlDao? 

/**
 * A master DAO class for automatically resolving
 * class properties to database table fields given
 * that the DAO's DataTranslator's DataTableMappings
 * are set
 * @author Michael Mason
 * @copyright 2019 Mason Innovative Software Design
 */
abstract class Dao implements DaoInterface
{
    // INSTANCE VARIABLES
    /** @var $conn \mysqli */
    private $conn;              // Database Connection
    private $tableName;         // Target Table
    private $dataTranslator;    /* Used to translate a resultset to 
                                zero, one or more Model objects */
    // CONSTRUCTOR & DESTRUCTOR
    /**
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $tableName The name of the table in
     * the database that corresponds to the class/entity
     * you wish to perform CRUD operations on
     */
    public function __construct(string $tableName, AbstractObjectModel $objectTemplate = null)
    {
        // instantiate variables
        $this->tableName = $tableName;
        
        // pass an object template to the DataTranslator for object<->row translation
        $this->dataTranslator = new DataTranslator($objectTemplate);

        // get a new connection from the database
        $this->conn = DatabaseManager::getConnection();

        if (DataMapRegistry::hasMapForTable($this->tableName))
        {
            // reference the table already in memory
            //console_log("Table '$this->tableName' already in memory...");
            $this->setTableMetadata(DataMapRegistry::getMapForTable($this->tableName));
        }
        else
        {
            // probe the database table
            //console_log("Probing table '$tableName'...");
            $this->probeTable();
        }
        
        // map class properties to table fields
        $this->setMapProperties($this->dataTranslator);
    }
    
    public function __destruct()
    {
        // close any open connections
        $this->conn->close();
    }

    // ABSTRACT METHODS
    /**
     * This method will need to be overridden in its
     * inheriting classes to map its model object's
     * properties to the class's corresponding database
     * table fields.
     * 
     * This method used to be abstract when the class
     * declaration for Dao was abstract, but now it is
     * just an empty stub for child DAO objects to
     * override
     * 
     * E.g. TODO: Need example here
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     */
    abstract protected function setMapProperties(DataTranslator &$translator);
    
    // PUBLIC METHODS
    /**
     * Enables/disables "transaction mode," which disables
     * the connection's autocommit feature when set to true.
     * If transaction mode is disabled (i.e. false), the
     * connections autocommit mode is re-enabled.
     *
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param bool $mode When true, transaction mode is enabled,
     * meaning autocommit is turned off.
     */
    public function transactionMode(bool $mode)
    {
        DatabaseManager::persistConnection($this->conn);
        $this->conn->autocommit(!$mode);
    }
    
    /**
     * A wrapper for this DAO's connection's
     * begin_transaction() method
     *
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     */
    public function beginTransaction()
    {
        $this->conn->begin_transaction();
    }
    
    /**
     * A wrapper for this DAO's connection's
     * commit() method
     *
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     */
    public function commit()
    {
        $this->conn->commit();
    }
    
    /**
     * A wrapper for this DAO's connection's
     * rollback() method
     *      
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     */
    public function rollback()
    {
        $this->conn->rollback();
    }
    
    /**
     * Merges the connection from multiple data services into
     * a single, master connection
     *
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param Dao ...$dataServices
     */
    public function mergeConnections(Dao ...$dataServices)
    {
        // make the first connection the master
        $masterConnection = $dataServices[0]->conn;
        
        // converge all other connections to the master
        for ($i = 1; $i < count($dataServices); $i++)
        {
            // close the current connection for safety
            $dataServices[$i]->conn->close();
            
            // change current connection to be the master
            $dataServices[$i]->conn = $masterConnection;
        }
    }
    
    /**
     * (non-PHPdoc)
     *
     * @see \misd\data\DaoInterface::findAll()
     */
    public function findAll()
    {
        // build SQL string
        $sql = <<<ML
            SELECT *
            FROM $this->tableName
            ;
ML;
        // debugging
        //console_log($sql);
        
        DatabaseManager::persistConnection($this->conn);
        
        // check to see if anything was returned
        $result = $this->conn->query($sql);
        if (!empty($result) && $result->num_rows > 0)
        {
            // records found
            //console_log("Returning " . $result->num_rows . " rows...");
            return $result->fetch_all(MYSQLI_ASSOC);
        }
        else
        {
            // no records found
            //console_log("No records found...");
            return null;
        }
        
        // problem authenticating
        $result->close();
        
    }

    /**
     * (non-PHPdoc)
     *
     * @see \misd\data\DaoInterface::findById()
     */
    public function findById($id)
    {
        // get the table's PRIMARY KEY
        $primKey = $this->getDataTranslator()->getDataTableMetadata()->getSurrogatePK();
        
        if (!is_null($primKey))
        {
            // build SQL statement
            $sql = <<<ML
                SELECT *
                FROM $this->tableName
                WHERE $primKey = ?
                ;
ML;
            // debugging
            //console_log("Executing SQL:\n$sql");
            
            DatabaseManager::persistConnection($this->conn);
            
            // create a prepared statement
            $stmt = $this->conn->prepare($sql);
            $stmt->bind_param("i", $id);
            $stmt->execute();
            
            // check to see if anything was returned
            $result = $stmt->get_result();
            if ($result->num_rows == 1)
            {
                // record found
                //console_log("Record found!");
                return $result->fetch_all(MYSQLI_ASSOC)[0];
            }
            elseif ($result->num_rows > 1)
            {
                // duplicate entry found
                //console_log("Duplicate records found!");
            }
            else
            {
                // no records found
                //console_log("No records found...");
            }
        }
        
        // close results & statement
        $result->close();
        $stmt->close();
        return null;
    }

    /**
     * {@inheritDoc}
     * @see \misd\data\DaoInterface::findIdViaDescriptor()
     */
    public function findIdViaDescriptor(string $descriptorField, string $description, bool $exactMatchOnly = true)
    {
        console_log("Looking for value '$description' in field '$descriptorField'...");
        
        /* retrieve the surrogate primary key
        for the this DAO's table */
        $pkField = $this->getTableMetadata()->getSurrogatePK();
        
        if (!is_null($pkField))
        {
            // determine operator to use in SQL Statement
            // WARNING: This is a specific implementation of MySQL
            $operator = "";
            if ($exactMatchOnly)
            {
                $operator = "=";
            }
            else
            {
                $operator = "LIKE";
            }
            
            // build SQL Statement
            $sql = <<<ML
                SELECT `$pkField`
                FROM `$this->tableName`
                WHERE `$descriptorField` $operator ?
                ;
ML;
            // debugging
            console_log("SQL Statement\n:$sql");
            
            try
            {
                DatabaseManager::persistConnection($this->conn);
                
                // append wildcards for LIKE comparison
                if (!$exactMatchOnly) $description = "%" . $description . "%";
                
                // create a prepared statement
                $stmt = $this->conn->prepare($sql);
                $stmt->bind_param("s", $description);
                $stmt->execute();
                
                // check to see if anything was returned
                $result = $stmt->get_result();
                if ($result->num_rows == 1)
                {
                    // record found
                    //console_log("Record found!");
                    return $result->fetch_all(MYSQLI_NUM)[0];
                }
                elseif ($result->num_rows > 1)
                {
                    // multiple entries found
                    //console_log("Multiple records found!");
                    
                    // TODO: Currently only returns 1 row where many found...: 
                    // Change implementation to return a list of INTs
                    // e.g. IN (1, 3, 6)
                    return $result->fetch_all(MYSQLI_NUM)[0];
                }
                else
                {
                    // no records found
                    //console_log("No records found...");
                }
            }
            catch (Exception $ex)
            {
                var_dump($ex);
            }
            finally
            {
                try
                {
                    $stmt->close();
                }
                catch (Exception $ex)
                {
                    var_dump($ex);
                }
            }
        }
        
        return false;
    }
    
    /**
     * {@inheritDoc}
     * @see \misd\data\DaoInterface::findLast()
     */
    public function findLast()
    {
        // get primary key
        $pkField = $this->getTableMetadata()->getSurrogatePK();
        
        // build SQL string
        $sql = <<<ML
            SELECT *
            FROM $this->tableName
            ORDER BY $pkField DESC
            LIMIT 1
            ;
ML;
        //console_log($sql);
        DatabaseManager::persistConnection($this->conn);
        
        // check to see if anything was returned
        $result = $this->conn->query($sql);
        if (!empty($result) && $result->num_rows == 1)
        {
            // records found
            //console_log($result->num_rows . " found!");
            return $result->fetch_all(MYSQLI_ASSOC)[0];
        }
        else
        {
            // no records found
            //console_log("No records found...");
            return null;
        }
        
        // problem authenticating
        $result->close();
    }
    
    /**
     * (non-PHPdoc)
     *
     * @see \misd\data\DaoInterface::insert()
     */
    public function insert($object) : int
    {   
        // initialize values
        $propVals = null;
        $fieldVals = null;
        
        // perform some validation checks, first, then 
        if (is_object($object))
        {
            if ($this->dataTranslator->hasObjectTemplate() && $this->dataTranslator->hasDataMap())
            {
                // retrieve all of the object's properties and their values
                $propVals = $this->dataTranslator->getPropertyValueArray($object);
                
                // debugging
                //console_log("Property/Values array:\n" . array_toString($propVals));
                
                // resolve properties to field names
                $fieldVals = $this->dataTranslator->translatePropertyValueArray($propVals);
                
                // debugging
                //console_log("Field/Values array:\n" . array_toString($fieldVals));
        
                // build SQL string
                $sql = $this->dataTranslator->translateObjectToSqlInsertUpdate($object, $fieldVals);
                    
                if (!empty($sql))
                {
                    // debugging
                    //console_log("Executing SQL command: \n$sql");
                    
                    DatabaseManager::persistConnection($this->conn);
            
                    // resolve binding parameter types and values
                    $types = $this->getObjectBindParamTypes($fieldVals);
                    
                    // build values array (ditch the keys in order to "unpack")
                    $values = array();
                    foreach($fieldVals as $data)
                    {
                        array_push($values, $data[DataTranslator::FLDVAL_ARRY_VAL]);
                    }
                    
                    //$values = array_values($fieldVals); // ditch the keys in order to "unpack"
                    
                    // debugging
                    //console_log("Passing \$types = $types to 1st bind_param argument...");
                    //console_log("Binding values " . array_toString($values) . " to the parameterized query...");
                    
                    // prepare query, bind values, and execute
                    $stmt = $this->conn->prepare($sql);
                    
                    try
                    {
                        $newId = 0;
                        $stmt->bind_param($types, ...$values);
                        
                        // optional BLOB handling
                        if (strpos($types, "b") !== false)
                        {
                            // attempt to upload/set BLOB types
                            $i = 0;
                            foreach($fieldVals as $data)
                            {
                                if (
                                    (isset($data[DataTranslator::FLDVAL_ARRY_PARAMTYPE])) && 
                                    ($data[DataTranslator::FLDVAL_ARRY_PARAMTYPE] == "b")
                                )
                                {
                                    $stmt->send_long_data($i, $data[DataTranslator::FLDVAL_ARRY_VAL]);
                                }
                                $i++;
                            }
                        }
                        
                        if ($stmt->execute())
                        {
                            // insertion successful
                            $newId = $stmt->insert_id;
                            
                            //console_log("Insert successful -> New row ID = $newId");
                        }
                        else
                        {
                            // insertion failed
                            //console_log("Insert failed");
                            //console_log($stmt->error);
                        }
                        
                        // cleanup
                        $stmt->close();
                        
                        return $newId;
                    }
                    catch (Exception $e)
                    {
                        console_log("Exception! $e");
                    }
                }
                else
                {
                    throw new Exception("Cannot insert new record. SQL Statement is empty.");
                }
            }
            else
            {
                throw new Exception("Cannot insert new record. DataTranslator is missing an object template or data map.");
            }
        }
        else
        {
            throw new Exception("Invalid argument for insert().  Must be an object.");
        }
        
        return 0;
    }

    /**
     * (non-PHPdoc)
     *
     * @see \misd\data\DaoInterface::update()
     */
    public function update($object) : bool
    {
        // initialize values
        $propVals = null;
        $fieldVals = null;
        
        // perform some validation checks, first, then
        if (is_object($object))
        {
            if ($this->dataTranslator->hasObjectTemplate() && $this->dataTranslator->hasDataMap())
            {
                // retrieve all of the object's properties and their values, but exclude the id property
                $propVals = $this->dataTranslator->getPropertyValueArray($object);
                
                // debugging
                //console_log("Property/Values array:\n" . array_toString($propVals));
                
                // resolve properties to field names
                $fieldVals = $this->dataTranslator->translatePropertyValueArray($propVals);
                
                // debugging
                //console_log("Field/Values array:\n" . array_toString($fieldVals));
                
                // get primary key field & id
                $pkField = $this->getTableMetadata()->getSurrogatePK();
                $id = $fieldVals[$pkField][DataTranslator::FLDVAL_ARRY_VAL];
                
                // build SQL string
                $sql = $this->dataTranslator->translateObjectToSqlInsertUpdate($object, $fieldVals, DataTranslator::SQL_UPDATE);
                
                if (!empty($sql))
                {
                    // debugging
                    //console_log("Executing SQL command: \n$sql");
                    
                    try
                    {
                        DatabaseManager::persistConnection($this->conn);
                        
                        // resolve binding parameter types (and add an "i" at the end 
                        // for the last integer to represent the value of the primary key field)
                        $types = $this->getObjectBindParamTypes($fieldVals) . "i";
                        
                        // build values array (ditch the keys in order to "unpack")
                        $values = array();
                        foreach($fieldVals as $data)
                        {
                            array_push($values, $data[DataTranslator::FLDVAL_ARRY_VAL]);
                        }
                        
                        // finally, add the primary key at the end of the values array
                        array_push($values, $id);
                        
                        // debugging
                        //console_log("Passing \$types = $types to 1st bind_param argument...");
                        //console_log("Binding values " . array_toString($values) . " to the parameterized query...");
                        
                        // prepare query, bind values, and execute
                        $stmt = $this->conn->prepare($sql);
                        $stmt->bind_param($types, ...$values);
                        
                        // optional BLOB handling
                        if (strpos($types, "b") !== false)
                        {
                            // attempt to upload/set BLOB types
                            $i = 0;
                            foreach($fieldVals as $data)
                            {
                                if ($data[DataTranslator::FLDVAL_ARRY_PARAMTYPE] == "b")
                                {
                                    $stmt->send_long_data($i, $data[DataTranslator::FLDVAL_ARRY_VAL]);
                                }
                                $i++;
                            }
                        }
                        
                        if ($stmt->execute())
                        {
                            // update successful
                            //console_log("Update successful");
                            return true;
                        }
                            
                        // insertion failed
                        //console_log("Update failed");
                        return false;
                    }
                    catch (Exception $e)
                    {
                        console_log("Exception! $e");
                    }
                    finally
                    {
                        if (!is_null($stmt))
                            $stmt->close();
                    }
                }
            }
        }
        return false;
    }
    
    /**
     * (non-PHPdoc)
     *
     * @see \misd\data\DaoInterface::deleteById()
     */
    public function deleteById($id) : bool
    {
        // get the table's PRIMARY KEY
        $primKey = $this->getDataTranslator()->getDataTableMetadata()->getSurrogatePK();
        
        if (!is_null($primKey))
        {
            // build SQL statement
            $sql = <<<ML
                DELETE
                FROM $this->tableName
                WHERE $primKey = ?
                ;
ML;
            try
            {
                DatabaseManager::persistConnection($this->conn);
                
                // create a prepared statement
                $stmt = $this->conn->prepare($sql);
                $stmt->bind_param("i", $id);
                $result = $stmt->execute();
                if ($result)
                {
                    //console_log("Record belonging to ID #$id successfully deleted...");
                    return $result;
                }
                //console_log("Something went wrong... Record could NOT be deleted.");
                return $result;
            }
            catch (Exception $ex)
            {
                print($ex->getMessage());
                return false;
            }
            finally
            {
                // close statement
                $stmt->close();
            }
        }
    }
    
    public function find($queryObject)
    {   
        /** @var $queryObject QueryObject */
        if ($queryObject instanceof QueryObject)
        {
            // get all of the criteria and resolve foreign key values as necessary
            $criteria = $queryObject->getCriteria();
            
            // TODO: Fix the flattenCriteriaValues() method before uncommenting
            //console_log("Querying table `$this->tableName` with " . count($criteria) . " criteria...");
            $this->dataTranslator->flattenCriteriaValues($criteria);
            $criteria = $queryObject->getCriteria(); // refresh the criteria after flattening
            //console_log("# of Criteria AFTER flattening: " . count($criteria));
            
            // TODO: Check to see that criteria is passed by reference properly
            
            // build SQL string
            $whereClause = $this->generateWhereClause($criteria);
            $sql = <<< ML
                SELECT *
                FROM $this->tableName
                WHERE $whereClause
                ;
ML;         
            // debugging
            //console_log("SQL Statement:\n$sql");
            
            try 
            {
                DatabaseManager::persistConnection($this->conn);
                
                // resolve binding parameter types and values from QueryObject criteria
                $types = $this->getCriteriaBindParamTypes($criteria);
                $values = $this->getCriteriaValues($criteria);
                
                // debugging
                //console_log("Passing \$types = $types to 1st bind_param argument...");
                //console_log("Binding values " . array_toString($values) . " to the parameterized query...");
                
                // prepare query, bind values, and execute
                $stmt = $this->conn->prepare($sql);
                $stmt->bind_param($types, ...$values);
                $stmt->execute();
                
                // check to see if anything was returned
                $result = $stmt->get_result();
                if ($result->num_rows > 0)
                {
                    // records found
                    //console_log($result->num_rows . " records found!");
                    return $result->fetch_all(MYSQLI_ASSOC);
                }
                else
                {
                    // no records found
                    //console_log("No records found...");
                }    
            }
            catch (Exception $ex)
            {
                var_dump($ex);
            }
            finally 
            {
                // temporarily suppress warnings
                $origErrReport = error_reporting();
                error_reporting(E_ERROR | E_WARNING);
                
                /*
                if (!is_null($result))
                {
                    try 
                    {
                        $result->close();
                    } 
                    catch (Exception $ex) 
                    {
                        //var_dump($ex);
                    }
                    finally
                    {
                        //error_reporting($origErrReport);
                    }
                }
                */
                
                // attempt to close the result, if needed
                if (!is_null($result))
                {
                    try
                    {
                        $result->close();
                    }
                    catch (Exception $ex)
                    {
                        //var_dump($ex);
                    }
                    finally
                    {
                        error_reporting($origErrReport);
                    }
                }
                
                // attempt to close the statement, if needed
                error_reporting(E_ERROR | E_WARNING);
                if (!is_null($stmt))
                {
                    try
                    {
                        $stmt->close();
                    }
                    catch (Exception $ex)
                    {
                        //var_dump($ex);
                    }
                    finally
                    {
                        error_reporting($origErrReport);
                    }
                }
            }
        }
        return null;
    }
    
    /**
     * Returns metadata about the DAO's underlying table
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return DataTableMetadata
     */
    public function getTableMetadata() : DataTableMetadata
    {
        return $this->dataTranslator->getDataTableMetadata();
    }
    
    /**
     * Sets the DAO's underlying table metadata
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param DataTableMetadata $metadata
     */
    public function setTableMetadata(DataTableMetadata $metadata)
    {
        $this->dataTranslator->setDataTableMetadata($metadata);
    }
    
    // PROTECTED METHODS
    
    /**
     * Sets the model object for the DataTranslator object
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param object $modelObject The object you want the
     * DataTranslator to be based off of
     */
    protected function setModel($modelObject)
    {
        $this->dataTranslator = new DataTranslator($modelObject);
    }
    
    /**
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return \misd\data\DataTranslator|NULL The
     * DataTranslator object by which you map object
     * properties to database table fields and return
     * instances of the model when DML queries are run
     */
    protected function &getDataTranslator()
    {
        return $this->dataTranslator;
    }
    
    /**
     *
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param array $row
     * @return object An instance of an object model
     * as represented by the database
     */
    protected function instantiateObject(array $row) : AbstractObjectModel
    {
        return $this->dataTranslator->translateRow($row);
    }
    
    /**
     * Takes a mysqli result set (array) as an argument
     * and, for each row, instantiates a new Object
     * and places it in an array of AbstractObjectModels.  
     * If the passed-in recordset is empty, this function
     * returns null.
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param array $recordset A result set (array) of
     * table rows, each which is itself an (associative)
     * array of field names (keys) and values
     * @return AbstractObjectModel[]|NULL
     */
    protected function instantiateObjects($recordset)
    {
        if (!empty($recordset))
        {
            // create an array of object models
            $objects = array();
            
            foreach($recordset as $row)
            {
                array_push($objects, $this->instantiateObject($row));
            }
            
            return $objects;
        }
        else
        {
            return null;
        }
    }
    
    // TODO: Refactor this to be more efficient with array iterations
    // E.g. one loop for all the info you need
    private function probeTable()
    {
        DatabaseManager::persistConnection($this->conn);
        
        // get DataTranslator's metadata object
        $metadata = &$this->dataTranslator->getDataTableMetadata();
        $colMeta = $this->getTableColumnMetaData();
        
        $metadata->setTableName($this->tableName);
        
        // get/set Field Types
        $fieldTypes = $this->getFieldTypes($colMeta);
        //console_log("Field Types: \n------------\n" . array_toString($fieldTypes));
        $metadata->setFieldTypes($fieldTypes);
        
        // get/set Required Fields
        $reqFields = $this->getRequiredFields($colMeta);
        //console_log("Required Fields: \n----------------\n" . array_toString($reqFields));
        $metadata->setRequired($reqFields);
        
        // get/set Defaults
        $defaults = $this->getFieldDefaults($colMeta);
        //console_log("Field Defaults: \n---------------\n" . array_toString($defaults));
        $metadata->setDefaults($defaults);
        
        // get/set Collations
        $collations = $this->getFieldCollations($colMeta);
        //console_log("Collations: \n-----------\n" . array_toString($collations));
        $metadata->setCollations($collations);
        
        // scan for descriptor (CHAR/VARCHAR) fields
        $descriptors = array();
        foreach ($metadata->getFieldTypes() as $field => $type)
        {
            if (stripos($type, "char") !== FALSE)
            {
                /* TODO: Consider passing assoc. array of
                 * field names and their max character lengths */
                array_push($descriptors, $field);
            }
        }
        //console_log("Descriptors: \n-----------\n" . array_toString($descriptors));
        $metadata->setDescriptors($descriptors);
        
        // aggregate all keys
        $keys = &$this->getDataTranslator()->getDataTableMetadata()->getKeys();
        $keys[DataTableMetadata::PRIMARY_KEY] = $this->getPrimaryKeys($colMeta);
        $keys[DataTableMetadata::UNIQUE_KEY] = $this->getUniqueKeys($colMeta);
        $keys[DataTableMetadata::FOREIGN_KEY] = $this->getForeignKeys();

        // debugging
        //console_log("Keys: \n-----\n" . array_toString($this->getDataTranslator()->getDataTableMetadata()->getKeys()));
        //console_log("The $this->tableName's primary key is " . $this->dataTranslator->getDataTableMetadata()->getSurrogatePK());

        // register the new map
        DataMapRegistry::registerMap($metadata);
    }
    
    // PRIVATE METHODS
    
    /* TODO: How to check for BLOB types?  Also, will
       dates be resolved to their proper types, too? 
    */
    /**
     * Resolves any value to one of the four MySqli Type
     * specification characters ("i", "d", "s", or "b")
     * 
     * Strings will be resolved to "s"
     * Doubles will be resolved to "d"
     * Integers will be resolved to "i"
     * Blob-types will be resolved to "b"
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param mixed $value The value you want to determine
     * @return string
     */
    private function resolveBindParamType($value) : string
    {
        // initialize variables
        $paramType = "";
        if (is_int($value))
        {
            $paramType = "i";
        }
        elseif (is_double($value))
        {
            $paramType = "d";
        }
        elseif (is_string($value))
        {
            $paramType = "s";
        }
        else
        {
            // default -- for date types and others
            $paramType = "s";
        }
        
        // debugging
        //console_log("Parameter Type for value $value resolved to '$paramType'");
        
        return $paramType;
    }
    
    /**
     * Returns a single string of mysqli types for
     * binding values to mysqli prepared statements
     * from an array of objects that implement 
     * CriterionInterface
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param CriterionInterface[] $criteria An array of
     * objects that implement CriterionInterface. Each
     * criterion will have a value of an unknown type to
     * be resolved to a mysqli "type" string
     * @return string A single string of mysqli types for use
     * in binding values to mysqli prepared statements
     */
    private function getCriteriaBindParamTypes(array $criteria) : string
    {
        $typesString = "";
        foreach($criteria as $criterion)
        {
            $val = $criterion->getCriterionValue();
            $typesString .= $this->resolveBindParamType($val);
        }
        return $typesString;
    }
    
    /**
     * Returns an array of JUST the values in an array of
     * criteria that implements CriterionInterface. Useful
     * when combining this method with the mysqli::bind_param()
     * function and "array unpacking" is used as its 2nd argument
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param CriterionInterface[] $criteria An array of
     * objects that implement CriterionInterface.
     * @return array An array of values of unknown type
     */
    private function getCriteriaValues(array $criteria) : array
    {
        $values = array();
        
        // debugging
        //console_log("Grabbing criteria values...");
        
        foreach($criteria as $criterion)
        {   
            /** @var $criterion \misd\data\CriterionInterface */
            // TODO: Previously removed $this->sqlEncaseValue() -- do I need to re-add it? || Answer: Only if not parameterized
            $val = $criterion->getCriterionValue();
            $operator = $criterion->getOperator();
            
            // debugging
            //console_log("Criterion operator = $operator");
            
            if ($operator == MySqlOperator::LIKE())
            {
                // Add Wildcard characters
                $wildcard = MySqlOperator::WILDCARD();
                $val = $wildcard . $val . $wildcard;
            }
            
            // debugging
            //console_log("Adding $val to the array...");

            array_push($values, $val);
        }
        return $values;
    }
    
    /**
     * Returns a single string of mysqli types for
     * binding values to mysqli prepared statements
     * from an associative array of field/value pairs
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param array $objFieldValArray An associative array
     * of table fields (key) and their respective values as
     * translated from a property/value array
     * @return string
     */
    private function getObjectBindParamTypes(array $objFieldValArray) : string
    {
        // declarations
        $typesString = "";
        
        foreach ($objFieldValArray as $field => $data)
        {
            // declare variables
            $type = "";
            $paramKey = DataTranslator::FLDVAL_ARRY_PARAMTYPE;
            
            // debugging
            //console_log("$field's data is of type " . gettype($data));
            
            // get field's bind param type, if available
            if (array_key_exists($paramKey, $data))
            {   
                $type = $data[$paramKey];
                
                // debugging
                //console_log("$field's bind param type was previously set to '$type'");
            }
            else
            {                
                // resolve field's bind param type using its value
                $val = $data[DataTranslator::FLDVAL_ARRY_VAL];
                $type = $this->resolveBindParamType($val);
                
                // debugging
                //console_log("$field's bind param type resolved to '$type'");
            }
            
            $typesString .= $type;
        }
        return $typesString;
    }
    
    /* REFLECTIVE DATABASE FUNCTIONS */
    
    // TODO: Consider refactoring this, as each call from a function that utilizes
    // a foreach() multiplies the number of calls that need to be made to the database
    /**
     * Returns all of the associated table's column metadata (properties) and their
     * values, or when $columnFilter is used, just the values of a particular 
     * property for all columns in the table
     * 
     * E.g. If the value of each column's data type is desired, the string "Type" can 
     * be passed into the function via the $columnFilter argument, and the function
     * will return an associative array in which the key represents the name of the
     * table's column and the value represents the data type of that column as
     * returned by the MySQL resultset
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $columnFilter The name of the specific column property you want
     * to filter the results by
     * @return array|NULL An array of associative arrays representing each table field's
     * column properties, or when the $columnFilter argument is used, returns
     * an associative array in which the key represents the name of the table column,
     * and its value is equal to the value of the column property as specified by
     * the $columnFilter
     */
    private function getTableColumnMetaData(string $columnFilter = null)
    {
        // CONSTANTS
        $FIELD_COL = "Field";
        
        // build SQL Statement
        $sql = "SHOW FULL COLUMNS FROM $this->tableName;";
        
        // refresh connection, if necessary
        DatabaseManager::persistConnection($this->conn);
        
        // run query
        $result = $this->conn->query($sql);
        if (!empty($result) && $result->num_rows > 0)
        {
            // records found
            //console_log("Query executed successfully...");
            $recordset = $result->fetch_all(MYSQLI_ASSOC);
            
            if (empty($columnFilter))
                
                // return the entire recordset, unfiltered (array of arrays)
                return $recordset;
            
            else
            {
                $columnValues = array();
                foreach($recordset as $row)
                {
                    // store the field's column value inside an associative array
                    $field = $row[$FIELD_COL];
                    $columnVal = $row[$columnFilter];
                    
                    // debugging
                    //console_log("Storing $columnFilter value '$columnVal' inside array key '$field'");
                    
                    $columnValues[$field] = $columnVal;
                }
                
                return $columnValues;
            }
        }
        else
        {
            // no records found
            console_log("Something went wrong...");
            return null;
        }
        
        // problem authenticating
        $result->close();
    }
    
    private function getFieldCollations($columnMeta) : array
    {
        // CONSTANTS
        $COLUMN_META = "Collation";
        
        // initialize variables
        $rs = array();
        $collations = array();
        
        // use the data from the source array
        foreach($columnMeta as $row)
        {
            // store the field's column value inside an associative array
            $field = $row["Field"];
            $columnVal = $row[$COLUMN_META];
            
            // debugging
            //console_log("Storing $COLUMN_META value '$columnVal' inside array key '$field'");
            
            $rs[$field] = $columnVal;
        }
        
        // include only rows whose Collation field is not empty
        foreach($rs as $key => $val)
        {
            if ($val != '')
            {
                /* store the key in an array copy along
                 corresponding value */
                //console_log("Storing value '$val' inside array index '$key'");
                $collations[$key] = $val;
            }
            
        }
        return $collations;
    }
    
    /**
     * Returns an associative array of all column fields
     * that have an assigned default value in MySQL
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return array An associative array of all column
     * fields which have an assigned default value
     * (with their name as the key and their default value
     * as the value)
     */
    private function getFieldDefaults($columnMeta) : array
    {
        // CONSTANTS
        $COLUMN_META = "Default";
        
        // DECLARATIONS
        $rs = array();
        $defFields = array();
        
            // use the data from the source array
            foreach($columnMeta as $row)
            {
                // store the field's column value inside an associative array
                $field = $row["Field"];
                $columnVal = $row[$COLUMN_META];
                
                // debugging
                //console_log("Storing $COLUMN_META value '$columnVal' inside array key '$field'");
                
                $rs[$field] = $columnVal;
            }
        
        // exclude rows whose "Default" field is an empty string
        foreach($rs as $key => $val)
        {
            if ($val != '')
            {
                /* store the key in an array copy along 
                corresponding value */
                //console_log("Storing value '$val' inside array index '$key'");
                $defFields[$key] = $val;
            }
                
        }
        return $defFields;
    }

    /**
     * Returns an associative array of all column fields
     * for this DAO's table and their data types
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param $columnMeta array An optional array of data
     * that reduces the number of calls to the database
     * to retrieve the same information.  You should
     * always try to pass in an array that represents
     * a previous call to retrieve column metadata if
     * you can.
     * @return array An associative array of all column
     * fields and their respective data types as returned
     * by MySQL
     */
    private function getFieldTypes($columnMeta) : array
    {
        // CONSTANTS
        $COLUMN_META = "Type";
        
        // DECLARATIONS
        $fieldTypeArray = array();

        // use the data from the source array
        foreach($columnMeta as $row)
        {
            // store the field's column value inside an associative array
            $field = $row["Field"];
            $columnVal = $row[$COLUMN_META];
            
            // debugging
            //console_log("Storing $columnFilter value '$columnVal' inside array key '$field'");
            
            $fieldTypeArray[$field] = $columnVal;
        }
            
        return $fieldTypeArray;
    }

    private function getPrimaryKeys($columnMeta) : array
    {
        //return $this->getTableColumnMetaData($COLUMN_META);
        
        // CONSTANTS
        $COLUMN_META = "Key";
        
        // initialize variables
        $rs = array();
        $primKeys = array();
        
        // use the data from the source array
        foreach($columnMeta as $row)
        {
            // store the field's column value inside an associative array
            $field = $row["Field"];
            $columnVal = $row[$COLUMN_META];
            
            // debugging
            //console_log("Storing $COLUMN_META value '$columnVal' inside array key '$field'");
            
            $rs[$field] = $columnVal;
        }
        
        // filter the keys by "Primary Key"
        foreach ($rs as $key => $val)
        {
            if ($val == 'PRI')
            {
                //console_log("Assigning PRIMARY KEY value '$key' to array index " . sizeof($primKeys));
                array_push($primKeys, $key);
            }
        }
        return $primKeys;
    }
    
    /**
     * Returns an array of foreign key columns from this
     * DAO's database table
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return array|NULL A simple array of columns in this
     * DAO's database table which are foreign keys to 
     * another table
     */
    private function getForeignKeys()
    {
        // CONSTANTS
        $COLNAME = "COLUMN_NAME";
        $FOREIGN_TBL = "REFERENCED_TABLE_NAME";
        $FOREIGN_PK = "REFERENCED_COLUMN_NAME";
        
        $schema = DatabaseManager::getDatabaseName();
        
        // build SQL Statement
        $sql = <<<ML
            SELECT $COLNAME, $FOREIGN_TBL, $FOREIGN_PK
            FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
            WHERE
                (CONSTRAINT_SCHEMA = '$schema') AND
                (TABLE_NAME = '$this->tableName') AND
                (REFERENCED_COLUMN_NAME IS NOT NULL)
            ;
ML;     
        // refresh connection, if necessary
        DatabaseManager::persistConnection($this->conn);
        
        // run query
        $result = $this->conn->query($sql);
        if (!empty($result) && $result->num_rows > 0)
        {
            // records found
            $recordset = $result->fetch_all(MYSQLI_ASSOC);
            
            $foreignKeys = array();
            foreach($recordset as $row)
            {   
                $fk = $row[$COLNAME];
                $fkTbl = $row[$FOREIGN_TBL];
                $fkTblPK = $row[$FOREIGN_PK];
                
                //console_log("Assigning FOREIGN KEY value '$key' to array index " . sizeof($foreignKeys));
                $foreignKeys[$fk] = array();
                $foreignKeys[$fk][$fkTbl] = $fkTblPK;
            }
            
            return $foreignKeys;
        }
        else
        {
            // no records found
            //console_log("Something went wrong...");
            return null;
        }
        
        // problem authenticating
        $result->close();
        return null;

    }
    
    /**
     * Returns an array of unique key columns from this
     * DAO's database table
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return array A simple array of columns which are
     * intended to be unique within this DAO's database
     * table
     */
    private function getUniqueKeys($columnMeta) : array
    {
        //return $this->getTableColumnMetaData($COLUMN_META);
        
        // CONSTANTS
        $COLUMN_META = "Key";
        
        // initialize variables
        $rs = array();
        $unqKeys = array();
        
        // use the data from the source array
        foreach($columnMeta as $row)
        {
            // store the field's column value inside an associative array
            $field = $row["Field"];
            $columnVal = $row[$COLUMN_META];
            
            // debugging
            //console_log("Storing $COLUMN_META value '$columnVal' inside array key '$field'");
            
            $rs[$field] = $columnVal;
        }
        
        // filter the keys by "Unique Key"
        foreach ($rs as $key => $val)
        {
            if ($val == 'UNI')
            {
                //console_log("Assigning UNIQUE KEY value '$key' to array index " . sizeof($unqKeys));
                array_push($unqKeys, $key);
            }
        }
        return $unqKeys;
    }
    
    /**
     * Returns a regular array (not-associative) of all 
     * column fields which are required in this DAO's table
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return array An array of all column fields whose
     * Null Column's value is 'NO' (meaning, that the
     * column's value CANNOT be NULL, and is therefore
     * required
     */
    private function getRequiredFields($columnMeta) : array
    {
        // TODO: Check also to see if the required field
        // is assigned a DEFAULT value, and if so, exclude it
        // from the list
        
        // CONSTANTS
        $COLUMN_META = "Null";
        
        // initialize variables
        $rs = array();
        $reqFields = array();
        
        // use the data from the source array
        foreach($columnMeta as $row)
        {
            // store the field's column value inside an associative array
            $field = $row["Field"];
            $columnVal = $row[$COLUMN_META];
            
            // debugging
            //console_log("Storing $COLUMN_META value '$columnVal' inside array key '$field'");
            
            $rs[$field] = $columnVal;
        }
        
        // filter the results to include only those fields
        // whose "Null" column is 'NO'
        foreach ($rs as $key => $val)
        {
            if ($val == 'NO')
            {
                array_push($reqFields, $key);
            }
        }
        return $reqFields;
    }
    
    // TODO: Unfinished implementation
    private function generateFieldList(QueryObject $query)
    {
        // initialize variables
        $list = "";
        
        $dataMap = $this->dataTranslator->getDataMap();
        
        // match up each class property to a database field
        // and generate a string representing the field list
        // that will be used in the SELECT statement
        foreach ($query->getCriteria() as $criterion)
        {
            /** @var $criterion \inc\classes\data\CriterionInterface */
            $propName = $criterion->getPropertyName();
            if ($propName == $dataMap)
            {
                
            }
        }
        
        return $list;
    }
    
    /**
     * Returns a SQL WHERE clause to be inserted after the
     * WHERE keyword in a SQL Statement
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param CriterionInterface[] $criteria
     * @param bool $parameterized [Optional] A boolean indicating
     * whether or not the WHERE clause is intended to be used in
     * a parameterized query.  Default value is TRUE.
     * @return string The string to be inserted AFTER the
     * WHERE clause that will be used to filter query results
     */
    private function generateWhereClause(array $criteria, bool $parameterized = true) : string
    {
        // initiate variables 
        $phrases = array();
        
        foreach ($criteria as $criterion)
        {
            /** @var $criterion \misd\data\CriterionInterface */
            // add an " AND " or an " OR " between criteria
            if (!empty($phrases)) array_push($phrases, $criterion->getCriterionType()->value());
            
            array_push($phrases, $this->dataTranslator->translateCriterion($criterion, $parameterized));
        }
        $clause = "(" . join(" ", $phrases) . ")";
        
        // debugging
        //console_log("generateWhereClause returning:\n$clause");
        
        return $clause;
    }   
}

