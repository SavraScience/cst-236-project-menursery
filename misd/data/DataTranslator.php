<?php
namespace misd\data;

// imports
use Exception;
use misd\models\AbstractObjectModel;

// TODO: Delete all debugging statements

/**
 * Translates an associative array of table fields
 * from a data row in a MySQLi ResultSet, commonly
 * retrieved by using ->fetch_all(MYSQLI_ASSOC)
 * from a query result, to a single object or
 * array of objects, which can be cast to a specific
 * class.  This class uses reflection and an array
 * of DataTableMappings 
 * 
 * 
 * @author Michael Mason
 * @copyright 2019 Mason Innovative Software Design
 */
class DataTranslator
{
    // CONSTANTS
    // -- method types
    public const GET_METHOD = "get";
    public const SET_METHOD = "set";
    // -- sql statement types
    public const SQL_INSERT = "insert";
    public const SQL_UPDATE = "update";
    // -- field/value array keys
    public const FLDVAL_ARRY_VAL = "value";
    public const FLDVAL_ARRY_PARAMTYPE = "bind param type";
    public const FLDVAL_ARRY_KEYTYPE = "key type";
    public const FLDVAL_ARRY_RELATIONS = "relations";
    
    // INSTANCE VARIABLES
    private $object;
    
    /** @var $reflection \ReflectionClass */
    private $reflection;
    
    /** @var $metadata DataTableMetadata */
    private $metadata;
    
    /** @var $dataMap DataTableMapping[] */
    private $dataMap;
    
    // TODO: Is this used?
    /** @var $resultSet \mysqli_result */
    private $resultSet; // TODO: Add setter + getter?
    
    private $getters;
    private $setters;

    // CONSTRUCTOR
    // TODO: Should I pass the object by reference or not?
    // TODO: Remove or comment out debugging statements
    public function __construct($object)
    {
        $this->object = $object;
        $this->dataMap = array();
        $this->metadata = new DataTableMetadata();
        
        // scan object for getters & setters
        if (!empty($object)) $this->reflection = new \ReflectionClass($this->object);
        $this->scanClassGetters();
        $this->scanClassSetters();
    }
    
    // PUBLIC METHODS
    
    /**
     * @param mixed $object The object you want
     * to map data to
     */
    public function setObject($object)
    {
        $this->object = $object;
        
        // scan object for getters & setters
        $this->reflection = new \ReflectionClass($this->object);
        $this->scanClassGetters();
        $this->scanClassSetters();
    }
    
    /**
     * Adds a new table mapping to the translator, which will
     * be used to translate the data retrieved from the database
     * to zero, one or more instances of the class you want to
     * convert the table data to.  Most often used with domain
     * models.
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $classPropertyName The name of the property you
     * want to map to a table field
     * @param string $tableFieldName The name of the table field you
     * are mapping to a class property
     * @param string $tableName The name of the table that contains
     * the field you want to match (optional)
     * @param string $bindParamType The key type for this field (non-key, 
     * primary key, or foreign key)
     * @param int $keyType The key type for this field (non-key, 
     * primary key, or foreign key)
     * @param array|null $tableRelations An associative array of table/field 
     * relationships that enforce referential integrity
     * @param Dao|null The binding parameter type for use 
     * with mysqli
     * @return DataTableMapping
     */
    public function addTableMapping(
        string $classPropertyName, 
        string $tableFieldName, 
        string $tableName, 
        string $bindParamType = null,
        int $keyType = 0,
        array $tableRelations = null,
        Dao $dao = null
    ) : DataTableMapping
    {
        $mapping = new DataTableMapping(
            $classPropertyName, 
            $tableFieldName, 
            $tableName, 
            $bindParamType,
            $keyType, 
            $tableRelations, 
            $dao
        );
        $this->dataMap[] = $mapping;
        return $mapping;
    }
    
    /**
     * Returns the data map (DataTableMappings) of class properties
     * matched up to table fields as built by using the 
     * addTableMapping() function
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return DataTableMapping[] Array of DataTableMapping objects.
     */
    public function getDataMap()
    {
        return $this->dataMap;
    }
    
    public function &getDataTableMetadata() : DataTableMetadata
    {
        return $this->metadata;
    }
    
    public function setDataTableMetadata(DataTableMetadata $metadata)
    {
        $this->metadata = $metadata;
    }
    
    /**
     * Creates an array of object property names and their
     * respective values via introspection & reflection. 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param object $object The object you want to build
     * a property/value array from.
     * @param bool $includeNulls (Optional) Include properties
     * whose values are null
     * @return array An array of object property names and
     * their respective values.
     */
    public function getPropertyValueArray($object, bool $includeNulls = false) : array
    {
        $array = array();
        
        /** @var $getMethod \ReflectionMethod */
        foreach ($this->getters as $getMethod => $method)
        {
            //console_log($getMethod);
            $propName = $this->resolveMethodToProperty($getMethod);
            $val = $method->invoke($object);
            if (!is_null($val) || $includeNulls) $array[$propName] = $val; // <- Added 10/27/19 (Remove comment if works)
            //$array[$propName] = $val; // <- Commented out on 10/27/19
        }
        return $array;
    }
    
    /**
     * Returns TRUE or FALSE indicating whether or not
     * the data map (array of DataTableMapping) has any
     * mappings between class properties and table fields
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return bool
     */
    public function hasDataMap() : bool
    {
        if (sizeof($this->dataMap) > 0)
        {
            return true;
        }
        return false;
    }
    
    /**
     * Returns TRUE/FALSE indicating whether or not a
     * ReflectionObject is set for the DataTranslator
     * Consequently, this also indicates whether or not
     * an object was passed into the DataTranslator's
     * contructor
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return bool
     */
    public function hasObjectTemplate() : bool
    {
        return isset($this->reflection);
    }
        
    /**
     * TODO: Revisit this method when you are ready to
     * begin type-checking
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $field The table field for which the
     * proposed value would be assigned
     * @param mixed $value The proposed value for the field
     * to be checked against
     * @return bool
     */
    public function isLegalValue(string $field, $value) : bool
    {
        $fldDataType = $this->metadata->getFieldTypes()[$field];
        
        // for string values
        if (is_string($value) && $this->isMySqlString($fldDataType))
        {
            return true;
        }
        elseif (is_int($value) && $this->isMySqlInt($fldDataType))
        {
            return true;
        }
        elseif (is_float($value) && $this->isMySqlFloat($fldDataType))
        {
            return true;
        }
        else 
        {
            
        }
        return false;
    }
    
    private function isMySqlString(string $fieldType) : bool
    {
        if (stripos($fldDataType, "char") !== FALSE) return true;
        return false;
    }
    
    private function isMySqlInt(string $fieldType) : bool
    {
        if (stripos($fldDataType, "int") !== FALSE) return true;
        return false;
    }
    
    private function isMySqlFloat(string $fieldType) : bool
    {
        if (stripos($fldDataType, "float") !== FALSE) return true;
        if (stripos($fldDataType, "double") !== FALSE) return true;
        if (stripos($fldDataType, "decimal") !== FALSE) return true;
        return false;
    }
    
    private function isMySqlBlob(string $fieldType) : bool
    {
        $isBlob = false;
        
        return false;
    }
    
    /**
     * Translates a property/value associative array to
     * a field/value associative array using the
     * DataTranslator's intrinsic data map.
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param array $propValArray An associative array of
     * property names (key) and their respective values (value)
     * @return array An associative array of table field names (key) 
     * and their respective values (value)
     */
    public function translatePropertyValueArray(array $propValArray) : array
    {
        $array = array();
        foreach ($propValArray as $prop => $val)
        {
            $fldName = $this->resolvePropertyToField($prop);
            if (!is_null($fldName))
            {   
                $data = array();
                $data[self::FLDVAL_ARRY_VAL] = $val;
                //console_log("Field '$fldName's value set to " . $data[self::FLDVAL_ARRY_VAL]);
                
                // get additional data
                $mapping = $this->getMappingForField($fldName);
                if (!is_null($mapping))
                {
                    //console_log("Additional data found for field '$fldName'");
                    if (!empty($mapping->getBindParamType()))
                    {
                        $data[self::FLDVAL_ARRY_PARAMTYPE] = $mapping->getBindParamType();
                        //console_log("Field '$fldName's bind param type set to " . $data[self::FLDVAL_ARRY_PARAMTYPE]);
                    }
                    $data[self::FLDVAL_ARRY_KEYTYPE] = $mapping->getKeyType();
                    //console_log("Field '$fldName's key type set to " . $data[self::FLDVAL_ARRY_KEYTYPE]);
                    $data[self::FLDVAL_ARRY_RELATIONS] = $mapping->getTableRelations();
                }
                else
                {
                    // could not retrieve additional data
                    // -- do nothing
                    //console_log("No \$mapping data discovered for field '$fldName'");
                }
                
                $array[$fldName] = $data;
            }
        }
        return $array;
    }

    
    public function translateObjectToSqlInsertUpdate($object, &$fieldVals = null, $sqlType = self::SQL_INSERT) : string
    {
        // initialize variables
        $sql = "";
        $fieldList = "";
        $valuesList = "";
        $setList = "";
        $idField = $this->metadata->getSurrogatePK();
        $tableName = $this->metadata->getTableName();
        $includeNulls = $sqlType == self::SQL_INSERT ? false : true;
        
        if (is_object($object))
        {
            if ($this->hasObjectTemplate() && $this->hasDataMap())
            {
                if (empty($fieldVals) || !is_array($fieldVals))
                {   
                    // retrieve all of the object's properties and their values
                    $propVals = $this->getPropertyValueArray($object, $includeNulls);
                    
                    // resolve properties to field names
                    $fieldVals = $this->translatePropertyValueArray($propVals);
                }
                
                // debugging
                //console_log("Property/Values array:\n" . array_toString($propVals));
                //console_log("Field/Values array:\n" . array_toString($fieldVals));
                
                if ($sqlType == self::SQL_INSERT)
                {
                    /* build a field list and values list separately */
                    
                    // check that all required fields have been set
                    if (!$this->requiredFieldsSet($fieldVals))
                    {
                        throw new Exception('Cannot insert object into table -- not all required fields were set!');
                    }
                    
                    /* check that each property being mapped matches 
                     * the data type of the receiving field and 
                     * convert objects to integers for foreign keys,
                     * if necessary
                     * */
                    try
                    {
                        $this->flattenFieldValueArray($fieldVals);
                    }
                    catch (Exception $ex)
                    {
                        throw new Exception("Cannot insert object into table -- " . $ex->getMessage());
                    }
                            
                    // debugging
                    //console_log("Field/Values array (after flattened):\n" . array_toString($fieldVals));
                    
                    // all required values are set
                    $fieldList = join(", ", array_keys($fieldVals));
                    
                    // build string of parameterized question marks
                    $paramArray = array();
                    for ($i = 0; $i < sizeof($fieldVals); $i++)
                    {
                        array_push($paramArray, "?");
                    }
                    $valuesList = join(", ", $paramArray);
                }
                elseif ($sqlType == self::SQL_UPDATE)
                {
                    /* build a set list */

                    // filter out the primary key field from the field/value array
                    $fieldVals = array_filter($fieldVals, function($v, $k) {
                        return $k != $this->metadata->getSurrogatePK();
                    }, ARRAY_FILTER_USE_BOTH);
                    
                    //console_log("New array minus primary key field/value");
                    //console_log(array_toString($fieldVals));
                    
                    /* check that each property being mapped matches
                     * the data type of the receiving field and
                     * convert objects to integers for foreign keys,
                     * if necessary
                     * */
                    try
                    {
                        $this->flattenFieldValueArray($fieldVals);
                    }
                    catch (Exception $ex)
                    {
                        throw new Exception("Cannot update object in table -- " . $ex->getMessage());
                    }
                    
                    $setArray = array();
                    foreach ($fieldVals as $field => $data)
                    {
                        array_push($setArray, "$field = ?");
                    }
                    $setList = join(", ", $setArray);
                }
            }
            else
            {
                // exit early -- cannot translate object because no object template or data map were found
                throw new Exception("Cannot translate object -- no object template or data map found");
            }
        }
        else
        {
            // $object is actually a scalar value
            //console_log("Non-object value detected in lieu of object");
            throw new Exception("Non-object value detected in lieu of object");
        }
        
        // build SQL string
        if ($sqlType == self::SQL_INSERT)
        {
            $sql = <<< ML
                INSERT INTO $tableName
                ($fieldList)
                VALUES($valuesList)
                ;
ML;
        }
        elseif ($sqlType == self::SQL_UPDATE)
        {
            $sql = <<< ML
                UPDATE $tableName
                SET $setList
                WHERE $idField = ?
                ;
ML;
        }
        
        return $sql;
    }
    
    /**
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param array $tableRow
     * @return object A single instance or array of instances
     * of the object desired to be returned. To get the most
     * out of this method, it is desirable to cast the result
     * of this method in its implementation to the desired
     * object, being sure to check if the returned object is
     * an array, an object or null.
     */
    public function translateRow(array $tableRow)
    {   
        // initialize variables
        $id = null;
        
        // debugging
        //console_log("Translating Table Row...");
        //var_dump($tableRow);
        
        // get the table's primary key & value, first
        try
        {
            $pk = $this->metadata->getSurrogatePK();
            $id = (int)$tableRow[$pk];
            //console_log('Setting $id to ' . $id);
        }
        catch (Exception $ex)
        {
            throw new Exception("Something went wrong whilst retrieving the table's primary key...");
        }
        
        // instantiate an empty object
        /** @var $object AbstractObjectModel */
        $object = $this->reflection->newInstance($id);
        //console_log("Object->getId() = " . $object->getId());
        
        foreach ($this->dataMap as $mapping)
        {      
            try
            {
                /** @var $mapping DataTableMapping */
                $fieldKey = $mapping->getFieldName();
                
                // if DataTableMapping can be matched to a field name
                if (array_key_exists($fieldKey, $tableRow))   
                {
                    // retrieve the name of the class property to set
                    $propName = $mapping->getClassProperty();
                    
                    // build a method name from the property
                    $methodName = $this->resolvePropertyToMethod($propName, self::SET_METHOD);
                    
                    // check to see if the method exists in the array of setter methods
                    if (array_key_exists($methodName, $this->setters))
                    {   
                        // retrieve the value of the field at this location
                        $var = $tableRow[$fieldKey];
                        $meta = $this->metadata;
                     
                        // TODO: Add proper checks to prevent errors
                        if ($meta->isForeignKey($fieldKey))
                        {
                            /* check to see if there is a DAO associated with
                            this DataTableMapping */
                            $dao = $mapping->getDao();
                            if (!empty($dao))
                            {
                                // attempt to convert the foreign key integer to an object
                                //console_log("Table mapping has a DAO object to resolve foreign keys");
                                $var = $dao->findById((int)$var);
                            }
                            else 
                            {
                                // TODO: FIX THIS -- we are no longer going to use this
//                                 if ($mapping->mapKeyToDescriptor())
//                                 {
//                                     // retrieve the foreign table and its primary key
//                                     $fkMap = $meta->getForiegnKeyMap($fieldKey);
//                                     $fkTable = array_key_first($fkMap);
//                                     $fkTablePk = $fkMap[$fkTable]; // do we need this anymore?
                                    
//                                     /* get a generic instance of the foreign key table
//                                      * and retrieve its corresponding record */
//                                     /* $dao = new DaoGay($fkTable); */
//                                     $record = $dao->findById((int)$var);
                                    
//                                     // ensure that just ONE record was retrieved
//                                     if (!is_null($record))
//                                     {
//                                         // look in table for the first CHAR/VARCHAR
//                                         $descriptors = $dao->getTableMetadata()->getDescriptors();
//                                         if (!empty($descriptors))
//                                         {
//                                             $descriptor = $descriptors[0];
//                                             $var = $record[$descriptor];
//                                         }
//                                     }
//                                }
//                                else
//                                {
//                                    // simply assign leave the value assigned as an integer
//                                    console_log("DataTranslator has no instructions for translating the foreign key...\n
//                                        Assigning the class property '$propName' to integer value '$var'.");
//                                }
                            }
                        }   
                        
                        // debugging
                        //console_log("Setting '$propName' to $var\n");
                        
                        // set the value of the object's property to its corresponding table field value
                        $this->reflection->getMethod($methodName)->invoke($object, $var);
                    }   
                }
            }
            catch (Exception $e)
            {
                // print message to console
                console_log($e);
            }
        }
        
        //console_log("Returning object id #" . $object->getId());
        return $object;
    }
    
    /**
     * Converts all criteria values that are associated with a
     * foreign key to a foreign key ID, if possible
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param CriterionInterface[] $criteria
     */
    public function flattenCriteriaValues(array $criteria) : void
    {
        /* First, check to see if any criteria needs to
         * be resolved to a foreign key, and if so resolve;
         * if it cannot be resolved, remove the criterion from
         * the list
         */
        for ($i = 0; $i < count($criteria); $i++)
        {
            /** @var $criterion CriterionInterface */
            // get the corresponding field name for property
            $criterion = $criteria[$i];
            $field = $this->resolvePropertyToField($criterion->getPropertyName());
            if ($field != "")
            {
                // check to see if the field is a foreign key...
                if ($this->metadata->isForeignKey($field))
                {
                    // ...then attempt to retrieve its DAO
                    $dao = $this->getDaoForField($field);
                    if (!is_null($dao))
                    {
                        // attempt to find a single descriptor for the DAO's table
                        $descriptor = $dao->getTableMetadata()->tryGetSoloDescriptor();
                        
                        if(!empty($descriptor))
                        {
                            // get current value for comparison
                            $currVal = $criterion->getCriterionValue();
                            
                            // TODO: This method is currently FLAWED because the value it attempts
                            // to look up may be incomplete (such as when LIKE is used as a comparison
                            // operator); therefore, it may be more feasible to add an optional 
                            
                            // query the table using descriptor field and value
                            $result = $dao->findIdViaDescriptor($descriptor, $currVal, false);
                            if ($result !== false)
                            {
                                $newVal = $result[0];
                                //console_log("\$newVal is type of " . gettype($newVal));
                                /* convert the criteria's old descriptor string
                                 value to its equivalent primary key ID */
                                //console_log("New criterion value resolved from '$currVal' to '$newVal'!");
                                $criterion->setCriterionValue($newVal);
                            }
                            else
                            {
                                // remove the criteria from the list
                                //console_log("Removing criterion from the list...");
                                $criterion->remove();
                            }
                        }   
                    }
                }
            }
            
        }
    }
    
    /**
     * Translates an object implementing the CriterionInterface into a
     * string
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param CriterionInterface $criterion The criterion for which to convert
     * into a string for the purpose of filtering some data store result
     * @param bool $parameterized [Optional] A boolean indicating whether 
     * or not the WHERE clause is intended to be used in a parameterized 
     * query.  Default value is TRUE.
     * @return string|NULL A single phrase that will be used as part of a greater
     * criteria clause
     */
    public function translateCriterion(CriterionInterface $criterion, bool $parameterized = true)
    {
        // initialize string array
        $phraseParts = array();
        
        // left-hand side (field name)
        $fieldName = $this->resolvePropertyToField($criterion->getPropertyName());
        //if (is_null($fieldName)) console_log("field name is null");
        if (!is_null($fieldName))
        {
            $phraseParts[] = $fieldName;
            
            // middle (comparison operator)
            $phraseParts[] = $criterion->getOperator();
                
            // right-hand side (value)
            if ($parameterized)
                $phraseParts[] = "?";
            else
                $phraseParts[] = strval($this->sqlEncaseValue($criterion->getCriterionValue()));
            
            // finally, combine the parts into a single phrase
            try
            {            
                $phrase = ' (' . join(" ", $phraseParts) . ') ';
                
                // debugging
                //console_log($phrase);
                
                return $phrase;
            }
            catch (Exception $e)
            {
                console_log($e);
                return null;
            }
        }
        else
        {
            return null;
        }
    }
    
    // PRIVATE FUNCTIONS
    /**
     * Iterates through a field/value array to ensure that
     * 1) All field values are legal, and
     * 2) All objects can be "flattened," or resolved to a
     * simple integer type.  This will attempt to insert
     * new values into the database for each field where an
     * object type is found and can be converted using its
     * corresponding DAO.
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param array $fieldVals The array of fields and values
     * to be "flattened"
     * @throws Exception
     * 
     * TODO: Remove/comment-out debugging statements
     */
    private function flattenFieldValueArray(array &$fieldVals) : void
    {
        // debugging
        //console_log("'Flattening' field values (resolving strings to IDs)...");
        
        // initialize variables
        $conversions = array(); // A placeholder for each foreign key field's DAO
        
        // first, check that there are or will be no errors with the data
        foreach ($fieldVals as $field => $data)
        {
            // get field value and key type
            $val = $data[self::FLDVAL_ARRY_VAL];
            $keyType = $data[self::FLDVAL_ARRY_KEYTYPE];
            
            if (is_null($val))
            {
                // do nothing -- resolve to null
            }
            elseif (is_object($val))
            {
                // if the field value is an object...
                // ...and if the field maps to a foreign key...
                if ($keyType == DataTableMapping::KEYTYPE_FOREIGN_KEY || $this->metadata->isForeignKey($field))
                {
                    // debugging
                    //console_log("Field '$field' is intended to be a foreign key which maps to another table...");
                    
                    // get the field's DataTableMapping
                    $mapper = $this->getMappingForField($field);
                    
                    if (!is_null($mapper))
                    {
                        // get the field's corresponding DAO
                        $dao = $mapper->getDao();
                        
                        if (!is_null($dao))
                        {
                            // add value and DAO to array for conversion later
                            //console_log("Retrieved field $field's DAO. Adding $val to the conversions array...");
                            $conversions[$field] = array($val, $dao);
                        }
                        else
                        {
                            throw new Exception("Cannot flatten field/value array -- " .
                                "No DAO could be found for field '$field' baring 'object' type!");
                        }
                    }
                    else
                    {
                        throw new Exception("Cannot flatten field/value array -- " .
                            "No DataTableMapping could be found for field '$field' baring 'object' type!");
                    }
                }
                else
                {
                    throw new Exception("Cannot flatten field/value array -- " .
                        "field '$field' baring type 'object' is not a foreign key field and therefore cannot be resolved!");
                }
            }
            elseif (is_scalar($val))
            {
                // TODO: Implement more complex checks to ensure data types match up,
                // but for now, do nothing
            }
            else
            {
                throw new Exception("Cannot flatten field/value array -- field '$field' value could not be resolved to " .
                    "either object or scalar type.");
            }
        }
        
        // TODO: Use ACID Transaction control to ensure that
        // all conversions happen all at once, or not at all
        foreach ($conversions as $field => $array)
        {
            // initialize variables (retrieve values from array
            $id = 0;
            $val = $array[0];
            /** @var $dao Dao */
            $dao = $array[1];
            
            if ($val instanceof AbstractObjectModel)
            {
                /** @var $val AbstractObjectModel */
                
                // debugging
                $tmp = "";
                if (stringConvertable($val)) $tmp = " ('$val')";
                //console_log("Field '$field' value$tmp is an AbstractObjectModel. Getting ID...");
                
                // get the ID using the object's innate getter method
                $id = $val->getId();
                //console_log("ID resolved to $id");
            }
            
            // debugging
            //console_log("$field's ID = $id");
            
            if ($id == 0)
            {
                try
                {
                    // attempt to insert new value into database
                    //console_log("Attempting to insert new value into database...");
                    $id = $dao->insert($val);
                    if ($id != 0)
                    {
                        // DAO was successful in inserting the new record
                        //console_log("Newly inserted ID = $id");
                    }
                    else
                    {
                        // DAO was NOT successful in inserting the new record;
                        // convert $id to NULL value
                        $id = null;
                    }
                }
                catch (Exception $ex)
                {
                    throw new Exception("Cannot flatten field/value array -- " . $ex->getMessage());
                }
            }
            
            // override $field value
            //console_log("Overriding $field's old array value to $id");
            $fieldVals[$field][self::FLDVAL_ARRY_VAL] = $id;
            //console_log("$field's new array value = $fieldVals[$field]");
        }
        
        // debugging
        //console_log("Finished 'flattening' field values (resolving strings to IDs)...");
    }
    
    /**
     * Returns the corresponding DAO for a foreign key
     * field, if it exists
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $fieldName The foreign key field for 
     * which you want to retrieve its DAO
     * @return \misd\data\Dao|NULL
     */
    private function getDaoForField(string $fieldName)
    {
        $mapping = $this->getMappingForField($fieldName);
        return $mapping->getDao();
    }
    
    /**
     * Returns a DataTableMapping for the corresponding
     * table field using its name; if one cannot be found,
     * it simply returns null.
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $fieldName The field for which you
     * want to return a DataTableMapping object
     * @return NULL|\misd\data\DataTableMapping
     */
    private function getMappingForField(string $fieldName)
    {
        // get the corresponding field's DataTableMapping
        $mapper = null;
        foreach ($this->dataMap as $mapping)
        {
            /** @var $mapping \misd\data\DataTableMapping */
            if ($fieldName == $mapping->getFieldName())
            {
                $mapper = $mapping;
                break;
            }
        }
        return $mapper;
    }
    
    /**
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return \ReflectionClass A ReflectionClass object
     * of the object set in the constructor or by calling
     * the setObject() method
     */
    private function getObjectClass()
    {
        return $this->reflection;
    }
    
    /**
     * Returns an array of a given object's public properties
     * as \ReflectionProperty objects
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param object $object Any object
     * @return array An array of \ReflectionProperty objects
     * 
     */
    private function getClassPublicMethods($object)
    {
        return $this->reflection->getMethods(\ReflectionMethod::IS_PUBLIC);
    }
    
    /**
     * Checks to ensure that all required table field values
     * are set, given an array of table field names and their
     * respective values
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param array $fieldVals An array of table field names
     * and their respective values
     * @return bool Returns true if all required fields are set
     * given the array of field values; otherwise, returns false.
     */
    private function requiredFieldsSet(array $fieldVals) : bool
    {        
        // get all required fields which do not have a default value
        $required = $this->getDataTableMetadata()->getMinRequiredFields();
        
        // debugging
        //console_log("Absolutely required fields:\n" . array_toString($required));
        
        // compare object properties to required fields
        foreach ($required as $reqField)
        {
            // debugging
            //console_log("Checking value for required field '$reqField'");
            //console_log("Var at index '$reqField' is of type " . gettype($fieldVals[$reqField]));
            
            if (!isset($fieldVals[$reqField]) || is_null($fieldVals[$reqField][self::FLDVAL_ARRY_VAL]))
            {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Sets the instance-level associative array $setters with
     * an array of ReflectionMethod objects that represent
     * all of the given object's public class mutator methods
     * (setters)
     * 
     * e.g. $setters['setName'] => ReflectiveMethod object
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     */
    private function scanClassSetters()
    {
        if (!empty($this->object))
        {
            // initialize instance variable
            $this->setters = array();
            
            // get all of the $object argument's public properties
            $publicMethods = $this->getClassPublicMethods($this->object);
            
            // check for matching property setters for
            // the $object's class
            foreach ($publicMethods as $method)
            {
                /** @var $method \ReflectionMethod */
                $methodName = $method->getName();
                
                // debugging
                //console_log($methodName);
                
                if (substr($methodName, 0, 3) == "set")
                {
                    // add the ReflectionMethod to the 
                    // associative array $setters
                    $this->setters[$methodName] = $method;
                }
            }
        
        }
    }

    /**
     * Sets the instance-level associative array $getters with
     * an array of ReflectionMethod objects that represent
     * all of the $this->object's public class accessor methods
     * (getters)
     * 
     * e.g. $getters['getName'] => ReflectiveMethod object
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     */
    private function scanClassGetters()
    {
        if (!empty($this->object))
        {
            // initialize instance variable
            $this->getters = array();
            
            // get all of the $object argument's public properties
            $publicMethods = $this->getClassPublicMethods($this->object);
            
            // check for matching property setters for
            // the $object's class
            foreach ($publicMethods as $method)
            {
                /** @var $method \ReflectionMethod */
                $methodName = $method->getName();
                
                // debugging
                //console_log($methodName);
                
                if (substr($methodName, 0, 3) == "get")
                {
                    // add the ReflectionMethod to the
                    // associative array $setters
                    $this->getters[$methodName] = $method;
                }
            }
            
        }
    }
    
    // RESOLVER METHODS
    
    /**
     * Attempts to resolve a field (name) to a property (name)
     * using the DataTranslator's $dataMap property, which is
     * an array of DataTableMappings
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $fieldName The name of the database table
     * field you want to resolve to a class property (name) using 
     * the DataTranslator's $dataMap
     * @return string|NULL The corresponding property name to the
     * $fieldName passed in as an argument
     */
    public function resolveFieldToProperty(string $fieldName) : string
    {
        // check to see if the field exists in the data map
        foreach ($this-dataMap as $mapping)
        {
            /** @var $mapping \inc\classes\data\DataTableMapping */
            if($fieldName == $mapping->getFieldName())
            {
                // property match found
                return $mapping->getClassProperty();
            }
            return null; // is this legal?
        }
    }
    
    /**
     * Resolves an object property's name to its corresponding
     * table field according to the DataTranslator's data map
     * (array of DataTableMappings).
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $propertyName The name of the class property 
     * you want to resolve to a database table field (name) using 
     * the DataTranslator's $dataMap
     * @return string|NULL The corresponding field name to the
     * $propertyName passed in as an argument
     */
    public function resolvePropertyToField(string $propertyName)
    {
        // debugging
        $class = $this->reflection->getName();
        //console_log("Attempting to resolve property '$propertyName' for $class...");
        
        // check to see if the property exists in the data map
        foreach ($this->dataMap as $mapping)
        {
            /** @var $mapping \misd\data\DataTableMapping */
            //console_log("Searching [$mapping]");
            if($propertyName == $mapping->getClassProperty())
            {
                // property match found
                //console_log("Match found - mapping $propertyName to " . $mapping->getFieldName());
                return $mapping->getFieldName();
            }
        }
        
        // property match not found
        return null;
    }
    
    /**
     * Given a $methodName, returns a truncated version without
     * "get" or "set" that resolves to the name of the property
     * the method corresponds to.
     * 
     * If the $methodName does not begin with "get" or "set",
     * it will not be resolved to a property and will thus
     * return NULL.
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $methodName The name of the method you want
     * to resolve to a property.
     * @return string|NULL The resolved name of the property to
     * which the passed $methodName belongs
     */
    private function resolveMethodToProperty(string $methodName)
    {
        // assume get or set is used
        $methodPrefix = substr($methodName, 0, 3);
        if ($methodPrefix == "get" || $methodPrefix == "set")
        {
            return lcfirst(substr($methodName, 3));
        }
        return null;
    }
    
    /**
     * Takes two arguments: the property's name you want to transform
     * into a method and the method type (GETTER: 0 or SETTER: 1) you
     * want it to transform into
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $propName The name of the property you want to
     * transform to a method name
     * @param string $methodType The method type (getter or setter) you want
     * transform the property name to.  In essence, the $methodType is prefixed
     * to the property name.  Best used with SET_METHOD and GET_METHOD constants.
     * @return string The transformed member name from property to method
     */
    private function resolvePropertyToMethod(string $propName, string $methodType)
    {
        // transpose the first letter of the property
        $propName = ucfirst($propName);
        
        // return the property name prefixed with the method type (get or set)
        return $methodType . $propName;
    }
    
    // STRING OPERATIONS
    
    /**
     * Returns a string with 1 space of padding before and after the
     * original string argument
     *
     * E.g. pad("AND") would return " AND "
     *
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $stringToPad
     * @return string
     */
    private function pad(string $stringToPad) : string
    {
        if (substr($stringToPad, 0, 1) == " " &&
            substr($stringToPad, -1) == " ")
        {
            // string is already surrounded with padding (space) -- do nothing
        }
        else
        {
            return " " . $stringToPad . " ";
        }
    }
    
    /**
     * Optionally encases a value in special characters according
     * to its value type.  For example, Strings are
     * enclosed in quotation marks whereas integer and
     * double values are left alone.
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param mixed $var The raw value to be encased,
     * if needed
     * @return mixed|string Returns the original value back
     * if the argument $var is NOT a string; otherwise,
     * it returns a string value encased in quotation marks
     * dependent upon whether or not it already contains such
     * marks and, if so, what kind
     */
    private function sqlEncaseValue($var)
    {
        if (is_string($var))
        {
            if (
                (substr($var, 0, 1) == "'" &&
                    substr($var, -1) == "'") ||
                (substr($var, 0, 1) == '"' &&
                    substr($var, -1) == '"')
                )
            {
                // string is already encased in quotes -- do nothing
                return $var;
            }
            else
            {
                $sym = "";
                
                if (strpos($var, '"') && strpos($var, "'"))
                {
                    // string includes both single AND double quotes -- escape one or the other
                    // TODO: implement
                }
                elseif (strpos($var, "'"))
                {
                    // string contains single quotes only -- enclose in double quotes
                    $sym = '"';
                }
                else
                {
                    // string may or may not contain double quotes -- just encase in single quotes
                    $sym = "'";
                }
                
                // return an string encased in quotes
                return $sym . $var . $sym;
            }
        }
        else
        {
            // Value is not a string or special value that needs to be encased
            return $var;
        }
    }
}