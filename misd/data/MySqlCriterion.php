<?php
namespace misd\data;

/**
 * A single criterion that can be used by a QueryObject 
 * for querying a MySQL database using a model object's 
 * properties 
 * @author Michael Mason
 * @copyright 2019 Mason Innovative Software Design
 */
class MySqlCriterion implements CriterionInterface
{
    // INSTANCE VARIABLES
    protected $queryObject;
    protected $propertyName;
    private $operator;
    protected $criteriaValue;
    protected $type;
    
    // CONSTRUCTOR
    
    /**
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param QueryObject $queryObject The query object that the
     * criterion belongs to.
     * @param string $classPropertyName The name of the class property
     * whose value you are using for comparison
     * @param ComparisonOperatorInterface $operator The type of operator
     * used in comparison
     * @param mixed $value The value of the class property you are using
     * for comparison
     * @param int $criterionType The type of criteria (usu. AND or OR)
     */
    public function __construct(
        QueryObject &$queryObject,
        string $classPropertyName, 
        ComparisonOperatorInterface $operator, 
        $value,
        int $criterionType = CriterionTypeInterface::AND
    )
    {   
        $this->queryObject = $queryObject;
        $this->propertyName = $classPropertyName;
        $this->operator = $operator;
        $this->criteriaValue = $value;
        $this->type = new MySqlCriterionType($criterionType);
    }

    /**
     * 
     * {@inheritDoc}
     * @see \misd\data\CriterionInterface::getPropertyName()
     */
    public function getPropertyName() : string
    {
        return $this->propertyName;
    }

    /**
     * 
     * {@inheritDoc}
     * @see \misd\data\CriterionInterface::getCriterionValue()
     */
    public function getCriterionValue()
    {
        return $this->criteriaValue;
    }

    /**
     * 
     * {@inheritDoc}
     * @see \misd\data\CriterionInterface::setCriterionValue()
     */
    public function setCriterionValue($critVal) : void
    {
        $this->criteriaValue = $critVal;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \misd\data\CriterionInterface::getOperator()
     */
    public function getOperator() : string
    {
        return $this->operator->value();
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \misd\data\CriterionInterface::getCriterionType()
     */
    public function getCriterionType() : CriterionTypeInterface
    {
        return $this->type;
    }
    
    /**
     * {@inheritDoc}
     * @see \misd\data\CriterionInterface::equals()
     */
    public function equals(CriterionInterface $criterion): bool
    {
        if ($this == $criterion)
            return true;
        else
        {
            // check to see that all class member values are the same
            if (
                $this->getPropertyName() == $criterion->getPropertyName() &&
                $this->getOperator() == $criterion->getOperator() &&
                $this->getCriterionValue() == $criterion->getCriterionValue() &&
                $this->getCriterionType()->value() == $criterion->getCriterionType()->value()
            )
            {
                return true;
            }
            return false;
        }
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \misd\data\CriterionInterface::remove()
     */
    public function remove(): void
    {
        $this->queryObject->removeCriterion($this);
    }
}

