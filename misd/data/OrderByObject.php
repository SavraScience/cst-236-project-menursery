<?php
namespace misd\data;

/**
 * An object that represents the way a table will
 * be sorted when it is queried.
 * @author Michael Mason
 * @copyright 2019 Mason Innovative Software Design
 */
class OrderByObject
{
    // PUBLIC CONSTANTS
    const ASC = 1;
    const DESC = 2;
    
    // INSTANCE VARIABLES
    private $fieldName;
    private $order;
    
    // CONSTRUCTOR
    public function __construct(string $fieldName, int $order = self::ASC)
    {
        $this->fieldName = $fieldName;
        $this->order = $this->order;
    }
    
    public function getFieldName() : string
    {
        return $this->fieldName;
    }
    
    public function getOrder() : int
    {
        return $this->order;
    }
    
    public function __toString()
    {
        return "$this->fieldName $this->order";
    }
}

