<?php
namespace misd\data;

/**
 * Specifies a MySQL criterion type, either AND or OR, for
 * a QueryObject.
 * @author Michael Mason
 * @copyright 2019 Mason Innovative Software Design
 */
class MySqlCriterionType implements CriterionTypeInterface
{
    // INSTANCE VARIABLES
    private $value;
    
    // PRIVATE CONSTRUCTOR
    public function __construct(int $type)
    {
        switch ($type)
        {
            case CriterionTypeInterface::AND:
                //console_log("Setting type to 'AND'");
                $this->value = "AND";
                break;
            case CriterionTypeInterface::OR:
                //console_log("Setting type to 'OR'");
                $this->value = "OR";
                break;
            default:
                $this->value = "AND";
        }
    }
    
    // PUBLIC FUNCTIONS
    public function value() : string
    {
        return $this->value;
    }
    
    // STATIC FUNCTIONS
    public static function AND() : CriterionTypeInterface
    {
        return new MySqlCriterionType(CriterionTypeInterface::AND);
    }

    public static function OR() : CriterionTypeInterface
    {
        return new MySqlCriterionType(CriterionTypeInterface::OR);
    }
}

