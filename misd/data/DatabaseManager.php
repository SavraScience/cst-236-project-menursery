<?php
namespace misd\data;

// TODO: Abstract out to enable other classes to extend with specific connection details.
/**
 * Manages connections to the database
 * @author Michael Mason
 * @copyright 2019 Mason Innovative Software Design
 */
class DatabaseManager
{    
    // CONSTANTS
    // TODO: These credentials really should be stored somewhere else
    /* production server */
    //private cosnt HOST = 'mikemasonpro.net';
    //private const USERNAME = 'mikema10_admin';
    //private const PASSWORD = 'iamgroot0529';
    //private const DATABASE = 'mikema10_menursery';
    /* testing server */
    private const HOST = 'localhost';
    private const USERNAME = 'tester1041_admin';
    private const PASSWORD = 'Tryinit05$';
    private const DATABASE = 'menursery';
    
    private static $ERR_MSG_CONNECTION = "There was a problem connecting to the database.";
    
    // METHODS
    /**
     * Connects to the application's database using the credentials supplied
     * by the constants HOST, USERNAME, PASSWORD, & DATABASE.
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return \mysqli A database connection object (mysqli)
     */
    public static function getConnection()
    {
        // get a new connection
        $connection = new \mysqli(self::HOST, self::USERNAME, self::PASSWORD, self::DATABASE);
        
        // select database
        /*
        $sql = "USE " . self::DATABASE;
        console_log("Executing query:\n$sql");
        $connection->query($sql);
        */
        
        return $connection;
    }
    
    public static function getDatabaseName()
    {
        return self::DATABASE;
    }
    
    /**
     * Refreshes a connection if needed. Best used in cases where a
     * connection is used across multiple functions as the property of
     * an object or an instance variable.
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param \mysqli $conn The connection you want to persist (keep alive)
     */
    public static function persistConnection(&$conn)
    {
        if (is_null($conn)) $conn = self::getConnection();
        if ($conn->connect_error) exit(self::$ERR_MSG_CONNECTION);
    }
    
    /**
     * Simply tests whether or not a connection can be made
     * to the database server using the credentials supplied
     * by the constants HOST, USERNAME, PASSWORD, & DATABASE
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return boolean
     */
    public static function testConnection($conn = null)
    {
        // instantiate variables
        $connected = false;
        
        if (is_null($conn))
        {
            // connect to the database and determine its status
            $conn = self::getConnection();
        }
        else
        {
            // connection passed in as argument
            // -- do nothing
        }
        
        if (!$conn->connect_error) 
            $connected = true;
        else
            exit(self::$ERR_MSG_CONNECTION);
        
        $conn->close();
        
        return $connected;
    }
}

