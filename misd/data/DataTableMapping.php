<?php
namespace misd\data;

use misd\models\AbstractObjectModel;
use Exception;

/**
 * A class to store mappings between model properties
 * and table field names.
 * @author Michael Mason
 * @copyright 2019 Mason Innovative Software Design
 */
class DataTableMapping
{
    // PUBLIC CONSTANTS
    public const KEYTYPE_NON_KEY        = 0;
    public const KEYTYPE_PRIMARY_KEY    = 1;
    public const KEYTYPE_FOREIGN_KEY    = 2;
    
    // INSTANCE VARIABLES
    private $classProperty;
    private $tableName;
    private $fieldName;
    
    private $keyType;
    private $tableRelations;    // array of table relationships; foreign key relationships mandatory
    private $bindParamType;     // mysqli bind param type (string)
    
    // TODO: DEPRECATED
    private $isForeignKey;
    private $mapFkToDescriptor;
    
    /** @var $dao Dao */
    private $dao;
    
    // CONSTRUCTOR
    
    /**
     * Provides a way to map the properties of a class to a database 
     * table's fields.
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $classPropertyName The name of the property you
     * want to map to a table field
     * @param string $tableFieldName The name of the table field you
     * are mapping to a class property
     * @param string $tableName The name of the table that contains
     * the field you want to match (optional)
     * @param int $keyType The key type for this field (non-key, 
     * primary key, or foreign key)
     * @param array|null $tableRelations An associative array of table/field 
     * relationships that enforce referential integrity
     * @param string $bindParamType The binding parameter type for use 
     * with mysqli
     * @param Dao $dao (Optional) The DAO you want to use to resolve
     * this property to an object using another table
     */
    public function __construct(
        string $classPropertyName, 
        string $tableFieldName, 
        string $tableName,
        string $bindParamType = null,
        int $keyType = 0,
        array $tableRelations = null,
        Dao $dao = null
    )
    {
        if (!empty($classPropertyName)) $this->classProperty = $classPropertyName;
        if (!empty($tableName)) $this->tableName = $tableName;
        if (!empty($tableFieldName)) $this->fieldName = $tableFieldName;
        $this->bindParamType = $bindParamType;
        $this->keyType = $keyType;
        
        if (!is_null($tableRelations))
        {
            $this->tableRelations = $tableRelations;
        }
        else
        {
            $this->tableRelations = array();
        }
        
        if (!is_null($dao)) $this->dao = $dao;
        
        return $this;
    }
    
    // ACCESSOR METHODS
    /**
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return string The class's property, private or public,
     * to be matched with its corresponding table field
     */
    public function getClassProperty() : string
    {
        // debugging
        //console_log("Accessing class property '$this->classProperty'");
        return $this->classProperty;
    }

    /**
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return Dao|null The DAO attached to this 
     * DataTableMapping, if any
     */
    public function getDao()
    {
        return $this->dao;
    }
    
    /**
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return string The name of the table housing the 
     * field to be associated
     */
    public function getTableName() : string
    {
        return $this->tableName;
    }

    /** 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return string The name of the table field to be
     * associated with the class property     
     */
    public function getFieldName() : string
    {
        return $this->fieldName;
    }
    
    /**
     * Retrieves the key type for this field.
     * 
     * Choices are:
     * -----------------------
     * 0 = KEYTYPE_NON_KEY
     * 1 = KEYTYPE_PRIMARY_KEY
     * 2 = KEYTYPE_FOREIGN_KEY
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return int
     */
    public function getKeyType() : int
    {
        return $this->keyType;
    }
    
    /**
     * Sets the key type for this field.
     * 
     * Choices are:
     * -----------------------
     * 0 = KEYTYPE_NON_KEY
     * 1 = KEYTYPE_PRIMARY_KEY
     * 2 = KEYTYPE_FOREIGN_KEY
     *      * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param int $keyType Refer to the KEYTYPE constants
     * @return void
     */
    public function setKeyType(int $keyType) : void
    {
        $this->keyType = $keyType;
    }
    
    /**
     * Retrieves an array of all table relationships for 
     * the associated field.  May include primary key 
     * relationships, too.
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return array
     */
    public function getTableRelations() : array
    {
        return $this->tableRelations;
    }
    
    /**
     * Sets the array of all table relationships for
     * the associated field.  Foreign key relationships are
     * mandatory, and primary key relationships are optional.
     *
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return array
     */
    public function setTableRelatiobns(array $tableRelations) : void
    {
        $this->tableRelations = $tableRelations;
    }
    
    /**
     * Accepts a peroid-delimited string of the related table's name,
     * followed by the related field.  
     * 
     * For example, if the current field
     * is a foreign key that relates to another table's primary key, and that
     * foreign table is called `order`, and the primary key for that table is
     * `order_id`, then the string parameter could be written as "order.order_id"
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $tableRelation
     */
    public function addTableRelation(string $tableRelation) : void
    {
        if (substr_count($tableRelation, ".") == 1)
        {
            // get delimiter position
            $pos = strpos($tableRelation, ".");
            
            if ($pos > 0 && $pos < strlen($tableRelation))
            {
                // split the string by its delimiter (a period)
                $tableAndField = explode(".", $tableRelation);
                $table = $tableAndField[0];
                $field = $tableAndField[1];
                $this->tableRelations[$table] = $field;
            }
            else
            {
                throw new Exception("Invalid position of delimiter (period).  Must not be at the beginning or end of the string");
            }
        }
        else
        {
            throw new Exception("Invalid number of delimiters (periods)... Expected one.");
        }
    }
    
    /**
     * Returns the corresponding mysqli bind param type for this field.
     * If not specified, the DataTranslator will attempt to resolve the
     * binding parameter type itself.
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return NULL|string
     */
    public function getBindParamType()
    {
        return $this->bindParamType;
    }
    
    /**
     * Sets the corresponding mysqli bind param type for this field.
     * If not specified, the DataTranslator will attempt to resolve the
     * binding parameter type itself.
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param NULL|string $bindParamType
     */
    public function setBindParamType($bindParamType) : void
    {
        $this->bindParamType = null;
    }
    
    /**
     * OVERRIDDEN METHODS
     */
    
    /**
     * Prints the DataTableMapping as "Class property {class property}
     * maps to table field {tableName}.{fieldName}
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return string
     */
    public function __toString() : string
    {
        return "Class property '$this->classProperty' maps to table field '" . 
            (empty($this->tableName) ? '' : $this->tableName . '.') . "$this->fieldName'";
    }
}

