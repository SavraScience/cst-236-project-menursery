<?php
namespace misd\data;

use Exception;

// TODO: Delete all debugging statements

/**
 * A class to store metadata about a particular
 * database table.  Stores information such as
 * a table's keys, required fields, field defaults,
 * and collations.
 * @author Michael Mason
 * @copyright 2019 Mason Innovative Software Design
 */
class DataTableMetadata
{
    // CONSTANTS
    const PRIMARY_KEY = 'primary';
    const UNIQUE_KEY = 'unique';
    const FOREIGN_KEY = 'foreign';
    
    // INSTANCE VARIABLES
    
    /**
     * The name of the table that metadata
     * is being stored for.
     * @var $tableName string
     */
    private $tableName;
    
    /** 
     * An associative array of table
     * field names and their respective
     * collation in the database.  This
     * may not be a complete list of
     * the table's fields because not
     * every field is given a collation.
     * @var $collations array
     */
    private $collations;
    
    /**
     * An associative array of table
     * field names and their respective
     * default values in the database.
     * This may not be a complete list of
     * the table's fields because not
     * every field is given a collation.
     * @var $defaults array
     */
    private $defaults;
    
    /**
     * A simple array of fields whose field
     * types could potentially be used to
     * describe a tuple in the table.  These
     * will be primarily used to resolve
     * foreign keys to a string during data 
     * translation from fully normalized tables
     * whose only other field aside from the
     * primary key is a CHAR or VARCHAR that
     * comprises the full description of data
     * in that table
     * @var $descriptors array
     */
    private $descriptors;
    
    /**
     * An associative of array of field names
     * and their respective data types as
     * specified by the database.
     * @var $fieldTypes array
     */
    private $fieldTypes;
    
    /**
     * An associative array of table key 
     * arrays that each store the field
     * names for fields which serve as a 
     * particular type of key in the database.
     * 
     * In other words, primary keys are stored
     * at array index 'primary', unique keys are 
     * stored at the index 'unique', and foreign
     * keys are stored at the index 'foreign'.
     * 
     * The 'foreign' keys index is special
     * because the key in the array represents
     * a foreign key in the table, and its value
     * is itself an associative array.  Each
     * subarray at a given index matching a
     * foreign key value represents the foreign
     * table as the subarray key and its primary
     * key field as the subarray key's value.
     * The primary key in the foreign table
     * USUALLY matches the foreign key in the
     * referencing table, but not always, so
     * this mechanism allows for foreign key
     * and associated primary keys to be
     * different
     * 
     * @var $keys array
     */   
    private $keys;
    
    /**
     * A simple array of required fields in the
     * table (in other words, declared NOT NULL
     * in the table definition).
     * @var $required array
     */
    private $required;
    
    /**
     * @return array
     */
    public function getDescriptors()
    {
        return $this->descriptors;
    }

    /**
     * @param array $descriptors
     */
    public function setDescriptors($descriptors)
    {
        $this->descriptors = $descriptors;
    }

    /**
     * @return string
     */
    public function getTableName()
    {
        return $this->tableName;
    }

    /**
     * @param string $tableName
     */
    public function setTableName($tableName)
    {
        $this->tableName = $tableName;
    }

    // CONSTRUCTOR
    public function __construct() {}
    
    // PUBLIC PROPERTIES
    /**
     * @return mixed
     */
    public function getCollations()
    {
        return $this->collations;
    }

    /**
     * @return mixed
     */
    public function getDefaults()
    {
        return $this->defaults;
    }

    /**
     * @return mixed
     */
    public function getFieldTypes()
    {
        return $this->fieldTypes;
    }

    /**
     * @return mixed
     */
    public function &getKeys()
    {
        return $this->keys;
    }
    
    /**
     * @return mixed
     */
    public function getRequired()
    {
        return $this->required;
    }

    /**
     * @param mixed $collations
     */
    public function setCollations($collations)
    {
        $this->collations = $collations;
    }

    /**
     * @param mixed $defaults
     */
    public function setDefaults($defaults)
    {
        $this->defaults = $defaults;
    }

    /**
     * @param mixed $fieldTypes
     */
    public function setFieldTypes($fieldTypes)
    {
        $this->fieldTypes = $fieldTypes;
    }

    /**
     * @param mixed $keys
     */
    public function setKeys($keys)
    {
        $this->keys = $keys;
    }

    /**
     * @param mixed $required
     */
    public function setRequired($required)
    {
        $this->required = $required;
    }
    
    // HELPER METHODS
    
    /**
     * Returns the associated foreign table (key) 
     * and primary key (value) pair for a given 
     * foreign key in this table.
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $fkFieldName The name of the foreign
     * key you want to retrieve the map from
     * @return array An associative array in which the key
     * represents the foreign table of the foreign key field
     * passed in as an argument, and the value represents the
     * primary key of the foreign table associated with the
     * foreign key
     */
    public function getForiegnKeyMap(string $fkFieldName) : array
    {
        return $this->keys[self::FOREIGN_KEY][$fkFieldName];
    }
    
    /**
     * Returns the data type of a given field as specified
     * by the database.
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $fieldName The name of the field
     * you want to determine the data type for
     */
    public function getDataType(string $fieldName)
    {
        return $this->fieldTypes[$fieldName];
    }
    
    /**
     * Determines the absolute bare minimum required
     * fields for a table schema.  This qualifies as
     * all fields which are required but do NOT have
     * a default value; it also excludes the 
     * primary key
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return array
     */
    public function getMinRequiredFields() : array
    {
        $defaults = array_keys($this->defaults);
        
        // exclude defaults
        $noDefaults = array_diff($this->required, $defaults);
        
        // retrieve just the primary keys
        $primKeys = $this->keys[self::PRIMARY_KEY];
        
        // return the field list excluding defaults and primary keys
        return array_diff($noDefaults, $primKeys);
    }
    
    /**
     * Retrieve's the table's surrogate, primary key
     * (which is almost always an integer)
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     */
    public function getSurrogatePK()
    {
        if (!empty($this->keys))
        {
            // retrieve just the primary keys
            $primKeys = $this->keys[self::PRIMARY_KEY];
            if (!empty($primKeys))
            {
                if (sizeof($primKeys) == 1)
                {
                    // table has only one PK
                    $pKey = $primKeys[0];
                    $datType = $this->getDataType($pKey);
                    if (stripos($datType, "int") !== FALSE)
                    {
                        // primary key is also an integer
                        return $pKey;
                    }
                }
            }
        }
        return null;
    }
    
    /**
     * Returns a true/false value indicating whether
     * or not the table has a surrogate primary key.
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return bool
     */
    public function hasSurrogatePK() : bool
    {
        if (!empty($this->keys))
        {
            // retrieve just the primary keys
            $primKeys = $this->keys[self::PRIMARY_KEY];
            if (!empty($primKeys))
            {
                //console_log("Primary key array is NOT empty.");
                if (sizeof($primKeys) == 1)
                {
                    // table has only one PK
                    $pKey = $primKeys[0];
                    if ($this->isIntColumn($pKey))
                    {
                        // primary key is also an integer
                        return true;
                    }
                    /*
                    $datType = $this->getDataType($pKey);
                    if (stripos($datType, "int") !== FALSE)
                    {
                        // primary key is also an integer
                        return true;
                    }
                    */
                }
            }
        }
        return false;
    }
    
    public function isPrimaryKey(string $fieldName) : bool
    {
        try 
        {
            // retrieve just the primary keys
            $pKeys = $this->keys[self::PRIMARY_KEY];
            if (!empty($pKeys))
            {
                if (in_array($fieldName, $pKeys)) 
                    return true;
                else
                    return false;
            }
        } 
        catch (Exception $e) 
        {
            // something went wrong
            return false;
        }
    }
    
    public function isForeignKey(string $fieldName) : bool
    {
        try
        {
            // retrieve just the primary keys
            $fKeys = $this->keys[self::FOREIGN_KEY];
            if (!empty($fKeys))
            {
                if (array_key_exists($fieldName, $fKeys))
                    return true;
                else
                    return false;
            }
            return false;
        }
        catch (Exception $e)
        {
            // something went wrong
            return false;
        }
    }
    
    public function isSurrogatePK(string $fieldName) : bool
    {
        try
        {
            // retrieve just the primary keys
            $pKeys = $this->keys[self::PRIMARY_KEY];
            if (!empty($pKeys))
            {
                if (sizeof($pKeys) == 1)
                {
                    // table has only one PK
                    if ($fieldName == $pKeys[0])
                        return true;
                    else
                        return false;
                }
            }
        } 
        catch (Exception $e)
        {
            // something went wrong
            return false;
        }
    }
    
    
    public function isRequired(string $fieldName) : bool
    {
        // get minimum required fields
        $required = $this->getMinRequiredFields();
        foreach ($required as $reqField)
        {
            if ($fieldName == $reqField)
                return true;
        }
        return false;
    }
    
    /**
     * Indicates TRUE/FALSE as to whether the table
     * $fieldName is a CHAR or VARCHAR data type
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $fieldName
     * @return bool
     */
    public function isCharColumn(string $fieldName) : bool
    {
        $datType = $this->getDataType($fieldName);
        if (stripos($datType, "char") !== FALSE)
        {
            return true;
        }
        return false;
    }
    
    /**
     * Indicates TRUE/FALSE as to whether the table
     * $fieldName is an INT data type
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $fieldName
     * @return bool
     */
    public function isIntColumn(string $fieldName) : bool
    {
        $datType = $this->getDataType($fieldName);
        if (stripos($datType, "int") !== FALSE)
        {
            return true;
        }
        return false;
    }
    
    /**
     * Attempts to return a unique descriptor field for this table
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return string|NULL If a descriptor could be resolved,
     * returns the field name that represents the descriptor
     * of the table (a unique string value that describes the
     * entity as a whole)
     */
    public function tryGetSoloDescriptor()
    {
        if (!empty($this->descriptors))
        {
            if (sizeof($this->descriptors) == 1)
            {
                $descriptor = $this->descriptors[0];
                
                if ($this->isRequired($descriptor))
                {
                 return $descriptor;
                }
                
                console_log("No matching descriptors could be found for object of String type");
                return null;
            }
        }
    }
}

