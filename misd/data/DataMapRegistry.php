<?php
namespace misd\data;

/**
 * A class intended to hold an array of metadata 
 * about various tables as they are used.  This
 * psuedo-cache reduces expensive database calls
 * to retrieve information about a table more than
 * once when combined with the table-aware DAO.
 * 
 * @author Michael Mason
 * @copyright 2019 Mason Innovative Software Design
 */
class DataMapRegistry
{
    // CONSTANTS
    private const MAP_REGISTRY_KEY = 'MAP_REGISTRY';
    
    // PUBLIC STATIC METHODS
    /**
     * Registers a DataTableMetadata object to the registry
     * for later recall
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param DataTableMetadata $map The metadata to be stored
     */
    public static function registerMap(DataTableMetadata $map): void
    {
        if (!isset($_SESSION)) session_start();
        $tableName = $map->getTableName();
        //console_log("Registering map for table ''...");
        $_SESSION[self::MAP_REGISTRY_KEY][$tableName] = $map;
        
        // debugging
        if (isset($_SESSION[self::MAP_REGISTRY_KEY]))
        {
            //console_log("Map successfully registered!");
        }
    }
    
    /**
     * Checks to see if a table matching a given table
     * name is currently stored in the session. Returns
     * true if a matching table was found, false, if not.
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $tableName
     * @return bool
     */
    public static function hasMapForTable(string $tableName) : bool
    {
        if (!isset($_SESSION)) session_start();
        //console_log("Looking for table '$tableName' in registry...");
        if (isset($_SESSION[self::MAP_REGISTRY_KEY]))
        {
            $metaTableMap = $_SESSION[self::MAP_REGISTRY_KEY];
            if (array_key_exists($tableName, $metaTableMap))
            {
                //console_log("Map exists for table $tableName!");
                return true;
            }
            //console_log("No map found for table $tableName...");
        }
        //console_log("Map registry not set!");
        return false;
    }
    
    /**
     * Returns the metadata about a given table using its
     * name as the key to look up in the registry.
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $tableName
     * @return DataTableMetadata|NULL
     */
    public static function getMapForTable(string $tableName)
    {
        if (!isset($_SESSION)) session_start();
        $metaTableMap = $_SESSION[self::MAP_REGISTRY_KEY];
        if (self::hasMapForTable($tableName))
            return $metaTableMap[$tableName];
        else
            return null;
    }
}

