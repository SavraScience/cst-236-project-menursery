<?php

    /**
     * A simple class that stores basic information about a webpage, such as
     * its name and path, that also provides a static function to check whether
     * or not the current page stored as a global is equal to $this.
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     */

    namespace misd\web;
    
    class Webpage
    {        
        // INSTANCE  VARIABLES
        private $name, $path;
        
        // CONSTRUCTOR
        public function __construct(string $name, string $path)
        {
            $this->name = $name;
            $this->path = $path;
        }
        
        // STATIC FUNCTIONS
        
        /**
         * @todo Add summary and variable descriptions
         * @author Michael Mason
         * @copyright 2019 Mason Innovative Software Design
         * @param Webpage $pageToCheck
         * @return boolean
         */
        static function isCurrentPage(Webpage $pageToCheck)
        {
            if($_ENV[Controller::CURR_PAGE_ID] == $pageToCheck->getName())
                return true;
            else
                return false;
        }
        
        // FUNCTIONS
        public function getName()
        {
            return $this->name;
        }
        public function getPath()
        {
            return Controller::appPartialUri() . $this->path;
        }
    }

?>

