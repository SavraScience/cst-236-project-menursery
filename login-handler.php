<?php

    /**
     * A login handler that uses the SecurityService class to authenticate,
     * then uses the static Controller class to redirect if successful
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @todo Validate and sanitize input before deploying!
     */

    use misd\web\Controller;
    use misd\security\SecurityService;

    // imports
    require_once "_config/a_config.php";
    require_once 'autoloader.php';

    $username = $_POST['txtUsername'];
    $password = $_POST['txtPassword'];
    $authServ = new SecurityService($username, $password);
    
    // authenticate user
    if ($authServ->login())
    {
        // login successful
        if (SecurityService::isCurrUserAdmin())
        {
            // -- redirect to administrative panel
            Controller::redirect('admin/admin-panel');
        }
        else 
        {
            // -- redirect to homepage
            Controller::redirect('index');
        }
    }
    else
    {
        // login failed -- attempt to redirect to originating page with errors
        echo "Uh-oh... Try again.";
    }
    
?>