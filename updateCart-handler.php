<?php

    // imports
    require_once 'autoloader.php';
    use inc\business\CartService;
    use inc\web\MeNurseryCache;
    use misd\web\Controller;
    
    // CONSTANTS
    define('TIMID', 'timid');
    define('QTY', 'newQty');
    
    // get post input
    if (isset($_POST[TIMID]) && isset($_POST[QTY]))
    {
        // initialize variables
        $timid = $_POST[TIMID];
        $newQty = $_POST[QTY];
        
        // debugging
        //echo "Updating cart item '$timid' to quantity $newQty...";
        
        // get the current user's shopping cart
        $cart = MeNurseryCache::get(MeNurseryCache::SESSKEY_SHOPPING_CART);
        
        if (!is_null($cart))
        {
            // cart retrieved from session cache
            // -- resolve cart item
            /** @var $cart \inc\models\ShoppingCartModel */
            $cartItem = $cart->resolveItem($timid);
            
            if (!is_null($cartItem))
            {
                // debugging
                //echo "Cart Item resolved from the user's shopping cart!";
                
                // change $cartItem's quantity
                $cartItem->setQuantity($newQty);
                
                // debugging
                //echo "Cart Item's quantity changed to " . $cartItem->getQuantity() . "!";
                
                // attempt to update the cart item in the database
                $service = new CartService();
                $result = $service->saveCartItem($cartItem);
                
                if ($result)
                {
                    // update successful
                    Controller::requireOnce("loadShoppingCart.php");
                }
                else
                {
                    // update failed
                    echo 0;
                }
            }
            else
            {
                // debugging
                //echo "Could not resolve cart item from user's shopping cart!";
                echo 0;
            }
        }
        else
        {
            // debugging
            //echo "Could not resolve user's shopping cart in Session Cache...";
            echo 0;
        }
    }
?>