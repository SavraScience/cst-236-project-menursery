<?php
    
    // imports
    require_once 'autoloader.php';
    use inc\business\CartService;
    use inc\models\ProductModel;
    use inc\models\ShoppingCartModel;
    use inc\web\MeNurseryCache;
    use misd\security\SecurityService;
    use misd\web\SessionCache;
    use inc\models\ShoppingCartLineItemModel;
use inc\data\UserDao;
    
    SessionCache::persistSession();
    
    // CONSTANTS
    define('CTL_TIMID', 'timid');    

    // fetch the clicked item
    if (isset($_POST[CTL_TIMID]))
    {
        $timid = $_POST[CTL_TIMID];
        //console_log("Temporary in-memory ID = $timid");
        
        // resolve the temporary ID to the session cache
        $product = new ProductModel();
        $product->setTempId($timid);
        $product = MeNurseryCache::resolve(MeNurseryCache::SESSKEY_PRODUCTS, $product);
        
        /** @var $product ProductModel */
        $productId = $product->getId();
        
        if ($productId > 0)
        {
            // product resolved
            // -- initialize CartService
            $service = new CartService();
            
            // -- get the current user
            $currUser = SecurityService::getCurrentUser();
            
            // -- check to see if a shopping cart exists in the session
            $cart = MeNurseryCache::get(MeNurseryCache::SESSKEY_SHOPPING_CART);
            
            if (is_null($cart))
            {
                if (!is_null($currUser))
                {
                    // attempt to retrieve cart for user in database
                    $userDao = new UserDao();
                    $user = $userDao->findById($currUser->getUserId());
                    
                    $cart = $service->getCartForUser($user);
                    if (is_null($cart))
                    {
                        // create new "Shopping Cart"
                        $cart = new ShoppingCartModel();
                    }
                }
                else
                {
                    // create new "Shopping Cart"
                    $cart = new ShoppingCartModel();
                }
            }
            
            // re-register cart in session cache
            MeNurseryCache::register(MeNurseryCache::SESSKEY_SHOPPING_CART, $cart);
            
            // debuggging
            //console_log($cart);
            //console_log("Searching for Product ID (int) $productId");
            
            // build a shopping cart item
            $cartItem = $cart->getItem($productId);
            if (is_null($cartItem))
            {
                //console_log("Creating new shopping cart item...");
                $cartItem = new ShoppingCartLineItemModel();
                $cartItem->setProductId($productId);
                $cartItem->setQuantity(1);
            }
            else
            {
                // debugging
                //console_log("Cart item was found in cart...");
            }

            if (!is_null($currUser))
            {
                // -- set the user for this cart
                $userId = $currUser->getUserId();
                $cart->setUserId($userId);
                $cartItem->setUserId($userId);
                
                // attempt to save the item to the cart (in the DB)
                //console_log("Attempting to save product to user's cart...");
                if ($service->saveCartItem($cartItem))
                {
                    // add the cart item to the in-memory cart
                    $cart->addItem($productId, 1);
                    
                    echo true;
                    //console_log("Product added to user's cart!");
                    exit(0);
                }
                else
                {
                    // failed to save item to cart
                    //console_log("Couldn't save the item to the user's cart...");
                }
            }
            else
            {
                // add the cart item to the in-memory cart
                $cart->addItem($productId, 1);
                echo false;
                //console_log("Product added to visitor's cart!");
                exit(0);
            }
        }
        else
        {
            echo false;
            //console_log("Product could not be resolved in the session cache...");
            exit(0);
        }
    }
    else
    {
        // debugging only
        //console_log("Temporary in-memory ID not set!");
    }
?>