<?php

/**
 * An administrative panel for managing users & products
 * @author Michael Mason
 * @copyright 2019 Mason Innovative Software Design
 */

// imports
use misd\web\Controller;
use misd\security\SecurityService;
use inc\business\OrderService;
use inc\models\OrderModel;

// configuration file
chdir("../");
require_once "_config/a_config.php";

// CONSTANTS
define('START_DATE', 'startDate');
define('END_DATE', 'endDate');

// security checkpoint
if (!SecurityService::isCurrUserAdmin())
{
    // redirect to forbidden page
    //Controller::redirect("index");
    Controller::includeOnce("inc/page-parts/403.php");
    exit();
}

// page metadata
$_ENV[Controller::CURR_PAGE_ID] = "Administrative Panel";
$_ENV[Controller::CURR_PAGE_TITLE] = "MeNusery - Admin Panel";

// get submitted form values if they exist
$startDate = isset($_GET[START_DATE]) ? $_GET[START_DATE] : "";
$endDate = isset($_GET[END_DATE]) ? $_GET[END_DATE] : "";

// get order data
/** @var $orders \inc\models\OrderModel[] */
$orderService = new OrderService();
$orders = $orderService->getOrdersForDateRange($startDate, $endDate);

?>

<!DOCTYPE html>
<html>
	<head>
		<?php 
		  Controller::includeOnce("inc/page-parts/head-meta-content.php"); 
        ?>
	</head>
	<body>
		<div class="page-wrap">
    		<!-- HEADER -->
    		<?php Controller::requireOnce('inc/page-parts/header.php'); ?>
    		
    		<!-- NAVIGATION -->
    		<?php Controller::requireOnce('inc/page-parts/nav.php'); ?>
		</div>
		
		<!-- PAGE CONTENT -->
		<div class="page-wrap">
    		<h2>Admin Panel</h2>
    		<hr />
 			<div class="container-fluid">
 				<div class="row">
 					<div class="col-12  col-md-4">
             			<div class="btn-group">
                			<button type="submit" formaction="<?php echo Controller::resolvePath("admin/displayUsers.php"); ?>" formmethod="get"
                				class="btn btn-primary"
            				>Manage Users</button>
                			<button type="submit" formaction="<?php echo Controller::resolvePath("admin/displayProducts.php"); ?>" formmethod="get"
                				class="btn btn-primary"
                			>Manage Products</button>
            			</div>
        			</div>
        			<div class="col-12 mt-3  col-md-8 mt-md-0 p-0">
            			<div class="container-fluid d-inline-block">
                			<h5 class="d-inline-block mt-1 mb-0 mr-3">Sales Reports</h5>
                			<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="get" 
                				class="d-inline-block py-1 px-3 border rounded"
                			>
                				<div class="form-group d-inline-block mb-0 mr-3">
                					<label class="mb-0">Start Date</label>
                					<input name="startDate" type="date" value="<?php echo $startDate; ?>" 
                						class="form-control" />
                				</div>
                				<div class="form-group d-inline-block mb-0 mr-3">
                					<label class="mb-0">End Date</label>
                					<input name="endDate" type="date" value="<?php echo $endDate; ?>"
                						class="form-control" />
                				</div>
                				<button type="submit" class="btn btn-success mt-3 mt-lg-0">Generate Report</button>
                			</form>
            			</div> <!-- END -->
        			</div> <!-- END .col (Sales Reports -->
    			</div> <!-- END .row -->
    			<?php 
    			     
    			     if (
			             (isset($_GET['startDate']) && !empty($_GET['startDate'])) ||
    			         (isset($_GET['endDate']) && !empty($_GET['endDate']))
			         ) 
    			     { 
    			        // locale
    			        $locale = 'en-US';
    			        $currCode = 'USD';
    			        $currFmt = new NumberFormatter($locale, NumberFormatter::CURRENCY);
    			        
                        // initialize variables
                        $totalItems = 0;
                        $grandTotal = 0;
                        
                        // format header dates
                        $dateFmt = new DateTime($startDate);
                        $startDateFmt = $dateFmt->format('m/d/Y');
                        $dateFmt = new DateTime($endDate);
                        $endDateFmt = $dateFmt->format('m/d/Y');
			    ?>
    			<div class="row">
    				<div id="sales-data-tbl" class="container border mt-5 p-4 rounded">
						<table class="table">
							<caption class="text-center" style="
							     caption-side: top;
							     font-size: 2em;
							     font-weight: bold;
						     ">
								Sales Report for Period <?php echo $startDateFmt; ?> thru <?php echo $endDateFmt; ?>
							</caption>
							<thead>
								<tr>
									<th class="text-center">Order Date</th>
									<th class="text-center">Items Purchased</th>
									<th class="text-right">Purchase Total</th>
								</tr>
							</thead>
							<tbody>
								<?php 
								    foreach($orders as $order)
								    {
								        // get order info
								        $dateFmt = new DateTime($order->getDate());
								        $date = $dateFmt->format('m-d-Y');
								        $numItems = 0;
								        foreach ($order->getItems() as $lineItem)
								        {
								            $numItems += $lineItem->getQuantity();   
								        }
								        $purchaseTotal = $order->getGrandTotal();
								        $purchaseTotalFmt = $currFmt->formatCurrency($purchaseTotal, $currCode);
								        
								        // tally totals
								        $totalItems += $numItems;
								        $grandTotal += $purchaseTotal;
								        
								        echo <<<ML
                                            <tr>
                                                <td class="text-center">$date</td>
                                                <td class="text-center">$numItems</td>
                                                <td class="text-right">$purchaseTotalFmt</td>
                                            </tr>
ML;
								    }
								    
								    // format totals
								    $grandTotalFmt = $currFmt->formatCurrency($grandTotal, $currCode);
								?>
							</tbody>
							<tfoot>
								<tr>
									<td class="text-center"><b>Totals for period :</b></td>
									<td class="text-center"><b><?php echo $totalItems; ?></b></td>
									<td class="text-right"><b><?php echo $grandTotalFmt; ?></b></td>
								</tr>
							</tfoot>
						</table>
    				</div>
    			</div>
    			<?php } ?>
			</div> <!-- END .container-fluid -->
		</div> <!-- END .page-wrap -->
		
		<!-- FOOTER -->
		<?php Controller::includeOnce('inc/page-parts/footer.php'); ?>
	</body>
</html>