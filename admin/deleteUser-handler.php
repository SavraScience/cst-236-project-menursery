<?php

// imports
use inc\data\UserDao;
use inc\models\UserModel;
use inc\web\MeNurseryCache;
use misd\web\Controller;

chdir('../');
require_once 'autoloader.php';
require_once 'inc/misc-functions.php';

// CONSTANTS
define('TEMP_ID', 'timid');

//console_log("Calling deleteProduct-handler.php...");

if (isset($_POST[TEMP_ID]))
{
    // retrieve the clicked product's temporary ID
    $tempId = $_POST[TEMP_ID];
    
    // create a temporary product object for resolving
    $tmpUser = new UserModel();
    $tmpUser->setTempId($tempId);
    
    // resolve the ID against the session's cache
    /** @var $user ProductModel */
    //console_log("Looking for temp id '$tempId'");
    $user = MeNurseryCache::resolve(MeNurseryCache::SESSKEY_USERS, $tmpUser);
    
    // call the business service's delete database call
    $dao = new UserDao();
    $result = $dao->deleteById($user->getId());
    
    if ($result)
    {
        // deletion successful
        // --remove the row from the table
        //console_log("Deletion successful");
        //Controller::redirect('Display Products');
        echo true;
    }
    else
    {
        // deletion unsuccessful
        // --display a message to the user
        console_log("Deletion unsuccessful");
        echo <<<ML
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Deletion Unsuccessful</strong>
                </div> <!-- END alert -->
ML;
    }
}
?>