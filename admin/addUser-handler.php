<?php

// imports
use inc\data\UserDao;
use inc\models\UserModel;
use misd\security\InputSanitizer;
use misd\web\Controller;
use misd\web\FormErrors;

chdir('../');
require_once 'autoloader.php';
require_once 'inc/misc-functions.php';


/* FORM FIELD LEGEND */
# selUserRole
# txtFirstName
# txtLastName
# txtEmail
# txtUsername
# txtPassword

// constants
define('FLD_NAME_ROLE', 'selUserRole');
define('FLD_NAME_FIRSTNAME', 'txtFirstName');
define('FLD_NAME_LASTNAME', 'txtLastName');
define('FLD_NAME_EMAIL', 'txtEmail');
define('FLD_NAME_USERNAME', 'txtUsername');
define('FLD_NAME_PASSWORD', 'txtPassword');

// sanitize input
InputSanitizer::sanitizeStringArray($_POST);

// collect the data on the page
$userRole       = isset($_POST[FLD_NAME_ROLE])          ? $_POST[FLD_NAME_ROLE]         : null;
$firstName      = isset($_POST[FLD_NAME_FIRSTNAME])     ? $_POST[FLD_NAME_FIRSTNAME]    : null;
$lastName       = isset($_POST[FLD_NAME_LASTNAME])      ? $_POST[FLD_NAME_LASTNAME]     : null;
$email          = isset($_POST[FLD_NAME_EMAIL])         ? $_POST[FLD_NAME_EMAIL]        : null;
$username       = isset($_POST[FLD_NAME_USERNAME])      ? $_POST[FLD_NAME_USERNAME]     : null;
$password       = isset($_POST[FLD_NAME_PASSWORD])      ? $_POST[FLD_NAME_PASSWORD]     : null;

// create a new product from the form data
$newUser = new UserModel();

// validate new user
$errors = new FormErrors();
try
{
    $newUser->setUserRole($userRole);
}
catch (Exception $ex)
{
    $errors->addError(FLD_NAME_ROLE, $userRole, $ex->getMessage());
}

try
{
    $newUser->setFirstName($firstName);
}
catch (Exception $ex)
{
    $errors->addError(FLD_NAME_FIRSTNAME, $firstName, $ex->getMessage());
}

try
{
    $newUser->setLastName($lastName);
}
catch (Exception $ex)
{
    $errors->addError(FLD_NAME_LASTNAME, $lastName, $ex->getMessage());
}

try
{
    $newUser->setEmail($email);
}
catch (Exception $ex)
{
    $errors->addError(FLD_NAME_EMAIL, $email, $ex->getMessage());
}

try
{
    $newUser->setUsername($username);
}
catch (Exception $ex)
{
    $errors->addError(FLD_NAME_USERNAME, $username, $ex->getMessage());
}

//$newUser->setZones($zones);

try
{
    $newUser->setPassword($password);
}
catch (Exception $ex)
{
    $errors->addError(FLD_NAME_PASSWORD, $password, $ex->getMessage());
}

if ($errors->hasErrors())
{
    // redirect back to form page
    console_log("Redirecting to addUser.php");
    Controller::redirect('addUser');
}
// END user assembly & validation

// insert new product into the database
$dao = new UserDao();
$result = $dao->insert($newUser);

// if insertion was successful, redirect to the Display Users page
if ($result)
{
    Controller::redirect('admin/displayUsers.php');
}
console_log("Oops!  Something went wrong...");

?>