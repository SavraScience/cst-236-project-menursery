<?php

    /**
     * An administrative form for adding products
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     */
    
    // imports
    use misd\security\SecurityService;
    use misd\web\Controller;
    
    // configuration file
    chdir('../');
    require_once "_config/a_config.php";
    
    // page metadata
    $_ENV[Controller::CURR_PAGE_ID] = "Add Product";
    $_ENV[Controller::CURR_PAGE_TITLE] = "MeNusery - Add Product";
    
    // security checkpoint
    if (!SecurityService::isCurrUserAdmin())
    {
        // redirect to forbidden page
        //Controller::redirect("index");
        Controller::includeOnce("inc/page-parts/403.php");
        exit();
    }
?>
    
<!DOCTYPE html>
<html>
	<head>
	<?php 
		  Controller::includeOnce("inc/page-parts/head-meta-content.php");
    ?>
    	<link rel="stylesheet" href="/me-nursery/css/product.css">
	</head>
	<body>
		<div class="page-wrap">
		
    		<!-- HEADER -->
    		<?php Controller::requireOnce('inc/page-parts/header.php'); ?>
    		
    		<!-- NAVIGATION -->
    		<?php Controller::requireOnce('inc/page-parts/nav.php'); ?>
    		
		</div>
		
		<!-- PAGE CONTENT -->
		<div class="page-wrap">
    		<h2>Add a New Product</h2>
    		<hr />
    		
    		<!-- Product Form -->
			<?php Controller::requireOnce('admin/showProductForm.php'); ?>
			
		</div>

		<!-- FOOTER -->
		<?php Controller::includeOnce('inc/page-parts/footer.php'); ?>	
		
		<!-- scripts -->
<!-- 		<script type="text/javascript" src="js/products.js"></script> -->
		<script type="text/javascript" src="<?php echo Controller::resolvePath('js/productForm.js'); ?>"></script>
	</body>
</html>