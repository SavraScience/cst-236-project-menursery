<?php

    // imports
    use inc\business\ProductService;
    use inc\web\MeNurseryCache;
    use inc\models\ProductModel;
    use misd\web\Controller;
    
    // constants
    define('SEARCH', 'q');
    define('SORT_BY', 'sort');
    
    /**
     * Sort function
     * @param $a ProductModel
     * @param $b ProductModel
     */
    function scopedCompare($a, $b)
    {
        // get the GET sort parameter, if applicable
        if (isset($_GET[SORT_BY]))
        {
            
        }
        else
        {
            // sort on vegetation types
            return strcmp($a->getVegetationType()->getDescription(), $b->getVegetationType()->getDescription());
        }
    }
    
    // initialize variables
    $service = new ProductService();
    $products = null;
    $searchString = "";
    
    if (isset($_GET[SEARCH]))
    {
        // build criteria
        $searchString = $_GET[SEARCH];
        $products = $service->find($searchString);
    }
    else
    {
        // get all products from the database
        $products = $service->getAll();
    }
    
    console_log('$products is a type of ' . gettype($products));
    if (is_array($products)) console_log("There was " . count($products) . " products returned from your query");
        
    // register the data in session cache
    MeNurseryCache::register(MeNurseryCache::SESSKEY_PRODUCTS, $products);
?>

<!-- Search Bar -->
<div class="search-container">
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="get">
    	<input id="search-products" name="<?php echo SEARCH; ?>" type="search" value="<?php echo $searchString; ?>" 
    		placeholder="Search products..." class="form-control" 
    		<?php if (isset($_GET[SEARCH])) echo "autofocus"; ?> 
		/>
    	<button type="submit" class="bg-primary"><i class="fas fa-search"></i></button>
    </form>
</div>

<?php 
    if (is_array($products) && count($products) > 0)
    {
        // sort the array
        usort($products, 'scopedCompare');
        
        // get image resources
        $iconEdit = Controller::resolvePath("res/img/edit-32.png");
        $iconDelete = Controller::resolvePath("res/img/delete-32.png");
        
        // resolve action paths
        $editAction = Controller::resolvePath("admin/editProduct.php");
        $deleteAction = Controller::resolvePath("admin/deleteProduct-handler.php");
        
        // populate table with data
        echo <<<ML
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th>Type</th>
                        <th>Product Description</th>
                        <th>Category</th>
                        <th>Price</th>

                        <!-- Empty Placeholders for images -->
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
ML;
        foreach($products as $product)
        {
            /** @var $product ProductModel */
            // -- get information about product
            $timid = $product->getTempId();
            $vegType = $product->getVegetationType()->getDescription();
            $desc = $product->getDescription();
            
            $categories = array();
            $subType = $product->getSubtype();
            $variety = $product->getVariety();
            if (!is_null($subType)) $categories['subtype'] = $subType->__toString();
            if (!is_null($variety)) $categories['variety'] = $variety->__toString();
            $category = join(", ", $categories);
            
            $price = "$" . number_format($product->getPrice(), 2);
            
            // -- display info in table
            echo <<<ML
                <tr id="$timid">
                    <td>$vegType</td>
                    <td>$desc</td>
                    <td>$category</td>
                    <td>$price</td>
                    <td class="icon-column">
                        <form action="$editAction" method="get">
                            <img src="$iconEdit" height="22" width="22" title="Edit Product" class="edit-icon interactive-icon" />
                            <input type="hidden" name="timid" value="$timid" />
                        </form>
                    </td>
                    <td class="icon-column">
                        <form action="$deleteAction" method="post">
                            <img src="$iconDelete" height="22" width="22" title="Delete Product" class="delete-icon interactive-icon" />
                            <input type="hidden" name="timid" value="$timid" />
                        </form>
                    </td>
                </tr>
ML;
        }
        
        // count # of products returned
        $productCount = count($products);
        echo <<<ML
                <tfoot>
                    <tr>
                        <td colspan="6" style="font-weight: 500; text-align: right;">$productCount Total Products</td>
                    </tr>
                </tfoot>
            </table>
ML;
    }
?>