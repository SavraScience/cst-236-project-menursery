<?php

/**
 * An administrative form for editing users
 * @author Michael Mason
 * @copyright 2019 Mason Innovative Software Design
 */

// imports
use inc\models\UserModel;
use inc\web\MeNurseryCache;
use misd\security\SecurityService;
use misd\web\Controller;

// configuration file
chdir('../');
require_once "_config/a_config.php";

// page metadata
$_ENV[Controller::CURR_PAGE_ID] = "Add User";
$_ENV[Controller::CURR_PAGE_TITLE] = "MeNusery - Add User";

// security checkpoint
if (!SecurityService::isCurrUserAdmin())
{
    // redirect to forbidden page
    //Controller::redirect("index");
    Controller::includeOnce("inc/page-parts/403.php");
    exit();
}

// get temporary ID
$timid = "";
if (isset($_GET['timid']))
{
    $timid = $_GET['timid'];
    $tempUser = new UserModel();
    $tempUser->setTempId($timid);
    
    // attempt to resolve the in-memory product
    /** @var $user ProductModel */
    $user = MeNurseryCache::resolve(MeNurseryCache::SESSKEY_USERS, $tempUser);
}

// TODO: Change all loaders to first check the cache before loading from the database
?>
    
<!DOCTYPE html>
<html>
	<head>
	<?php 
		  Controller::includeOnce("inc/page-parts/head-meta-content.php");
    ?>
    	<link rel="stylesheet" href="/me-nursery/css/user.css">
	</head>
	<body>
		<div class="page-wrap">
		
    		<!-- HEADER -->
    		<?php Controller::requireOnce('inc/page-parts/header.php'); ?>
    		
    		<!-- NAVIGATION -->
    		<?php Controller::requireOnce('inc/page-parts/nav.php'); ?>
    		
		</div>
		
		<!-- PAGE CONTENT -->
		<div class="page-wrap">
    		<h2>Add User</h2>
    		<hr />
    		<form id="user-form" method="post" action="addUser-handler.php">
    			<div id="user-container">
        			
        			<!-- User Role -->
        			<label id="lbl-user-role" for="sel-user-role" class="form-lbl">User Role</label>
        			<select id="sel-user-role" name="selUserRole" class="form-ctl" required autofocus="autofocus">
        				<option value="2">User</option>
        				<option value="1">Admin</option>
        			</select>     			
        			
        			<!-- First Name -->
        			<label id="lbl-firstname" for="txt-firstname" class="form-lbl">First Name</label>
        			<input id="txt-firstname" name="txtFirstName" type="text" class="form-ctl" required />
        			
        			<!-- Last Name -->
        			<label id="lbl-lastname" for="lbl-lastname" class="form-lbl">Last Name</label>
        			<input id="txt-lastname" name="txtLastName" type="text" class="form-ctl" required />
        			
        			<!-- E-mail Address -->
        			<label id="lbl-email" for="txt-email" class="form-lbl">Email address</label>
        			<input id="txt-email" name="txtEmail" type="text" class="form-ctl" required />
        			
        			<!-- Username -->
        			<label id="lbl-username" for="txt-username" class="form-lbl">Username</label>
        			<input id="txt-username" name="txtUsername" type="text" class="form-ctl" required />
        			
        			<!-- Password -->
        			<label id="lbl-password" for="txt-password" class="form-lbl">Password</label>
        			<input id="txt-password" name="txtPassword" type="password" class="form-ctl" required />
        			
        			<button id="btn-register" type="submit" class="btn btn-primary">Add</button>
    			</div>
    		</form> 
		</div>

		<!-- FOOTER -->
		<?php Controller::includeOnce('inc/page-parts/footer.php'); ?>	
		
		<!-- scripts -->
<!-- 		<script type="text/javascript" src="js/products.js"></script> -->
		<script type="text/javascript" src="<?php echo Controller::resolvePath('js/products.js'); ?>"></script>
	</body>
</html>