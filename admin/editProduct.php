<?php

/**
 * An administrative form for editing products
 * @author Michael Mason
 * @copyright 2019 Mason Innovative Software Design
 */

// imports
use inc\models\ProductModel;
use inc\web\MeNurseryCache;
use misd\security\SecurityService;
use misd\web\Controller;

// configuration file
chdir('../');
require_once "_config/a_config.php";

// security checkpoint
if (!SecurityService::isCurrUserAdmin())
{
    // redirect to forbidden page
    //Controller::redirect("index");
    Controller::includeOnce("inc/page-parts/403.php");
    exit();
}

// page metadata
$_ENV[Controller::CURR_PAGE_ID] = "Edit Product";
$_ENV[Controller::CURR_PAGE_TITLE] = "MeNusery - Edit Product";

// get temporary ID
$timid = "";
if (isset($_GET['timid'])) 
{
    $timid = $_GET['timid'];
    $tempProduct = new ProductModel();
    $tempProduct->setTempId($timid);
    
    // attempt to resolve the in-memory product
    /** @var $product ProductModel */
    $product = MeNurseryCache::resolve(MeNurseryCache::SESSKEY_PRODUCTS, $tempProduct);
}

// TODO: Change all loaders to first check the cache before loading from the database
?>
    
<!DOCTYPE html>
<html>
	<head>
	<?php 
		  Controller::includeOnce("inc/page-parts/head-meta-content.php");
    ?>
    	<link rel="stylesheet" href="/me-nursery/css/product.css">
	</head>
	<body>
		<div class="page-wrap">
		
    		<!-- HEADER -->
    		<?php Controller::requireOnce('inc/page-parts/header.php'); ?>
    		
    		<!-- NAVIGATION -->
    		<?php Controller::requireOnce('inc/page-parts/nav.php'); ?>
    		
		</div>
		
		<!-- PAGE CONTENT -->
		<div class="page-wrap">
    		<h2>Edit Product</h2>
    		<hr />
    		<form id="product-form" method="post" action="editProduct-handler.php">
    			<div id="product-container">
        			
        			<div class="form-section-heading">
        				<h3>Product Information</h3>
        			</div>
        			
        			<input type="hidden" name="timid" value="<?php echo $timid; ?>" />
        			
        			<div id="grid-group">
        				<!-- Vegatation Type -->
            			<label id="lbl-vegetation" for="sel-vegetation" class="form-lbl" accesskey="v"><u>V</u>egetation Type</label>
            			<select id="sel-vegetation" name="selVegetationType" class="form-ctl" autofocus required
            				value="<?php 
            				    $desc = $product->getVegetationType()->getDescription();
            				    echo (is_null($desc) ? "" : $desc);
        				    ?>" 
        				>
            				<?php Controller::requireOnce('loadVegetationSelect.php'); ?>
            			</select>
            			        
            			<!-- Subtype -->
            			<label id="lbl-subtype" for="sel-subtype" class="form-lbl" accesskey="u">S<u>u</u>btype</label>
            			<select id="sel-subtype" name="selSubtype" class="form-ctl" 
            				value="<?php 
            				    if (!is_null($product->getSubtype()))
            				    {
                				    $desc = $product->getSubtype()->getDescription();
                				    echo (is_null($desc) ? "" : $desc);
            				    }
        				    ?>" 
            				>
            				<!-- use AJAX to populate subtypes -->
            			</select>
            			<div id="loading-img-subtype" class="spinner-border spinner-border-sm text-primary" style="visibility: hidden;"></div>
            			 
            			<!-- Varieties -->
            			<label id="lbl-variety" for="sel-variety" class="form-lbl" accesskey="v"><u>V</u>ariety</label>
            			<select id="sel-variety" name="selVariety" class="form-ctl"
            				value="<?php 
            				    if (!is_null($product->getVariety()))
            				    {
                				    $desc = $product->getVariety()->getDescription(); 
                				    echo (is_null($desc) ? "" : $desc);
            				    }
        				    ?>"
            			>
            				<!-- use AJAX to populate varieties -->
            			</select>
            			<div id="loading-img-variety" class="spinner-border spinner-border-sm text-primary" style="visibility: hidden;"></div>
            			 
            			<!-- Bloom Period -->
            			<label id="lbl-bloom-pd" for="sel-bloom-pd" class="form-lbl" accesskey="b"><u>B</u>loom Period</label>
            			<select id="sel-bloom-pd" name="selBloomPeriod" class="form-ctl"
            				value="<?php 
            				    if (!is_null($product->getBloomPeriod()))
            				    {
            				        $desc = $product->getBloomPeriod()->getDescription();
            				        echo (is_null($desc) ? "" : $desc);
            				    }
        				    ?>"
            			>
            				<?php 
            				    Controller::requireOnce('loadBloomPeriodSelect.php');
            				?>
            			</select>
            			 
            			<!-- Flower Color -->
            			<label id="lbl-flower-color" for="sel-flower-color" class="form-lbl" accesskey="c">Flower <u>C</u>olor</label>
            			<select id="sel-flower-color" name="selFlowerColor" class="form-ctl"
            				value="<?php 
            				    if (!is_null($product->getFlowerColor()))
            				    {
            				        $color = $product->getFlowerColor()->getColor();
            				        echo (is_null($color) ? "" : $color);
            				    }
        				    ?>">
            				<?php 
            				Controller::requireOnce('loadFlowerColorSelect.php');
            				?>
            			</select>
        			
            			<!-- Product Description-->
            			<label id="lbl-product-desc" for="txt-product-desc" class="form-lbl" accesskey="r">P<u>r</u>oduct Description</label>
            			<textarea id="txt-product-desc" name="txtProductDesc" class="form-ctl" rows="5" cols="100" required
            			><?php 
            			     $desc = $product->getDescription();
            			     echo (is_null($desc) ? "" : $desc);
            			 ?></textarea>    			    			 
            			 
            			<!-- Height & Height Units -->
            			<label id="lbl-height" for="txt-height" class="form-lbl" accesskey="h"><u>H</u>eight</label>
            			<div id="sub-grid">
                			<input id="txt-height" name="txtHeight" class="form-ctl" type="text" 
                				value="<?php echo $product->getHeight(); ?>"
                			/>
                			<select id="sel-height-units" name="selHeightUnits" class="form-ctl"
                				value="<?php echo $product->getHeightUnit(); ?>" 
                			>
                				<option value="1">ft</option>
                				<option value="2">in</option>
                			</select> 
                			 
            			     <!-- Spread & Spread Units -->
                			<label id="lbl-spread" for="txt-spread" class="form-lbl" accesskey="s"><u>S</u>pread</label>
                			<input id="txt-spread" name="txtSpread" class="form-ctl" type="text" 
                				value="<?php echo $product->getSpread(); ?>"
                			/>
                			<select id="sel-spread-units" name="selSpreadUnits" class="form-ctl"
                				value="<?php echo $product->getSpreadUnit(); ?>"        			
                			>
                				<option value="1">ft</option>
                				<option value="2">in</option>
                			</select> 
            			</div>
            			 
            			<!-- Price -->
            			<label id="lbl-price" for="txt-price" class="form-lbl" accesskey="p">Price</label>
            			<input id="txt-price" name="txtPrice" class="form-ctl" type="text" required 
            				value="<?php echo $product->getPrice(); ?>"
            			/>
            			
            			<!-- Product Image -->
            			<label id="lbl-product-image" for="txt-product-image" class="form-lbl" accesskey="i">Product <u>I</u>mage</label>
            			<input id="txt-product-image" name="imgProductImage" type="url" class="form-ctl" />
            			 
            			<button id="btn-save" type="submit" class="btn btn-primary">Save</button>
        			</div>
    			</div>
    			<img id="img-product" src="" height="160" width="160" class="hidden" />
    		</form>
			
		</div>

		<!-- FOOTER -->
		<?php Controller::includeOnce('inc/page-parts/footer.php'); ?>	
		
		<!-- scripts -->
<!-- 		<script type="text/javascript" src="js/products.js"></script> -->
		<script type="text/javascript" src="<?php echo Controller::resolvePath('js/products.js'); ?>"></script>
	</body>
</html>