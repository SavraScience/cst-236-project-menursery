<?php
    
    /**
     * An administrative form for managing products
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     */

    // imports
    use misd\security\SecurityService;
    use misd\web\Controller;
    
    // configuration file
    chdir('../');
    require_once "_config/a_config.php";

    // security checkpoint
    if (!SecurityService::isCurrUserAdmin())
    {
        // redirect to forbidden page
        //Controller::redirect("index");
        Controller::includeOnce("inc/page-parts/403.php");
        exit();
    }
    
    // page metadata
    $_ENV[Controller::CURR_PAGE_ID] = "Display Products";
    $_ENV[Controller::CURR_PAGE_TITLE] = "Admin - Display Products";

?>
    
<!DOCTYPE html>
<html>
	<head>
	<?php 
		  Controller::includeOnce("inc/page-parts/head-meta-content.php");
    ?>
    	<link rel="stylesheet" href="<?php echo Controller::resolvePath("css/products.css"); ?>">
	</head>
	<body>
		<div class="page-wrap">
		
    		<!-- HEADER -->
    		<?php Controller::requireOnce('inc/page-parts/header.php'); ?>
    		
    		<!-- NAVIGATION -->
    		<?php Controller::requireOnce('inc/page-parts/nav.php'); ?>
		
		</div>
		<!-- PAGE CONTENT -->
		<div class="page-wrap">
    		<h2>Manage Products</h2>
    		<hr />
    		
    		<?php Controller::includeOnce('admin/loadProductsTable.php'); ?>
    
    		<form action="<?php echo Controller::resolvePath('admin/addProduct'); ?>" method="get">
    			<button type="submit" accesskey="a" class="btn btn-primary"><u>A</u>dd Product</button>
    		</form>
		</div>

		<!-- FOOTER -->
		<?php Controller::includeOnce('inc/page-parts/footer.php'); ?>	
		
		<!-- scripts -->
		<script src="<?php echo Controller::resolvePath("js/displayProducts.js"); ?>"></script>
	</body>
</html>