<?php

// imports
use inc\data\ProductDao;
use inc\models\ProductModel;
use inc\models\SubtypeModel;
use inc\models\VegetationTypeModel;
use inc\models\VarietyModel;
use inc\models\BloomPeriodModel;
use inc\models\FlowerColorModel;
use misd\security\InputSanitizer;
use misd\web\Controller;
use inc\web\MeNurseryCache;
use misd\web\FormErrors;

chdir('../');
require_once 'autoloader.php';
require_once 'inc/misc-functions.php';


/* FORM FIELD LEGEND */
# selVegetationType
# selSubtype
# selVariety
# selBloomPeriod
# selFlowerColor
# selZones
# txtProductDesc
# txtHeight
# selHeightUnits
# txtSpread
# selSpreadUnits
# txtPrice
# imgProductImage

// constants
define('FLD_NAME_VEG_TYPE', 'selVegetationType');
define('FLD_NAME_SUBTYPE', 'selSubtype');
define('FLD_NAME_VARIETY', 'selVariety');
define('FLD_NAME_BLOOM_PD', 'selBloomPeriod');
define('FLD_NAME_FLOWER_COL', 'selFlowerColor');
define('FLD_NAME_ZONES', 'selZones');
define('FLD_NAME_PRODUCT_DESC', 'txtProductDesc');
define('FLD_NAME_HEIGHT', 'txtHeight');
define('FLD_NAME_HEIGHT_UNITS', 'selHeightUnits');
define('FLD_NAME_SPREAD', 'txtSpread');
define('FLD_NAME_SPREAD_UNITS', 'selSpreadUnits');
define('FLD_NAME_PRICE', 'txtPrice');
define('FLD_NAME_PRODUCT_IMG_URL', 'imgProductImageUrl');
define('FLD_NAME_PRODUCT_IMG_FILE', 'imgProductImageFile');

// sanitize input
InputSanitizer::sanitizeStringArray($_POST);

// collect the data on the page
$vegTypeDesc        = isset($_POST[FLD_NAME_VEG_TYPE])          ? html_entity_decode($_POST[FLD_NAME_VEG_TYPE])     : null;
$subTypeDesc        = isset($_POST[FLD_NAME_SUBTYPE])           ? html_entity_decode($_POST[FLD_NAME_SUBTYPE])      : null;
$varietyDesc        = isset($_POST[FLD_NAME_VARIETY])           ? html_entity_decode($_POST[FLD_NAME_VARIETY])      : null;
$bloomPdDesc        = isset($_POST[FLD_NAME_BLOOM_PD])          ? html_entity_decode($_POST[FLD_NAME_BLOOM_PD])     : null;
$flowerColDesc      = isset($_POST[FLD_NAME_FLOWER_COL])        ? html_entity_decode($_POST[FLD_NAME_FLOWER_COL])   : null;
$productDesc        = isset($_POST[FLD_NAME_PRODUCT_DESC])      ? $_POST[FLD_NAME_PRODUCT_DESC]                     : null;
$height             = isset($_POST[FLD_NAME_HEIGHT])            ? $_POST[FLD_NAME_HEIGHT]                           : null;
$heightUnits        = isset($_POST[FLD_NAME_HEIGHT_UNITS])      ? $_POST[FLD_NAME_HEIGHT_UNITS]                     : null;
$spread             = isset($_POST[FLD_NAME_SPREAD])            ? $_POST[FLD_NAME_SPREAD]                           : null;
$spreadUnits        = isset($_POST[FLD_NAME_SPREAD_UNITS])      ? $_POST[FLD_NAME_SPREAD_UNITS]                     : null;
$price              = isset($_POST[FLD_NAME_PRICE])             ? $_POST[FLD_NAME_PRICE]                            : null;
$productImgFile     = isset($_FILES[FLD_NAME_PRODUCT_IMG_FILE]) && 
                      ($_FILES[FLD_NAME_PRODUCT_IMG_FILE]['error'] == UPLOAD_ERR_OK)  
                                                                ? $_FILES[FLD_NAME_PRODUCT_IMG_FILE]                : null;
$productImgUrl      = isset($_POST[FLD_NAME_PRODUCT_IMG_URL])   ? $_POST[FLD_NAME_PRODUCT_IMG_URL]                  : null;
$productImgContent  = null;

// first, process the iamge
if (!empty($productImgFile))
{
    // determine destination path
    $destinationPath = Controller::resolvePath("tmp/");
        
    // get file metadata
    $orgFilename = $_FILES[FLD_NAME_PRODUCT_IMG_FILE]['name'];
    $tmpFilename = $_FILES[FLD_NAME_PRODUCT_IMG_FILE]['tmp_name'];
    $newFilepath = $destinationPath . basename($orgFilename);
    
    // attempt to transfer the uploaded file to a temporary destination
    $result = move_uploaded_file($tmpFilename, $newFilepath);
    
    if ($result)
    {
        $productImgContent = file_get_contents($newFilepath);
    }
    else
    {
        console_log("Something went wrong...");
        exit(0);
    }
}
elseif (!is_null($productImgUrl) && $productImgUrl != "")
{
    // get URL file extension
    $fileInfo = pathinfo($productImgUrl);
    if (isset($fileInfo['extension']))
    {
        $ext = $fileInfo['extension'];
        console_log("Extension for image = $ext");
     
        // check to see if extension is a valid image
        if (isValidImageType($ext))
        {
            // -- optionally check to see if the URL is valid, first
            //file_get_contents_curl($productImgUrl);
            //$tmpPath = "img-1." . $ext;
            $result = save_external_file_curl($productImgUrl);

            if (!is_null($result))
            {
                console_log("Image saved successfully to '$result'...");
                $productImgContent = file_get_contents($result);
            }
            else 
            {
                exit(0);
            }
        }
    }
}

// create a new product from the form data
$newProduct = new ProductModel();

// create each component of the ProductModel, first
// and resolve each to existing identity
$vegType = null;
if (!is_null($vegTypeDesc))
{
    $vegType = new VegetationTypeModel();
    $vegType->setDescription($vegTypeDesc);
    $vegType = MeNurseryCache::resolve(MeNurseryCache::SESSKEY_VEG_TYPES, $vegType);
    /** @var $vegType VegetationTypeModel */
    //console_log('$vegType->id = ' . $vegType->getId());
}

$subType = null;
if (!is_null($subTypeDesc))
{
    $subType = new SubTypeModel();
    $subType->setVegetationType($vegType);
    $subType->setDescription($subTypeDesc);
    $subType = MeNurseryCache::resolve(MeNurseryCache::SESSKEY_SUBTYPES, $subType);
    /** @var $subType SubtypeModel */
    //console_log('$subType->id = ' . $subType->getId());
}

$variety = null;
if (!is_null($varietyDesc))
{
    $variety = new VarietyModel();
    $variety->setVegetationType($vegType);
    $variety->setDescription($varietyDesc);
    $variety = MeNurseryCache::resolve(MeNurseryCache::SESSKEY_VARIETIES, $variety);
    /** @var $variety VarietyModel */
    //console_log('$variety->id = ' . $variety->getId());
}

$bloomPeriod = null;
if (!is_null($bloomPdDesc))
{
    $bloomPeriod = new BloomPeriodModel();
    $bloomPeriod->setDescription($bloomPdDesc);
    $bloomPeriod = MeNurseryCache::resolve(MeNurseryCache::SESSKEY_BLOOM_PERIODS, $bloomPeriod);
    /** @var $bloomPeriod BloomPeriodModel */
    //console_log('$bloomPeriod->id = ' . $bloomPeriod->getId());
}

$flowerColor = null;
if (!is_null($flowerColDesc))
{
    $flowerColor = new FlowerColorModel();
    $flowerColor->setColor($flowerColDesc);
    $flowerColor = MeNurseryCache::resolve(MeNurseryCache::SESSKEY_FLOWER_COLORS, $flowerColor);
    /** @var $flowerColor FlowerColorModel */
    //console_log('$flowerColor->id = ' . $flowerColor->getId());
}

// assemble (and validate) new product
$errors = new FormErrors();
try 
{
    $newProduct->setVegetationType($vegType);
} 
catch (Exception $ex) 
{
    $errors->addError(FLD_NAME_VEG_TYPE, $vegType, $ex->getMessage());
}

try
{
    $newProduct->setSubtype($subType);
}
catch (Exception $ex)
{
    $errors->addError(FLD_NAME_SUBTYPE, $subType, $ex->getMessage());
}

try
{
    $newProduct->setVariety($variety);
}
catch (Exception $ex)
{
    $errors->addError(FLD_NAME_VARIETY, $variety, $ex->getMessage());
}

try
{
    $newProduct->setBloomPeriod($bloomPeriod);
}
catch (Exception $ex)
{
    $errors->addError(FLD_NAME_BLOOM_PD, $bloomPeriod, $ex->getMessage());
}

try
{
    $newProduct->setFlowerColor($flowerColor);
}
catch (Exception $ex)
{
    $errors->addError(FLD_NAME_FLOWER_COL, $flowerColor, $ex->getMessage());
}

//$newProduct->setZones($zones);

try
{
    $newProduct->setDescription($productDesc);
}
catch (Exception $ex)
{
    $errors->addError(FLD_NAME_PRODUCT_DESC, $productDesc, $ex->getMessage());
}

try
{
    
    $newProduct->setHeight($height);
}
catch (Exception $ex)
{
    $errors->addError(FLD_NAME_HEIGHT, $height, $ex->getMessage());
}

try
{
    $newProduct->setHeightUnit($heightUnits);
}
catch (Exception $ex)
{
    $errors->addError(FLD_NAME_HEIGHT_UNITS, $heightUnits, $ex->getMessage());
}

try
{
    $newProduct->setSpread($spread);
}
catch (Exception $ex)
{
    $errors->addError(FLD_NAME_SPREAD, $spread, $ex->getMessage());
}

try
{
    $newProduct->setSpreadUnit($spreadUnits);
}
catch (Exception $ex)
{
    $errors->addError(FLD_NAME_SPREAD_UNITS, $spreadUnits, $ex->getMessage());
}

try
{
    $newProduct->setPrice($price);
}
catch (Exception $ex)
{
    $errors->addError(FLD_NAME_PRICE, $price, $ex->getMessage());
}

try
{
    $newProduct->setImage($productImgContent);
}
catch (Exception $ex)
{
    $errors->addError(FLD_NAME_PRODUCT_IMG, $productImgContent, $ex->getMessage());
}

if ($errors->hasErrors())
{
    // redirect back to form page
    console_log("Redirecting to addProduct.php");
    Controller::redirect('addProduct');
}
// END product assembly & validation

// insert new product into the database
$dao = new ProductDao();
$result = $dao->insert($newProduct);

if ($result)
{
    // attempt to delete temporary file
    unlink($newFilepath);
    
    // redirect to Diplay Products page
    Controller::redirect('admin/displayProducts.php');
}
console_log("Oops!  Something went wrong...");

?>