<?php

    // imports
    use misd\web\Controller;

?>

    		<form id="product-form" method="post" action="addProduct-handler.php" enctype="multipart/form-data">
    			<div id="product-container">
        			
        			
        			<div id="" class="form-section-heading">
        				<h3>Product Information</h3>
        			</div>
        			
        			<!-- Vegatation Type -->
        			<div id="grid-group">
            			<label id="lbl-vegetation" for="sel-vegetation" class="form-lbl" accesskey="v"><u>V</u>egetation Type</label>
            			<select id="sel-vegetation" name="selVegetationType" class="form-ctl" autofocus required>
            				<?php Controller::requireOnce('loadVegetationSelect.php'); ?>
            			</select>
        
            			<!-- Subtype -->
            			<label id="lbl-subtype" for="sel-subtype" class="form-lbl" accesskey="u">S<u>u</u>btype</label>
            			<select id="sel-subtype" name="selSubtype" class="form-ctl">
            				<!-- use AJAX to populate subtypes -->
            			</select>
            			<div id="loading-img-subtype" class="spinner-border spinner-border-sm text-primary" style="visibility: hidden;"></div>
            			 
            			<!-- Varieties -->
            			<label id="lbl-variety" for="sel-variety" class="form-lbl" accesskey="v"><u>V</u>ariety</label>
            			<select id="sel-variety" name="selVariety" class="form-ctl">
            				<!-- use AJAX to populate varieties -->
            			</select>
            			<div id="loading-img-variety" class="spinner-border spinner-border-sm text-primary" style="visibility: hidden;"></div>
            			 
            			<!-- Bloom Period -->
            			<label id="lbl-bloom-pd" for="sel-bloom-pd" class="form-lbl" accesskey="b"><u>B</u>loom Period</label>
            			<select id="sel-bloom-pd" name="selBloomPeriod" class="form-ctl">
            				<?php 
            				    Controller::requireOnce('loadBloomPeriodSelect.php');
            				?>
            			</select>
            			 
            			<!-- Flower Color -->
            			<label id="lbl-flower-color" for="sel-flower-color" class="form-lbl" accesskey="c">Flower <u>C</u>olor</label>
            			<select id="sel-flower-color" name="selFlowerColor" class="form-ctl">
            				<?php 
            				Controller::requireOnce('loadFlowerColorSelect.php');
            				?>
            			</select>
            			
            			<!-- Product Description-->
            			<label id="lbl-product-desc" for="txt-product-desc" class="form-lbl" accesskey="r">P<u>r</u>oduct Description</label>
            			<textarea id="txt-product-desc" name="txtProductDesc" class="form-ctl" rows="5" cols="100" required></textarea>    			    			 
            			<br />
            			 
            			<!-- Height & Height Units -->
            			<label id="lbl-height" for="txt-height" class="form-lbl" accesskey="h"><u>H</u>eight</label>
        				<div id="sub-grid">
                			<input id="txt-height" name="txtHeight" class="form-ctl" type="text" />
                			<select id="sel-height-units" name="selHeightUnits" class="form-ctl">
                				<option value="feet">ft</option>
                				<option value="inches">in</option>
                			</select> 
                			 
                			<!-- Spread & Spread Units -->
                			<label id="lbl-spread" for="txt-spread" class="form-lbl">Spread</label>
                			<input id="txt-spread" name="txtSpread" class="form-ctl" type="text" />
                			<select id="sel-spread-units" name="selSpreadUnits" class="form-ctl">
                				<option value="feet">ft</option>
                				<option value="inches">in</option>
                			</select> 
        				</div>
        			
            			<!-- Price -->
            			<label id="lbl-price" for="txt-price" class="form-lbl" accesskey="p">Price</label>
            			<input id="txt-price" name="txtPrice" class="form-ctl" type="text" required />
            			<br />
            			
            			<!-- Product Image -->
            			<label id="lbl-product-image" for="txt-product-image" class="form-lbl" accesskey="u">Product Image <u>U</u>RL</label>
            			<input id="txt-product-image" name="imgProductImageUrl" type="url" class="form-ctl" />
            			<div id="img-product-container"></div>
            			 
            			 <p id="product-image-alt"><b>OR</b></p>
            			 
            			 <label id="lbl-product-image-file" for="product-image-file" class="form-lbl" accesskey="i">Product <u>I</u>mage</label>
            			 <input id="product-image-file" name="imgProductImageFile" type="file" accept=".jpg, .jpeg, .png" />
            			 
            			 <!-- Save Button -->
            			<button id="btn-save" type="submit" accesskey="s" class="btn btn-primary"><u>S</u>ave</button>
        			</div>
    			</div>
    		</form>