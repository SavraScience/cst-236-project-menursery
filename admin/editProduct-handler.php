<?php

// imports
use inc\data\ProductDao;
use inc\models\ProductModel;
use inc\models\SubtypeModel;
use inc\models\VegetationTypeModel;
use inc\models\VarietyModel;
use inc\models\BloomPeriodModel;
use inc\models\FlowerColorModel;
use misd\security\InputSanitizer;
use misd\web\Controller;
use inc\web\MeNurseryCache;
use misd\web\FormErrors;

chdir('../');
require_once 'autoloader.php';
require_once 'inc/misc-functions.php';


/* FORM FIELD LEGEND */
# selVegetationType
# selSubtype
# selVariety
# selBloomPeriod
# selFlowerColor
# selZones
# txtProductDesc
# txtHeight
# selHeightUnits
# txtSpread
# selSpreadUnits
# txtPrice
# imgProductImage

// constants
define('FLD_NAME_TEMP_ID', 'timid');
define('FLD_NAME_VEG_TYPE', 'selVegetationType');
define('FLD_NAME_SUBTYPE', 'selSubtype');
define('FLD_NAME_VARIETY', 'selVariety');
define('FLD_NAME_BLOOM_PD', 'selBloomPeriod');
define('FLD_NAME_FLOWER_COL', 'selFlowerColor');
define('FLD_NAME_ZONES', 'selZones');
define('FLD_NAME_PRODUCT_DESC', 'txtProductDesc');
define('FLD_NAME_HEIGHT', 'txtHeight');
define('FLD_NAME_HEIGHT_UNITS', 'selHeightUnits');
define('FLD_NAME_SPREAD', 'txtSpread');
define('FLD_NAME_SPREAD_UNITS', 'selSpreadUnits');
define('FLD_NAME_PRICE', 'txtPrice');
define('FLD_NAME_PRODUCT_IMG', 'imgProductImage');

// sanitize input
InputSanitizer::sanitizeStringArray($_POST);

// collect the data on the page
$timid          = isset($_POST[FLD_NAME_TEMP_ID])                                   ? $_POST[FLD_NAME_TEMP_ID]                          : null;
$vegTypeDesc    = isset($_POST[FLD_NAME_VEG_TYPE])                                  ? html_entity_decode($_POST[FLD_NAME_VEG_TYPE])     : null;
$subTypeDesc    = isset($_POST[FLD_NAME_SUBTYPE])                                   ? html_entity_decode($_POST[FLD_NAME_SUBTYPE])      : null;
$varietyDesc    = isset($_POST[FLD_NAME_VARIETY])                                   ? html_entity_decode($_POST[FLD_NAME_VARIETY])      : null;
$bloomPdDesc    = isset($_POST[FLD_NAME_BLOOM_PD])                                  ? html_entity_decode($_POST[FLD_NAME_BLOOM_PD])     : null;
$flowerColDesc  = isset($_POST[FLD_NAME_FLOWER_COL])                                ? html_entity_decode($_POST[FLD_NAME_FLOWER_COL])   : null;
$zones          = isset($_POST[FLD_NAME_ZONES])                                     ? $_POST[FLD_NAME_ZONES]                            : null;
$productDesc    = isset($_POST[FLD_NAME_PRODUCT_DESC])                              ? $_POST[FLD_NAME_PRODUCT_DESC]                     : null;
$height         = (isset($_POST[FLD_NAME_HEIGHT]) && $_POST[FLD_NAME_HEIGHT] != '') ? $_POST[FLD_NAME_HEIGHT]                           : null;
$heightUnits    = isset($_POST[FLD_NAME_HEIGHT_UNITS])                              ? $_POST[FLD_NAME_HEIGHT_UNITS]                     : null;
$spread         = (isset($_POST[FLD_NAME_SPREAD]) && $_POST[FLD_NAME_SPREAD] != '') ? $_POST[FLD_NAME_SPREAD]                           : null;
$spreadUnits    = isset($_POST[FLD_NAME_SPREAD_UNITS])                              ? $_POST[FLD_NAME_SPREAD_UNITS]                     : null;
$price          = isset($_POST[FLD_NAME_PRICE])                                     ? $_POST[FLD_NAME_PRICE]                            : null;
$productImgUrl  = isset($_POST[FLD_NAME_PRODUCT_IMG])                               ? $_POST[FLD_NAME_PRODUCT_IMG]                      : null;

// first, resolve the product against the session cache
/** @var $product ProductModel */
$product = new ProductModel();
$product->setTempId($timid);
$product = MeNurseryCache::resolve(MeNurseryCache::SESSKEY_PRODUCTS, $product);
//console_log($product);

if ($product->getId() > 0)
{
    $errors = new FormErrors();
    
    // check to see which values are different, and override where necessary
    $vegType = $product->getVegetationType();
    if ($vegTypeDesc != '')
    {
        if (is_null($vegType) || (!is_null($vegType) && $vegType->getDescription() != $vegTypeDesc))
        {
            // resolve Vegetation Type
            $vegType = new VegetationTypeModel();
            $vegType->setDescription($vegTypeDesc);
            $vegType = MeNurseryCache::resolve(MeNurseryCache::SESSKEY_VEG_TYPES, $vegType);
            $product->setVegetationType($vegType);
        }
    }
    else
    {
        if (!is_null($vegType)) $product->setVegetationType(null);
    }

    $subType = $product->getSubtype();
    if ($subTypeDesc != '')
    {
        if (is_null($subType) || (!is_null($subType) && $subType->getDescription() != $subTypeDesc))
        {
            // resolve Subtype
            $subType = new SubtypeModel();
            $subType->setDescription($subTypeDesc);
            $subType = MeNurseryCache::resolve(MeNurseryCache::SESSKEY_SUBTYPES, $subType);
            $product->setSubtype($subType);
        }
    }
    else
    {
        if (!is_null($subType)) $product->setSubtype(null);
    }
    
    $variety = $product->getVariety();
    if ($varietyDesc != '')
    {
        console_log($varietyDesc);
        if (is_null($variety) || (!is_null($variety) && $variety->getDescription() != $varietyDesc))
        {
            // resolve Variety
            $variety = new VarietyModel();
            $variety->setDescription($varietyDesc);
            $variety = MeNurseryCache::resolve(MeNurseryCache::SESSKEY_VARIETIES, $variety);
            if ($variety->getId() > 0) console_log("Resolved variety!");
            $product->setVariety($variety);
        }
    }
    else
    {
        if (!is_null($variety)) $product->setVariety(null);
    }
    
    $bloomPd = $product->getBloomPeriod();
    if ($bloomPdDesc != '')
    {
        if (is_null($bloomPd) || (!is_null($bloomPd) && $bloomPd->getDescription() != $bloomPdDesc))
        {
            // resolve Variety
            $bloomPd = new BloomPeriodModel();
            $bloomPd->setDescription($bloomPdDesc);
            $bloomPd = MeNurseryCache::resolve(MeNurseryCache::SESSKEY_BLOOM_PERIODS, $bloomPd);
            $product->setBloomPeriod($bloomPd);
        }
    }
    else
    {
        if (!is_null($bloomPd)) $product->setBloomPeriod(null);
    }

    $flowerCol = $product->getFlowerColor();
    if ($flowerColDesc != '')
    {
        if (is_null($flowerCol) || (!is_null($flowerCol) && $flowerCol->getColor() != $flowerColDesc))
        {
            // resolve Variety
            $flowerCol = new FlowerColorModel();
            $flowerCol->setColor($flowerColDesc);
            $flowerCol = MeNurseryCache::resolve(MeNurseryCache::SESSKEY_FLOWER_COLORS, $flowerCol);
            $product->setFlowerColor($flowerCol);
        }
    }
    else
    {
        if (!is_null($flowerCol)) $product->setFlowerColor(null);
    }

    // skip zones
    if ($product->getDescription() != $productDesc)
    {
        $product->setDescription($productDesc);
    }
    $product->setHeight($height);
    $product->setHeightUnit($heightUnits);
    $product->setSpread($spread);
    $product->setSpreadUnit($spreadUnits);
    $product->setPrice($price);
    
    /*
    // finally, process the iamge
    if ($productImgUrl != "")
    {
        // get URL file extension
        $fileInfo = pathinfo($productImgUrl);
        if (isset($fileInfo['extension']))
        {
            $ext = $fileInfo['extension'];
            console_log("Extension for image = $ext");
            
            // check to see if extension is a valid image
            if (isValidImageType($ext))
            {
                // -- optionally check to see if the URL is valid, first
                $fileData = file_get_contents_curl($productImgUrl);
                $tmpPath = "img-1." . $ext;
                
                // copy the file data to new file
                file_put_contents($tmpPath, $fileData);
                if (file_exists($tmpPath))
                {
                    //console_log("Image saved successfully to '$tmpPath'...");
                }
            }
        }
    }
    */
    
    // update product in database
    $dao = new ProductDao();
    $result = $dao->update($product);
    
    if ($result)
    {
        Controller::redirect('admin/displayProducts.php');
    }
}


console_log("Oops!  Something went wrong...");

?>