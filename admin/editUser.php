<?php

/**
 * An administrative form for editing products
 * @author Michael Mason
 * @copyright 2019 Mason Innovative Software Design
 */

// imports
use inc\models\UserModel;
use inc\web\MeNurseryCache;
use misd\security\SecurityService;
use misd\web\Controller;

// configuration file
chdir('../');
require_once "_config/a_config.php";

// security checkpoint
if (!SecurityService::isCurrUserAdmin())
{
    // redirect to forbidden page
    //Controller::redirect("index");
    Controller::includeOnce("inc/page-parts/403.php");
    exit();
}

// page metadata
$_ENV[Controller::CURR_PAGE_ID] = "Edit User";
$_ENV[Controller::CURR_PAGE_TITLE] = "MeNusery - Edit User";

// get temporary ID
$timid = "";
if (isset($_GET['timid']))
{
    $timid = $_GET['timid'];
    $tempUser = new UserModel();
    $tempUser->setTempId($timid);
    
    // attempt to resolve the in-memory product
    /** @var $user UserModel */
    $user = MeNurseryCache::resolve(MeNurseryCache::SESSKEY_USERS, $tempUser);
}

// TODO: Change all loaders to first check the cache before loading from the database
?>
    
<!DOCTYPE html>
<html>
	<head>
	<?php 
		  Controller::includeOnce("inc/page-parts/head-meta-content.php");
    ?>
    	<link rel="stylesheet" href="/me-nursery/css/user.css">
	</head>
	<body>
		<div class="page-wrap">
		
    		<!-- HEADER -->
    		<?php Controller::requireOnce('inc/page-parts/header.php'); ?>
    		
    		<!-- NAVIGATION -->
    		<?php Controller::requireOnce('inc/page-parts/nav.php'); ?>
    		
		</div>
		
		<!-- PAGE CONTENT -->
		<div class="page-wrap">
    		<h2>Edit User</h2>
    		<hr />
    		<form id="user-form" method="post" action="editUser-handler.php">
    		
    			<input type="hidden" name="timid" value="<?php echo $timid; ?>" />
    		
    			<div id="user-container">
        			
        			<!-- User Role -->
        			<label id="lbl-user-role" for="sel-user-role" class="form-lbl">User Role</label>
        			<select id="sel-user-role" name="selUserRole" class="form-ctl" required autofocus="autofocus"
        				value="<?php echo $user->getUserRole();?>"
        			>
        				<option value="2">User</option>
        				<option value="1">Admin</option>
        			</select>        			
        			
        			<!-- First Name -->
        			<label id="lbl-firstname" for="txt-firstname" class="form-lbl">First Name</label>
        			<input id="txt-firstname" name="txtFirstName" type="text" class="form-ctl" required autofocus="autofocus" 
        				value="<?php echo $user->getFirstName(); ?>"
        			/>
        			
        			<!-- Last Name -->
        			<label id="lbl-lastname" for="lbl-lastname" class="form-lbl">Last Name</label>
        			<input id="txt-lastname" name="txtLastName" type="text" class="form-ctl" required 
        				value="<?php echo $user->getLastName(); ?>"
        			/>
        			
        			<!-- E-mail Address -->
        			<label id="lbl-email" for="txt-email" class="form-lbl">Email address</label>
        			<input id="txt-email" name="txtEmail" type="text" class="form-ctl" required 
        				value="<?php echo $user->getEmail(); ?>"
        			/>
        			
        			<!-- Username -->
        			<label id="lbl-username" for="txt-username" class="form-lbl">Username</label>
        			<input id="txt-username" name="txtUsername" type="text" class="form-ctl" required 
        				value="<?php echo $user->getUsername() ?>"
        			/>
        			
        			<!-- Password -->
        			<label id="lbl-password" for="txt-password" class="form-lbl">New Password</label>
        			<input id="txt-password" name="txtPassword" type="password" class="form-ctl" required 
        				value="<?php echo $user->getPassword() ?>"
        			/>
        			
        			<button id="btn-register" type="submit" class="btn btn-primary">Save</button>
    			</div>
    		</form> 
		</div>

		<!-- FOOTER -->
		<?php Controller::includeOnce('inc/page-parts/footer.php'); ?>	
		
		<!-- scripts -->
<!-- 		<script type="text/javascript" src="js/products.js"></script> -->
		<script type="text/javascript" src="<?php echo Controller::resolvePath('js/users.js'); ?>"></script>
	</body>
</html>