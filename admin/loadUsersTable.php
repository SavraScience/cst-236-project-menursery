<?php

// imports
use inc\business\UserService;
use inc\models\UserModel;
use inc\web\MeNurseryCache;
use misd\web\Controller;

// get all products from the database
$service = new UserService();
$users = $service->getAll();

// register the data in session cache
MeNurseryCache::register(MeNurseryCache::SESSKEY_USERS, $users);

if (is_array($users) && count($users) > 0)
{
    // get image resources
    $iconEdit = Controller::resolvePath("res/img/edit-32.png");
    $iconDelete = Controller::resolvePath("res/img/delete-32.png");
    
    // resolve action paths
    $editAction = Controller::resolvePath("admin/editUser.php");
    $deleteAction = Controller::resolvePath("admin/deleteUser-handler.php");
    
    // populate table with data
    echo <<<ML
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th>Date Created</th>
                        <th>Username</th>
                        <th>Role</th>
                        <th>Name</th>
                        <th>E-mail</th>
                        <!-- Empty Placeholders for images -->
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
ML;
    
    foreach($users as $user)
    {
        // get string-formatted info about user
        /** @var $user UserModel */
        $timid = $user->getTempId();
        $timestamp = new DateTime($user->getDateCreated());
        $dateCreated = $timestamp->format("m/d/Y h:i:s");
        $userName = $user->getUsername();
        $userRole = "";
        switch ($user->getUserRole())
        {
            case 1: 
                $userRole = "Administrator";
                break;
            case 2: 
                $userRole = "User";
                break;
            default: 
                $userRole = "";
        }
        $email = $user->getEmail();
        $fullName = $user->getFirstName() . " " . $user->getLastName();
        
        echo <<<ML
                <tr id="$timid">
                    <td>$dateCreated</td>
                    <td>$userName</td>
                    <td>$userRole</td>
                    <td>$fullName</td>
                    <td>$email</td>
                    <td class="icon-column">
                        <form action="$editAction" method="get">
                            <img src="$iconEdit" height="22" width="22" title="Edit Product" class="edit-icon interactive-icon" />
                            <input type="hidden" name="timid" value="$timid" />
                        </form>
                    </td>
                    <td class="icon-column">
                        <form action="$deleteAction" method="post">
                            <img src="$iconDelete" height="22" width="22" title="Delete Product" class="delete-icon interactive-icon" />
                            <input type="hidden" name="timid" value="$timid" />
                        </form>
                    </td>
                </tr>
ML;
    }
    echo "</table>";
}
?>