<?php

// imports
use inc\data\UserDao;
use inc\models\UserModel;
use misd\security\InputSanitizer;
use misd\web\Controller;
use inc\web\MeNurseryCache;
use misd\web\FormErrors;

chdir('../');
require_once 'autoloader.php';
require_once 'inc/misc-functions.php';

/* FORM FIELD LEGEND */
# selUserRole
# txtFirstName
# txtLastName
# txtEmail
# txtUsername
# txtPassword

// constants
define('FLD_NAME_TEMP_ID', 'timid');
define('FLD_NAME_ROLE', 'selUserRole');
define('FLD_NAME_FIRSTNAME', 'txtFirstName');
define('FLD_NAME_LASTNAME', 'txtLastName');
define('FLD_NAME_EMAIL', 'txtEmail');
define('FLD_NAME_USERNAME', 'txtUsername');
define('FLD_NAME_PASSWORD', 'txtPassword');

// sanitize input
InputSanitizer::sanitizeStringArray($_POST);

// collect the data on the page
$timid          = isset($_POST[FLD_NAME_TEMP_ID])       ? $_POST[FLD_NAME_TEMP_ID]      : null;
$userRole       = isset($_POST[FLD_NAME_ROLE])          ? $_POST[FLD_NAME_ROLE]         : null;
$firstName      = isset($_POST[FLD_NAME_FIRSTNAME])     ? $_POST[FLD_NAME_FIRSTNAME]    : null;
$lastName       = isset($_POST[FLD_NAME_LASTNAME])      ? $_POST[FLD_NAME_LASTNAME]     : null;
$email          = isset($_POST[FLD_NAME_EMAIL])         ? $_POST[FLD_NAME_EMAIL]        : null;
$username       = isset($_POST[FLD_NAME_USERNAME])      ? $_POST[FLD_NAME_USERNAME]     : null;
$password       = isset($_POST[FLD_NAME_PASSWORD])      ? $_POST[FLD_NAME_PASSWORD]     : null;

// first, resolve the user against the session cache
/** @var $user UserModel */
$user = new UserModel();
$user->setTempId($timid);
$user = MeNurseryCache::resolve(MeNurseryCache::SESSKEY_USERS, $user);
//console_log($user);

if ($user->getId() > 0)
{
    $errors = new FormErrors();
    try
    {
        $user->setUserRole($userRole);
    }
    catch (Exception $ex)
    {
        $errors->addError(FLD_NAME_ROLE, $userRole, $ex->getMessage());
    }
    
    try
    {
        $user->setFirstName($firstName);
    }
    catch (Exception $ex)
    {
        $errors->addError(FLD_NAME_FIRSTNAME, $firstName, $ex->getMessage());
    }
    
    try
    {
        $user->setLastName($lastName);
    }
    catch (Exception $ex)
    {
        $errors->addError(FLD_NAME_LASTNAME, $lastName, $ex->getMessage());
    }
    
    try
    {
        $user->setEmail($email);
    }
    catch (Exception $ex)
    {
        $errors->addError(FLD_NAME_EMAIL, $email, $ex->getMessage());
    }
    
    try
    {
        $user->setUsername($username);
    }
    catch (Exception $ex)
    {
        $errors->addError(FLD_NAME_USERNAME, $username, $ex->getMessage());
    }
    
    //$user->setZones($zones);
    
    try
    {
        $user->setPassword($password);
    }
    catch (Exception $ex)
    {
        $errors->addError(FLD_NAME_PASSWORD, $password, $ex->getMessage());
    }
    
    if ($errors->hasErrors())
    {
        // redirect back to form page
        console_log("Redirecting to addUser.php");
        Controller::redirect('addUser');
    }
    // END user assembly & validation
    
    // update product in database
    $dao = new UserDao();
    $result = $dao->update($user);
    
    if ($result)
    {
        //console_log("Update successful!");
        Controller::redirect('admin/displayUsers.php');
    }
}


console_log("Oops!  Something went wrong...");

?>