<?php

    // imports
    require_once 'autoloader.php';
    use misd\web\Controller;
    use misd\web\SessionCache;
    use inc\web\MeNurseryCache;
    use inc\business\OrderService;

    // get the current user
    SessionCache::persistSession();
    
    // retrieve the user's shopping cart
    $cart = MeNurseryCache::get(MeNurseryCache::SESSKEY_SHOPPING_CART);
    
    if (!is_null($cart))
    {
        // debugging
        //console_log("Cart was resolved in session cache!");
        
        $service = new OrderService();
        $result = $service->createOrder($cart);
        
        // debugging
        //console_log("Result of transaction: $result");
                
        if ($result)
        {
            Controller::redirect("orders/order-confirmation.php");
            exit(0);
        }
        else 
        {
            // debugging
            //console_log("Something went wrong... Could not process the order...");
        }
    }
    else
    {
        // debugging
        //console_log("User's cart could not be resolved...");
    }
        
    // something went wrong... redirect to cart
    Controller::redirect("cart.php");
?>