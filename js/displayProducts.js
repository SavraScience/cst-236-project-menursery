"use strict";

/* The JavaScript for admin/displayProduct.php */

/*
var imgEditIcon = document.getElementsByClassName('edit-icon');
var imgDeleteIcon = document.getElementsByClassName('delete-icon');
*/

// add click events for all edit icons 
document.querySelectorAll('.edit-icon').forEach(item => {
  item.addEventListener('click', event => {

    //console.log('You cliked "Edit!"');

    var form = item.closest("form");
    var action = form.action;
    var timid = form.elements[0].value;

    // asynchronous request
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200)
			{
				document.getElementById(timid);
			}
		};    
    form.submit();
  })
});

// add click events for all delete icons 
document.querySelectorAll('.delete-icon').forEach(item => {
  item.addEventListener('click', event => {

    //console.log('You cliked "Delete!"');

    // get the form, data, action, and corresponding ID for use later
    var form = item.closest("form");
    var action = form.action;
    var data = new FormData(form);
    var timid = item.closest("tr").id;
    
    // debugging
    // console.log(action);

    // asynchronous request
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200)
			{
        //console.log(this.responseText);
        if (this.responseText == "1")
        {
          // remove the corresponding row from the table
          document.getElementById(timid).remove();
        }
        else
        {
          // show a dismissable failure message
          document.body.appendChild(this.responseText);
        }
			}
		};    

    xmlhttp.open("POST", action, true);
    xmlhttp.send(data);
    // form.submit();
  })
});