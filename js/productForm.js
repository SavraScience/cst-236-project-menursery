"use strict";

// DECLARATIONS
var currValue;

// -- select boxes
var selVegType 		= document.getElementById('sel-vegetation');
var selSubType 		= document.getElementById('sel-subtype');
var selVariety 		= document.getElementById('sel-variety');
var selBloomPd 		= document.getElementById('sel-bloom-pd');
var selFlowerCol 	= document.getElementById('sel-flower-color');
var selHeightUnit = document.getElementById('sel-height-units');
var selSpreadUnit = document.getElementById('sel-spread-units');

// -- spinners
var spinSubtype = document.getElementById("loading-img-subtype");
var spinVariety = document.getElementById("loading-img-variety");

// -- image url textbox and image container
var txtImageUrl = document.getElementById("txt-product-image");
var productImgPrev = document.getElementById("img-product-container"); 

// IMMEDIATE ACTIONS
// -- Match Static Values to Select Options
// -- for Vegetation Type
resolveSelectValue(selVegType);
// -- for Bloom Period
resolveSelectValue(selBloomPd);
// -- for Flower Color
resolveSelectValue(selFlowerCol);
// -- for Height Unit
resolveSelectValue(selHeightUnit);
// -- for Spread Unit
resolveSelectValue(selSpreadUnit);

function resolveSelectValue(selectElement)
{
	var currValue = selectElement.getAttribute("value");
	//console.log(selectElement.id + " value = " + currValue);
	if (currValue != "")
	{
		for (var i = 0; i < selectElement.options.length; i++)
		{
			var optVal = selectElement.options[i].value;
			if (currValue  ==  optVal) selectElement.options[i].selected = true;
		}
	}
}

// EVENT HANDLERS
// -- Vegetation Select Control
function vegetationOnChange()
{
	// get Vegetation Type's value
	var val = selVegType.options[selVegType.selectedIndex].value;
	
	// debugging
	//console.log("Value of VegType = " + val);
	
	if (val.length == 0)
	{
		//console.log("Clearing Subtype selection...");
		selSubType.innerHTML = "";
		

		return;
	}
	else
	{
		//console.log("Looking up Subtype selection for '" + val + "'...");
		
		// show spinner(s)
		var visibility = "visible";
		spinSubtype.style.visibility = visibility;
		spinVariety.style.visibility = visibility;

		// asynchronous requests
		var xmlhttpSubtypes = new XMLHttpRequest();
		var xmlhttpVarieties = new XMLHttpRequest();
		
		// loading subtypes
		xmlhttpSubtypes.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200)
			{
				// populate subtype options
				selSubType.innerHTML = this.responseText;
				//console.log("Subtype options should have loaded...");

				// resolve subtype
				resolveSelectValue(selSubType);

				spinSubtype.style.visibility = "hidden";
			}
		};

		// loading varieties
		xmlhttpVarieties.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200)
			{
				// populate variety options
				selVariety.innerHTML = this.responseText;
				//console.log("Variety options should have loaded...");

				// resolve subtype
				resolveSelectValue(selVariety);

				spinVariety.style.visibility = "hidden";			
			}
		};

		// load select element values
		var encVal = encodeURIComponent(val);
		var subtypeUri = "/me-nursery/loadSubtypeSelect.php?q=" + encVal;
		var varietyUri = "/me-nursery/loadVarietySelect.php?q=" + encVal;
		
		// debugging
		//console.log("Loading URI '" + uri + "'");
		
		xmlhttpSubtypes.open("GET", subtypeUri, true);
		xmlhttpVarieties.open("GET", varietyUri, true);
		xmlhttpSubtypes.send();
		xmlhttpVarieties.send();
	}
}

// --Product Image Url Textbox
function imageUrlOnChange() 
{
	//console.log("Calling imageUrlOnChange()...");
	var url = txtImageUrl.value;
	if (url != "")
	{
		if (isValidImageUrl(url))
		{
			// show image
			productImgPrev.style.backgroundImage = "url(" + url + ")";
			//productImgPrev.classList.remove("hidden");
			return;
		}
	}
	// else: hide image container
	productImgPrev.style.backgroundImage = "none";
}

/* Attach Events */
window.addEventListener('load', vegetationOnChange, false);
selVegType.addEventListener('change', vegetationOnChange, false);
txtImageUrl.addEventListener('keyup', imageUrlOnChange, false);
txtImageUrl.addEventListener('paste', imageUrlOnChange, false);

// HELPER FUNCTIONS
function isValidImageUrl(url)
{
	return (url.match(/\.(jpeg|jpg|gif|png)$/) != null);
}