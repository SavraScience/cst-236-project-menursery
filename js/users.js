"use strict";

// DECLARATIONS
var currValue;

// -- select boxes
var selUserRole = document.getElementById('sel-user-role');

// IMMEDIATE ACTIONS
// -- Match Static Values to Select Options
// -- for User Role
resolveSelectValue(selUserRole);

function resolveSelectValue(selectElement)
{
	var currValue = selectElement.getAttribute("value");
	//console.log(selectElement.id + " value = " + currValue);
	if (currValue != "")
	{
		for (var i = 0; i < selectElement.options.length; i++)
		{
			var optVal = selectElement.options[i].value;
			if (currValue  ==  optVal) selectElement.options[i].selected = true;
		}
	}
}