"use strict";

$(document).ready(documentOnLoad());
function documentOnLoad() 
{
    // show delete button on hover
    $('#shopping-cart-tbl tbody tr').on('mouseover', function() {

      
      // build selector query
      var select = '#' + this.id + ' img.delete-btn'; 

      // get image element
      var img = $(select);

      if (img.length != 0)
      {
        //console.log('Showing image at row #' + this.id);
        img.css('opacity', '1');
      }
    });

    // dim delete button on hover
    $('#shopping-cart-tbl tbody tr').on('mouseout', function() {

      // build selector query
      var select = '#' + this.id + ' img.delete-btn'; 

      // get image element
      var img = $(select);

      if (img.length != 0)
      {
        //console.log('Hiding image at row #' + this.id);
        img.css('opacity', '0.33');
      }

    } );

    // update quantity 'change' handler
    $('input.cart-item-qty').on('change', function() {
      
      // get the row element
      var row = this.closest('tr');

      if (row.length != 0)
      {
        console.log("You updated row #" + row.id + " to value " + this.value);

        // send asyncronous request to the server to update the shopping cart
        $.post('updateCart-handler.php', { timid: row.id, newQty: this.value }, function(data) {
          
          // debugging
          //console.log("Sent asyncronous post request to updateCart-handler.php");
          //console.log("Response = '" + data + "'");

          // get toast message
          var toast = $('.toast');

          if (data != 0)
          {
            // refresh table & reload document
            $('#shopping-cart').html(data);
            documentOnLoad();

            // modify toast
            // -- change message
            $('.toast toast-body').text("Cart updated");
            // -- change bg
            toast.removeClass('bg-danger');
            toast.addClass('bg-info');
          }
          else
          {
            // modify toast
            // -- change message
            $('.toast toast-body').text("Failed to update cart");            
            // -- change bg            
            toast.removeClass('bg-info');
            toast.addClass('bg-danger');
          }

          // display result as toast
          toast.toast({delay: 2000});
          toast.toast('show');
        });
      }
    });

    // delete button 'click' handler
    $('img.delete-btn').on('click', function() {

      // get the row element
      var row = this.closest('tr');

      if (row.length != 0)
      {
        //console.log("You clicked delete for row #" + row.id);

        // send asyncronous request to the server to delete the selected row
        $.post('removeShoppingCartItem-handler.php', { timid: row.id }, function(data) {
          if (data == 0)
          {
            console.log('Could not delete!');
          }
          else
          {
            // reload shopping cart
            //console.log("Reloading shopping cart...");
            $('.cart').load('loadCartIcon.php .cart');
            $('#shopping-cart').html(data);

            // update cart count
            /*
            var cartCountEl = $('#cart-item-count');
            var cartCount = parseInt(cartCountEl.text());
            cartCount--;

            if (cartCount == 0)
              cartCountEl.css('visibility', 'hidden');
            else
              cartCountEl.text(cartCount);
            */
            // reload document
            documentOnLoad();
          }

        });
      }

    });

    // checkout button 'click' handler
    $('#checkout-btn').on('click', function() {
        
      console.log("You clicked the checkout button...");

      // get modal elements
      var modalHeader = $('#modal-title');
      var modalMessage = $('#modal-message');
      var modalPrimaryBtn = $('#modal-btn-primary');
      var modalSecondaryBtn = $('#modal-btn-secondary');

        // -- style modal
        // ---- header ----
        modalHeader.text("Confirm Checkout");
        modalHeader.attr('class', 'modal-title');
        // ---- message
        modalMessage.text("Are you sure you want to check out?");
        // ---- buttons ----
        modalPrimaryBtn.html("<u>Y</u>es");
        modalPrimaryBtn.attr('class', 'btn btn-warning');
        modalPrimaryBtn.attr('accesskey', 'y');
        // ---- show secondary button
        modalSecondaryBtn.html("<u>N</u>o");
        modalSecondaryBtn.attr('class', 'btn btn-secondary');
        modalSecondaryBtn.show();
        modalSecondaryBtn.attr('accesskey', 'n');

        // change primary button onClick() behavior
        modalPrimaryBtn.on('click', function() {

          // get closest form and submit 
          var form = modalPrimaryBtn.closest('form');
          console.log('Navigating to ' + form.attr('action'));
          form.submit();

      });

      // display a modal to the user
      $('#modal-checkout').modal('toggle');
        
    });
}