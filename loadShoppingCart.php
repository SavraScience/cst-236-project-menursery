<?php

    // imports
    require_once('autoloader.php');
    use inc\business\CartService;
    use inc\data\ProductDao;
    use inc\data\UserDao;
    use inc\models\ProductModel;
    use inc\web\MeNurseryCache;
    use misd\web\Controller;
    use misd\security\SecurityService;
use inc\models\ShoppingCartModel;

    // DECLARATIONS
    $cart = null;
    $numItems = 0;
    
    // get resources
    $imgPathDelete = Controller::resolvePath("res/img/delete-32.png");
        
    // attempt to load the shopping cart from session cache
    $cart = MeNurseryCache::get(MeNurseryCache::SESSKEY_SHOPPING_CART);
        
    if (is_null($cart))
    {
        // initialize the cart
        $cart = new ShoppingCartModel();
        
        if (SecurityService::isCurrUserLoggedIn())
        {
            // attempt to load shopping cart from database
            // -- get some info about the user
            $userDao = new UserDao();
            $currUser = SecurityService::getCurrentUser();
            $user = $userDao->findById($currUser->getUserId());
            
            // -- load the cart from the database
            //console_log("User is logged in!");
            //console_log("Loading shopping cart from database...");
            $service = new CartService();
            $cart = $service->getCartForUser($user);
            
            // store cart in sessino cache
            MeNurseryCache::register(MeNurseryCache::SESSKEY_SHOPPING_CART, $cart);
        }
    }
    else
    {
        //console_log("Cart loaded from cache...");
        //console_log("# of cart items: " . $cart->size());
    }
    
    /** @var $cart inc\models\ShoppingCartModel */
    if (!is_null($cart))
    {
        // debugging
        $numItems = $cart->size();
        //console_log("Cart successfully loaded ($numItems items)...");
        
        if ($numItems > 0)
        {
            
            // load product info
            $products = array();
            $dao = new ProductDao();
            foreach ($cart->getItems() as $cartItem)
            {
                /** @var $cartItem inc\models\ShoppingCartLineItemModel */
                // inspect cart item info
                $productId = $cartItem->getProductId();
                
                // first, attempt to resolve product from session cache
                $product = new ProductModel($productId);
                $product = MeNurseryCache::resolve(MeNurseryCache::SESSKEY_PRODUCTS, $product);
                
                if (is_null($product))
                {
                    // if unresolved, resolve product from database
                    $product = $dao->findById($productId);
                    
                    // debugging
                    //if (!is_null($product)) console_log("Product resolved in database");
                }
                else
                {
                    // debugging
                    //console_log("Product resolved in session cache");
                }
                
                if (!is_null($product)) 
                {
                    array_push($products, $product);
                }
                else
                {
                    // debugging
                    //console_log("Product could not be resolved!");   
                }
            }
            
            // debugging
            //console_log(count($products) . " loaded into products array...");
            //console_log("Loading shopping cart into HTML table...");
            
            echo <<<ML
                <table id="shopping-cart-tbl" class="table table-hover">
                    <thead class="thead-light">
                        <tr>
                            <th>Product</th>
                            <th>Unit Price</th>
                            <th>Quantity</th>
                            <th>Cost</th>

                            <!-- empty row header -->
                            <th></th>
                        <tr>
                    </thead>
                    <tbody>
ML;
            // locale
            $locale = 'en_US';
            $currCode = 'USD';
            
            // initialize variables
            $i = 0;
            $totalCost = 0;
            $fmt = new NumberFormatter($locale, NumberFormatter::CURRENCY);
            
            foreach ($cart->getItems() as $cartItem)
            {
                /** @var $products inc\models\ProductModel[] */
                
                // inspect cart item info
                $timid = $cartItem->getTempId();
                $productDesc = $products[$i]->getDescription();
                $unitPrice = $products[$i]->getPrice();
                $unitPriceFmt = $fmt->formatCurrency($unitPrice, $currCode);
                $qty = $cartItem->getQuantity();
                $cost = $unitPrice * $qty;
                $costFmt = $fmt->formatCurrency($cost, $currCode);
                $totalCost += $cost;
                
                echo <<<ML
                    <tr id="$timid">
                        <td>$productDesc</td>
                        <td>$unitPriceFmt</td>
                        <td>
                            <input class="cart-item-qty" name="quantity" type="number" 
                                value="$qty" maxlength="5" />
                        </td>
                        <td>$costFmt</td>
                        
                        <!-- empty row headers to account for buttons -->
                        <td align="center">
                            <img src="$imgPathDelete" height="24" width="24" 
                                title="Remove Item" class="delete-btn" />
                        </td>
                    <tr>
ML;
                $i++; // increment counter
            }
            
            // determine shipping
            // -- default shipping = $19.99
            $shipping = 19.99;
            if ($totalCost >= 150)
            {
                $shipping = 0;
            }
            
            // calculate grand total
            $grandTotal = $totalCost + $shipping;
            
            // finally, format costs
            $totalCostFmt = $fmt->formatCurrency($totalCost, $currCode);
            $shippingFmt = $fmt->formatCurrency($shipping, $currCode);
            $grandTotalFmt = $fmt->formatCurrency($grandTotal, $currCode);
            
            // generate HTML
            echo <<<ML
                </tbody>
                <tfoot>
                    <tr>
                        <td><p>$numItems items in cart</p></td>
                        <td><!-- purposefully empty --></td>
                        <td colspan="2">
                            <div class="container">
    
                                <div class="row justify-content-end mb-2 mb-sm-1">
                                    <div class="col-12 col-sm-9" style="padding: 0; text-align: right;">
                                        <strong class="mr-sm-3">Total Cost: </strong>
                                    </div>
                                    <div class="col-12 col-sm-3" style="padding: 0; text-align: right;">
                                        <span id="cart-total-cost">$totalCostFmt</span>
                                    </div>
                                </div>
                                <div class="row justify-content-end mb-2 mb-sm-1">
                                    <div class="col-12 col-sm-9" style="padding: 0; text-align: right;">
                                        <strong class="mr-sm-3">Shipping: </strong>
                                    </div>
                                    <div class="col-12 col-sm-3" style="padding: 0; text-align: right;">
                                        <span id="cart-shipping-fee">$shippingFmt</span>
                                    </div>
                                </div>
                                <div class="row justify-content-end mb-2">
                                    <div class="col-12 col-sm-9" style="padding: 0; text-align: right;">
                                        <strong class="mr-sm-3">Grand Total: </strong>
                                    </div>
                                    <div class="col-12 col-sm-3" style="padding: 0; text-align: right;">
                                        <span id="cart-shipping-fee">$grandTotalFmt</span>
                                    </div>
                                </div>
                                <div class="row justify-content-end mt-4">
                                    <form id="checkout-form" action="checkout-handler.php" method="post">
                                        <button id="checkout-btn" type="button" class="btn btn-success">Checkout</button>
                                    </form>
                                </div>
                            </div>
                        </td>
                        <td><!-- purposely empty --></td>
                    </tr>
                </tfoot>
            </table>
ML;
        }
        else
        {
        ?>
        	<div class="container-fluid empty-container">
        		<p class="empty-cart-msg">Your cart is currently empty!</p>
        		<form action="products.php" method="get">
        			<button type="submit" class="btn btn-lg btn-success">Go Shopping</button>
        		</form>
    		</div>    
    	<?php
        }
    }
    else
    {
        console_log("Cart failed to load...");
    }
?>

<!-- checkout confirmation dialog -->
<div class="modal fade" id="modal-checkout">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 id="modal-title" class="modal-title"></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <span id="modal-message"></span>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
        	<button id="modal-btn-secondary" type="button" class="btn" data-dismiss="modal"></button>
        	<form action="checkout-handler.php">
          		<button id="modal-btn-primary" type="button" class="btn" data-dismiss="modal"></button>
          	</form>
        </div>
        
      </div>
    </div>
</div>