<?php

    /**
     * Simply provides an HTTP mechanism for the user to logout using the
     * SecurityService class. Most commonly used with a "Logout" link or button.
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     */

    use misd\security\SecurityService;

    // imports
    require_once 'autoloader.php';

    SecurityService::logout();
?>