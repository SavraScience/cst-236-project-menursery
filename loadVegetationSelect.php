<?php

    // imports
    use inc\business\ProductService;
    use inc\web\MeNurseryCache;
    
    // populate form data selection
    $service = new ProductService();
    $vegTypes = $service->getAllVegetationTypes();

    // register the data in session cache
    MeNurseryCache::register(MeNurseryCache::SESSKEY_VEG_TYPES, $vegTypes);
    
    foreach($vegTypes as $vegType)
    {
        /** @var $vegType \inc\models\VegetationTypeModel */
        $desc = $vegType->getDescription();
        echo <<<ML
            <option value="$desc">$desc</option>\n
ML;
    }
    
?>

