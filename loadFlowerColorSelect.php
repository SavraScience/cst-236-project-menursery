<?php

    // imports
    use inc\business\ProductService;
    use inc\web\MeNurseryCache;
    
    // populate form data selection
    $service = new ProductService();
    $flowerColors = $service->getAllFlowerColors();
    
    // register the data in session cache
    MeNurseryCache::register(MeNurseryCache::SESSKEY_FLOWER_COLORS, $flowerColors);
    
    echo "<option value=''>Select...</option>";
    foreach($flowerColors as $color)
    {
        /** @var $color \inc\models\FlowerColorModel */
        $desc = $color->getColor();
        
        echo <<<ML
            <option value="$desc">$desc</option>\n
ML;
    }
?>