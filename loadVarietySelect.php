<?php

    // imports
    require_once("autoloader.php");
    use inc\business\ProductService;
    use inc\web\MeNurseryCache;
    
    // initialize variables
    $PARAM_KEY = 'q';
    $queryParam = '';
    $varieties = null;
    
    // get Product Service
    $service = new ProductService();
    
    // get query parameter, if available
    if (isset($_GET[$PARAM_KEY]))
    {
        $queryParam = $_GET[$PARAM_KEY];
        //console_log("Getting Subtypes by name '$queryParam'...");
        $varieties = $service->getVarietyByVegetationTypeName($queryParam);
    }
    else
    {
        // get all product subtypes from database
        //console_log("Getting all Varieties...");
        $varieties = $service->getAllVarieties();
    }
    
    // debugging
    //if (is_null($subTypes)) console_log("Subtypes is NULL!"); else console_log("Subtypes are NOT null");
    
    if (!is_null($varieties))
    {
        // register the data in session cache
        MeNurseryCache::register(MeNurseryCache::SESSKEY_VARIETIES, $varieties);
        
        // add default option (none)
        echo "<option value=''>Select...</option>";
        
        foreach($varieties as $variety)
        {
            /** @var $variety \inc\models\VarietyModel */
            $desc = $variety->getDescription();
            
            echo <<<ML
                <option value="$desc">$desc</option>\n
ML;
        }
    }    
?>