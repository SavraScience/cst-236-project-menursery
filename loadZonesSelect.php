<?php

    // imports
    use inc\business\ProductService;
    use inc\web\MeNurseryCache;
    
    // populate form data selection
    $service = new ProductService();
    $zones = $service->getAllZones();
    
    // register the data in session cache
    MeNurseryCache::register(MeNurseryCache::SESSKEY_ZONES, $zones);
    
    foreach($zones as $zone)
    {
        /** @var $zone \inc\models\ZoneModel */
        $val = $zone->getZone();
        
        echo <<<ML
            <option value="$val">$val</option>\n
ML;
    }
?>