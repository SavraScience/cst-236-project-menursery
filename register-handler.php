<?php

    // imports
    require_once 'autoloader.php';
    require_once 'inc/misc-functions.php';
    use inc\models\UserModel;
    use misd\security\InputSanitizer;
    use inc\data\UserDAO;
    use misd\security\SecurityService;
    use misd\web\Controller;
    
    /**
     * A registration handler that processes the data on the registration form
     * and redirects the user as necessary
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @todo Validate and sanitize input before deploying!
     */
    
    // sanitize input
    InputSanitizer::sanitizeStringArray($_POST);

    /* LEGEND */
    # - personal info -
    # txtFirstName
    # txtLastName
    # txtAddressLine1
    # txtAddressLine2
    # txtAddressLine1
    # txtCity
    # txtState
    # txtZipcode
    
    # - login info -
    # txtEmail
    # txtReenterEmail
    # txtUsername
    # txtPassword
    
    // collect the data on the page
    $firstName = $_POST['txtFirstName'];
    $lastName = $_POST['txtLastName'];
    
    /*
    $address = $_POST['txtAddressLine1'] . 
        (!empty($_POST['txtAddressLine2']) ? 
        "\n" . $_POST['txtAddressLine2'] : 
        "");
    $city = $_POST['txtCity'];
    $state = $_POST['txtState'];
    $zip = $_POST['txtZipcode'];
    */
    
    $email = $_POST['txtEmail'];
    $email2 = $_POST['txtReenterEmail'];
    $un = $_POST['txtUsername'];
    $pw = $_POST['txtPassword'];
    
    // check to see that both e-mail addresses are the same
    
    // create a new address

    // create a new user from the form data
    $newUser = new UserModel();
    $newUser->setFirstName($firstName);
    $newUser->setLastName($lastName);
    $newUser->setEmail($email);
    $newUser->setUsername($un);
    $newUser->setPassword($pw);
    
    // insert new user into the database
    $dao = new UserDAO();
    $result = $dao->insert($newUser);
    
    if ($result)
    {   
        // login and redirect the user
        $security = new SecurityService($newUser->getUsername(), $newUser->getPassword());
        if ($security->login()) Controller::redirect('index');
    }
    console_log("Oops!  Something went wrong...");
?>