<?php 
    
    require_once('autoloader.php');
    use inc\data\ShoppingCartDao;
    use inc\web\MeNurseryCache;
    use misd\security\SecurityService;
    use misd\web\Controller;

    /* url resources */
    $urlShoppingCart = Controller::resolvePath("cart.php");
?>

<form action="<?php echo $urlShoppingCart; ?>" class="cart-container">
	<button type="submit">
		<img src="<?php echo Controller::resolvePath("res/img/cart.png"); ?>" alt="Shopping Cart" />
		<p>Shopping Cart</p>
		<span id="cart-item-count" class="badge badge-warning"><?php
		
			// insert logic to access the cart here
			$cart = null; $count = "";
			if (SecurityService::isCurrUserLoggedIn())
			{
			    // get an instance of the user
			    $user = SecurityService::getCurrentUser();
			    
			    // get the cart from the database...
			    $dao = new ShoppingCartDao();
			    $cart = $dao->getCartForUserId($user->getUserId());
			    
			    // store cart in session cache
			    MeNurseryCache::register(MeNurseryCache::SESSKEY_SHOPPING_CART, $cart);
			    
			    if (!is_null($cart)) $count = count($cart->getItems());
			}
		    else 
		    {
		        // get the cart from session cache
		        $cart = MeNurseryCache::get(MeNurseryCache::SESSKEY_SHOPPING_CART);
		        if (!is_null($cart)) $count = count($cart);
		    }
		    
		    // convert count to empty string if 0 or null
		    if (is_null($count) || $count == 0) $count = "";
		    
		    echo $count;
		    
		?></span>
	</button>
</form>