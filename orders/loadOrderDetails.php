<?php

// imports
use inc\web\MeNurseryCache;
use inc\models\OrderModel;
use inc\business\ProductService;

// CONSTANTS
define('TIMID', 'timid');

if (isset($_GET[TIMID]))
{
    $tempId = $_GET[TIMID];
    if (!empty($tempId))
    {
        // resolve order in session cache
        $order = new OrderModel();
        $order->setTempId($tempId);
        $order = MeNurseryCache::resolve(MeNurseryCache::SESSKEY_ORDERS, $order);
        
        /** @var $order OrderModel */
        if (!is_null($order))
        {
            // initialize services
            $productService = new ProductService();
            
            // locale
            $locale = 'en-US';
            $currCode = 'USD';
            
            // formatters
            $currFmt = new NumberFormatter($locale, NumberFormatter::CURRENCY);
            $dateFmt = new DateTime($order->getDate());
            
            // format display values
            $orderDate = $dateFmt->format('F m, Y');
            $subtotal = $currFmt->formatCurrency($order->getSubtotal(), $currCode);
            $shipping = $currFmt->formatCurrency($order->getShipping(), $currCode);
            $grandTotal = $currFmt->formatCurrency($order->getGrandTotal(), $currCode);
            
            // set up the table
            ?>
                <table id="order-detail-tbl" class="table table-sm">
                	<thead class="thead-light">
                		<tr class="bg-primary">
                			<th colspan="5">
                				<div class="container-fluid my-2">
                    				<div id="order-info" class="row">
                    					<div class="col-9">
                    						<label>Date of Purchase:</label>
                    						<span><?php echo $orderDate; ?></span>
                						</div>
                						<div class="col-3 text-right">
                							<span><?php echo $order->size(); ?> items purchased</span>
                						</div>
                    				</div>
                				</div>
                			</th>
                		</tr>
                		<tr>
                			<th></th>
                			<th>Product</th>
                			<th>Unit Price</th>
                			<th>Quantity Ordered</th>
                			<th>Cost</th>
                		</tr>
                	</thead>
                	<tbody>
<?php 
                    foreach($order->getItems() as $lineItem)
                    {
                        /** @var $product \inc\models\ProductModel */
                        // get product details
                        $product = $productService->resolveOrderLineItemToProduct($lineItem);
                        $img = base64_encode($product->getImage());
                        $price = $currFmt->formatCurrency($product->getPrice(), $currCode);
                        $cost = $product->getPrice() * $lineItem->getQuantity();
                        $costFmt = $currFmt->formatCurrency($cost, $currCode);
                        
                        echo <<<ML
                            <tr>
                                <td><img src="data:image/jpg;base64, $img" height="50" width="50" class="thumbnail rounded" /></td>
                                <td valign="center">{$product->getDescription()}</td>
                                <td>$price</td>
                                <td>{$lineItem->getQuantity()}</td>
                                <td>$costFmt</td>
                            </tr>                     
ML;
                    }
?>
                	</tbody>
                	<tfoot>
                		<td><!-- purposely empty --></td>
                		<td><!-- purposely empty --></td>
                		<td><!-- purposely empty --></td>
                        <td colspan="2">
                            <div class="container-fluid">
                                <div class="row justify-content-end mb-2 mb-sm-1">
                                    <div class="col-12 col-sm-9" style="padding: 0; text-align: right;">
                                        <strong class="mr-sm-3 mr-md-0">Total Cost: </strong>
                                    </div>
                                    <div class="col-12 col-sm-3" style="padding: 0; text-align: right;">
                                        <span id="cart-total-cost"><?php echo $subtotal; ?></span>
                                    </div>
                                </div>
                                <div class="row justify-content-end mb-2 mb-sm-1">
                                    <div class="col-12 col-sm-9" style="padding: 0; text-align: right;">
                                        <strong class="mr-sm-3 mr-md-0">Shipping: </strong>
                                    </div>
                                    <div class="col-12 col-sm-3" style="padding: 0; text-align: right;">
                                        <span id="cart-shipping-fee"><?php echo $shipping; ?></span>
                                    </div>
                                </div>
                                <div class="row justify-content-end mb-2">
                                    <div class="col-12 col-sm-9" style="padding: 0; text-align: right;">
                                        <strong class="mr-sm-3 mr-md-0">Grand Total: </strong>
                                    </div>
                                    <div class="col-12 col-sm-3" style="padding: 0; text-align: right;">
                                        <span id="cart-shipping-fee"><?php echo $grandTotal; ?></span>
                                    </div>
                                </div>
                            </div>
                        </td>                		
            		</tfoot>
                </table>
<?php 
            }
            else
            {
                console_log("Order could not be resolved...");
            }
        }
    }
?>