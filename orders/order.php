<?php

// imports
use misd\web\Controller;

// configuration file
chdir('../');
require_once "_config/a_config.php";

// page metadata
$_ENV[Controller::CURR_PAGE_ID] = "Order History";
$_ENV[Controller::CURR_PAGE_TITLE] = "MeNusery - Order History";
?>

<!DOCTYPE html>
<html>
	<head>
		<?php 
		  Controller::includeOnce("inc/page-parts/head-meta-content.php");
        ?>
	</head>
	<body>
		<div class="page-wrap">
		
    		<!-- HEADER -->
    		<?php Controller::requireOnce('inc/page-parts/header.php'); ?>
    		
    		<!-- NAVIGATION -->
    		<?php Controller::requireOnce('inc/page-parts/nav.php'); ?>
    		
		</div>
		
		<!-- PAGE CONTENT -->
		<div class="page-wrap">
    		<h2>Order Detail</h2>
    		<?php Controller::requireOnce("orders/loadOrderDetails.php") ?>
		</div>
		
		<!-- FOOTER -->
		<?php Controller::includeOnce('inc/page-parts/footer.php'); ?>			
	</body>
</html>