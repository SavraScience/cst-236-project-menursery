<?php

// imports
use misd\web\Controller;
use misd\security\SecurityService;

// configuration file
chdir("../");
require_once "_config/a_config.php";

// page metadata
$_ENV[Controller::CURR_PAGE_ID] = "Home";
$_ENV[Controller::CURR_PAGE_TITLE] = "Weapon Store - Home";

/* url resources */
$urlHomePage = Controller::resolvePath("index.php");
$urlOrderHx = Controller::resolvePath("orders/order-history.php");

// get current user
$currUser = SecurityService::getCurrentUser();
?>

<!DOCTYPE html>
<html>
	<head>
		<?php 
		  Controller::includeOnce("inc/page-parts/head-meta-content.php");
        ?>
	</head>
	<body>
		<div class="page-wrap">
		
    		<!-- HEADER -->
    		<?php Controller::requireOnce('inc/page-parts/header.php'); ?>
    		
    		<!-- NAVIGATION -->
    		<?php Controller::requireOnce('inc/page-parts/nav.php'); ?>
    		
		</div>
		
		<!-- PAGE CONTENT -->
		<div class="page-wrap">
			<div class="jumbotron jumbotron-fluid clear-float" 
				style="margin-top: 80px; background-color: #04F1;"
			>
				<div class="container">
    				<h2>Order Confirmation</h2>
    				<h5>Thank you for your purchase<?php 
    				    if (!is_null($currUser)) echo ", " . $currUser->getUsername(); 
    				    ?>!
				    </h5>
    				<form action="<?php echo $urlHomePage; ?>" class="mr-3" style="display: inline-block;">
    					<button type="submit" class="btn btn-warning mt-4">Keep Shopping</button>
    				</form>
    				<form action="<?php echo $urlOrderHx; ?>" class="" style="display: inline-block;">
    					<button type="submit" class="btn btn-primary mt-4">View Order History</button>
    				</form>
    			</div>
    		</div>
		</div>
		
		<!-- FOOTER -->
		<?php Controller::includeOnce('inc/page-parts/footer.php'); ?>	
		
		<!-- Scripts -->
		
	</body>
</html>