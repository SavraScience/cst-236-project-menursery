<?php

    // imports
    use inc\business\OrderService;
    use inc\business\UserService;
    use inc\web\MeNurseryCache;
    use misd\security\SecurityService;
    
    // initialize variables
    $numOrders = 0;
    
    // get the current user
    $currUser = SecurityService::getCurrentUser();
    $userService = new UserService();
    $user = $userService->findById($currUser->getUserId());
    
    // get all orders for the current user
    /** @var $orders OrderModel[] */
    $service = new OrderService();
    $orders = $service->getOrdersForUser($user);
    
    // register the orders in session cache
    MeNurseryCache::register(MeNurseryCache::SESSKEY_ORDERS, $orders);
    
    if (!is_null($orders)) $numOrders = count($orders);
    
    if ($numOrders > 0)
    {
?>
		<div style="width: 85%; margin-top: 30px;">
            <table id="order-history-tbl" class="table table-hover">
                <thead>
                	<a href="www.google.com">
                        <tr>
                            <th><!-- purposely empty --></th>
                            <th class="text-center">Purchase Date</th>
                            <th class="text-center" style="font-size: 90%"># of Items <br>Purchased</th>
                            <th class="text-right">Cost</th>
                        <tr>
                    </a>
                </thead>
                <tbody>		
<?php
        foreach ($orders as $order)
        {
            /** @var $order \inc\models\OrderModel */
            
            // locale
            $locale = 'en-US';
            $currCode = 'USD';
            
            // format
            $fmt = new NumberFormatter($locale, NumberFormatter::CURRENCY);
            $totalFmt = $fmt->formatCurrency($order->getGrandTotal(), $currCode);
            
            // format date
            $dateFmt = new DateTime($order->getDate());
            $orderDate = $dateFmt->format('F d, Y');
            
            echo <<<ML
                <tr>
                    <td>
                        <div class="d-flex justify-content-center">
                        <form action="order.php" method="get">
                            <input name="timid" type="hidden" value="{$order->getTempId()}" />
                            <button type="submit" class="btn btn-sm btn-outline-primary">View Order</button>
                        </form>
                    </td>
                    <td class="text-center">$orderDate</td>
                    <td class="text-center">{$order->size()}</td>
                    <td class="text-right">$totalFmt</td>
                </tr>         
ML;
        }
?>
                </tbody>
                    <tfoot>
                        <tr>
                        	<td colspan="4" class="text-right"><p><?php echo $numOrders; ?> Total Orders placed</p></td>
                        </tr>
                    </tfoot>
            </table>
        </div>
<?php
    }
    else
    {
?>
    <div class="container-fluid empty-container">
		<p class="empty-orders-msg">You haven't purchased anything, yet!</p>
		<form action="products.php" method="get">
			<button type="submit" class="btn btn-lg btn-success">Go Shopping</button>
		</form>
	</div>      
<?php           
    }
    
    
?>