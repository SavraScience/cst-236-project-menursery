<?php

    // imports
    require_once("autoloader.php");
    use inc\business\ProductService;
    use inc\web\MeNurseryCache;

    // initialize variables
    $PARAM_KEY = 'q';
    $queryParam = '';
    $subTypes = null;
    
    // get Product Service
    $service = new ProductService();
    
    // get query parameter, if available
    if (isset($_GET[$PARAM_KEY]))
    {
        $queryParam = $_GET[$PARAM_KEY];
        //console_log("Getting Subtypes by name '$queryParam'...");
        $subTypes = $service->getSubtypeByVegetationTypeName($queryParam);
    }
    else
    {
        // get all product subtypes from database
        //console_log("Getting all Subtypes...");
        $subTypes = $service->getAllSubtypes();
    }

    // debugging
    //if (is_null($subTypes)) console_log("Subtypes is NULL!"); else console_log("Subtypes are NOT null");
    
    if (!is_null($subTypes))
    {
        // register the data in session cache
        MeNurseryCache::register(MeNurseryCache::SESSKEY_SUBTYPES, $subTypes);
        
        // add default option (none)
        echo "<option value=''>Select...</option>";
        
        foreach($subTypes as $subType)
        {
            /** @var $subType \inc\models\SubtypeModel */
            $desc = $subType->getDescription();
            echo <<<ML
                <option value="$desc">$desc</option>\n
ML;
        }
    }
?>