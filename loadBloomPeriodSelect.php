<?php

    // imports
    use inc\business\ProductService;
    use inc\web\MeNurseryCache;
    
    // populate form data selection
    $service = new ProductService();
    $bloomPeriods = $service->getAllBloomPeriods();
    
    // register the data in session cache
    MeNurseryCache::register(MeNurseryCache::SESSKEY_BLOOM_PERIODS, $bloomPeriods);
    
    foreach($bloomPeriods as $period)
    {
        /** @var $period \inc\models\BloomPeriodModel */
        $desc = $period->getDescription();
        echo <<<ML
            <option value="$desc">$desc</option>\n
ML;
    }
?>