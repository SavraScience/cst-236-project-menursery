<?php

    /**
     * A registration form for new users
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @todo Validate and sanitize input before deploying!
     */

    // imports
    use misd\web\Controller;

    // configuration file
    require_once "_config/a_config.php";
    
    // page metadata
    $_ENV[Controller::CURR_PAGE_ID] = "Register";
    $_ENV[Controller::CURR_PAGE_TITLE] = "MeNusery - Register";
?>

<!DOCTYPE html>
<html>
	<head>
		<?php 
		  Controller::includeOnce("inc/page-parts/head-meta-content.php");
        ?>
        <!-- change link? -->
        <link rel="stylesheet" type="text/css" href="/me-nursery/css/registration.css" />
	</head>
	<body>
		<div class="page-wrap">
    		<!-- HEADER -->
    		<?php Controller::requireOnce('inc/page-parts/header.php'); ?>
    		
    		<!-- NAVIGATION -->
    		<?php Controller::requireOnce('inc/page-parts/nav.php'); ?>
		</div>
		
		<!-- PAGE CONTENT -->
		<div class="page-wrap">
    		<h2>Register</h2>
    		<hr />
    		<form id="registration-form" method="post" action="register-handler.php">
    			<div id="registration-container">
        			
        			<div id="section-heading-personal-info" class="form-section-heading">
        				<h3>Personal Information</h3>
        			</div>
        			
        			<!-- First Name -->
        			<label id="lbl-firstname" for="txt-firstname" class="form-lbl">First Name</label>
        			<input id="txt-firstname" name="txtFirstName" type="text" class="form-ctl" required autofocus="autofocus" />
        			
        			<!-- Last Name -->
        			<label id="lbl-lastname" for="lbl-lastname" class="form-lbl">Last Name</label>
        			<input id="txt-lastname" name="txtLastName" type="text" class="form-ctl" required />
        			
        			<!-- Street Address -->
        			<!--
        			<label id="lbl-streetaddress" for="txt-addr-line-1" class="form-lbl">Street Address</label>
        			<input id="txt-addr-line-1" name="txtAddressLine1" type="text" class="form-ctl" />
        			<input id="txt-addr-line-2" name="txtAddressLine2" type="text" class="form-ctl" />
        			  -->
        			  
        			<!-- City -->
        			<!-- 
        			<label id="lbl-city" for="txt-city" class="form-lbl">City</label>
        			<input id="txt-city" name="txtCity" type="text" class="form-ctl" />
        			 -->
        			
        			<!-- State -->
        			<!--
        			<label id="lbl-state" for="sel-state" class="form-lbl">State</label>
        			<select id="sel-state" name="selState" class="form-ctl">
        				<?php 
        				    // Populate with list of U.S. States
        				?>
        			</select>
        			 -->
        			 
        			<!-- Zip Code -->
        			<!--
        			<label id="lbl-zipcode" for="txt-zipcode" class="form-lbl">ZIP Code</label>
        			<input id="txt-zipcode" name="txtZipcode" type="text" class="form-ctl" />
        			
        			<div id="section-heading-login-info" class="form-section-heading">
        				<h3>Login Info</h3>
        			</div>
        			-->
        			
        			<!-- E-mail Address -->
        			<label id="lbl-email" for="txt-email" class="form-lbl">Email address</label>
        			<input id="txt-email" name="txtEmail" type="text" class="form-ctl" required />
        			
        			<!-- Re-enter e-mail address -->
        			<label id="lbl-reenter-email" for="txt-reenter-email" class="form-lbl">Re-enter email address</label>
        			<input id="txt-reenter-email" name="txtReenterEmail" type="text" class="form-ctl" required />
        			
        			<!-- Username -->
        			<label id="lbl-username" for="txt-username" class="form-lbl">Username</label>
        			<input id="txt-username" name="txtUsername" type="text" class="form-ctl" required />
        			
        			<!-- Password -->
        			<label id="lbl-password" for="txt-password" class="form-lbl">Password</label>
        			<input id="txt-password" name="txtPassword" type="password" class="form-ctl" required />
        			
        			<button id="btn-register" type="submit" class="btn btn-primary">Register</button>
    			</div>
    		</form>
		</div>
		
		<!-- FOOTER -->
		<?php Controller::includeOnce('inc/page-parts/footer.php'); ?>			
	</body>
</html>