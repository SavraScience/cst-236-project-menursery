<?php

    // imports
    require_once 'autoloader.php';    
    use inc\business\CartService;
    use inc\web\MeNurseryCache;
    use misd\web\Controller;
    
    // CONSTANTS
    define('TIMID', 'timid');
    
    // get post input
    if (isset($_POST[TIMID]))    
    {
        $timid = $_POST[TIMID];
        
        // debugging
        //echo "Attempting to resolve cart item $timid\n";
        
        // resolve cart item
        // -- get shopping cart from DB
        /** @var $cart inc\models\ShoppingCartModel */
        $cart = MeNurseryCache::get(MeNurseryCache::SESSKEY_SHOPPING_CART);
        
        if (!is_null($cart))
        {
            /* attempt to resolve the item in the cart using the item's
               temporary ID */
            $cartItem = $cart->resolveItem($timid);
            //  var_dump($cartItem); // <- cart item does not have ID
            
            /** @var $cartItem inc\models\ShoppingCartLineItemModel */
            if (!is_null($cartItem) && $cartItem->getId() > 0)
            {
    
                // $cartItem resolved in Session cache
                // -- attempt to delete the item in the shopping cart
                $service = new CartService();
                if ($service->removeCartItem($cartItem))
                {
                    // update the cart in session cache
                    $cart->removeItem($cartItem->getProductId());
                    MeNurseryCache::register(MeNurseryCache::SESSKEY_SHOPPING_CART, $cart);
                    
                    // debugging
                    //console_log("Size after cache refresh: " . $cart->size());
                    //echo "Delete succeeded.";
                }
                else
                {
                    // debugging
                    //echo "Delete failed.";
                }
            }
            else
            {
                //echo "Cart Item could not be resolved in the cart...";
            }
        }
        else
        {
            //echo "Cart not be resolved in session cache...";
        }
    }

    // reload shopping cart
    Controller::requireOnce('loadShoppingCart.php');

?>