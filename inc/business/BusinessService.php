<?php
namespace inc\business;


abstract class BusinessService
{
    // INSTANCE VARIABLES
    
    // CONSTRUCTOR
    public function __construct() {}
    
    // PUBLIC METHODS
    public abstract function getAll();
    public abstract function findById($id);
    public abstract function update($object);
    public abstract function deleteById($id);
}

