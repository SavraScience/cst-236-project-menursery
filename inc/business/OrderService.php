<?php
namespace inc\business;

use inc\models\OrderModel;
use inc\models\ProductModel;
use inc\models\UserModel;
use inc\models\ShoppingCartModel;
use inc\models\ShoppingCartLineItemModel;
use inc\models\OrderLineItemModel;
use inc\data\OrderDao;
use inc\data\OrderLineItemDao;
use inc\web\MeNurseryCache;
use misd\security\SecurityService;
use misd\data\QueryObject;
use misd\data\MySqlOperator;

class OrderService
{
    // INSTANCE VARIABLES
    private $orderDao;
    private $orderLineItemDao;
    
    // CONSTRUCTOR
    public function __construct()
    {
        $this->orderDao = new OrderDao();
        $this->orderLineItemDao = new OrderLineItemDao();
    }
    
    // PUBLIC METHODS
    /**
     * Retrieves all orders from the database
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return array
     */
    public function getAllOrders() : array
    {
        $orders = $this->orderDao->findAll();
        
        // attach line items for each order
        foreach($orders as $order)
        {
            $this->attachOrderLineItems($order);
        }
        
        return $orders;
    }
    
    /**
     * Retrieves all orders for a particular user
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param UserModel $user
     * @return \misd\models\AbstractObjectModel[]|NULL
     */
    public function getOrdersForUser(UserModel $user)
    {
        $orders = array();
        
        // find all orders for this user
        $query = new QueryObject();
        $query->addCriterion("userId", MySqlOperator::EQUALS(), $user->getId());
        $orders =  $this->orderDao->find($query);
        
        // attach line items for each order
        foreach($orders as $order)
        {
            $this->attachOrderLineItems($order);
        }
        
        return $orders;
    }
    
    public function getOrdersForDateRange($startDate, $endDate)
    {
        $orders = array();
        
        // modify end date
        $endDateFmt = new \DateTime($endDate);
        $endDate = $endDateFmt->add(new \DateInterval("P1D"))->format('Y-m-d');
        
        // debugging
        //console_log("Start Date: $startDate");
        //console_log("End Date: $endDate");
        
        // find all orders that fall within the date range
        $query = new QueryObject();
        $query->addCriterion("date", MySqlOperator::GREATER_THAN_OR_EQUALS(), $startDate);
        $query->addCriterion("date", MySqlOperator::LESS_THAN(), $endDate);
        $orders =  $this->orderDao->find($query);
        
        // attach line items for each order
        foreach($orders as $order)
        {
            $this->attachOrderLineItems($order);
        }
        
        return $orders;
    }
    
    /**
     * Creates a new order in the database with as many line items as needed
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param ShoppingCartModel $cart
     * @return bool
     */
    public function createOrder(ShoppingCartModel $cart) : bool
    {
        // initialize variables
        $order = new OrderModel();
        $result = false;
        
        // debugging
        //console_log("Creating new order...");
        
        // get the currently logged-in user
        $currUser = SecurityService::getCurrentUser();
        $order->setUserId($currUser->getUserId());
        
        // convert all shopping cart items to order line items
        foreach($cart->getItems() as $cartItem)
        {
            /** @var $cartItem ShoppingCartLineItemModel */
            /** @var $lineItem OrderLineItemModel */
            $lineItem = $this->convertShoppingCartItemToOrderLineItem($cartItem);
            $order->addItem($lineItem);
        }
        
        // calculations
        $this->calculateSubtotal($order);
        $this->calculateShipping($order);
        $this->calculateGrandTotal($order);
        
        // save the order to the database
        // -- begin transaction mode
        $this->orderDao->transactionMode(true);
        $this->orderLineItemDao->transactionMode(true);
        
        // first, save the order to the database
        $newId = $this->orderDao->insert($order);
        //console_log("Result = $newId");
        if ($newId > 0)
        {
            // debugging
            //console_log("Order inserted... Attempting to insert line items...");
            
            $order->setOrderId($newId);
            foreach($order->getItems() as $lineItem)
            {
                $result = $this->orderLineItemDao->insert($lineItem);
                if ($result == 0) 
                    break;
            }
            
            if ($result)
            {
                // attempt to delete cart
                $cartService = new CartService();
                $result = $cartService->deleteCartForUser($currUser);
            }
            else
            {
                // debugging
                //console_log("Could not insert one or more OrderLineItemModels " +
                //    "into the database...");
            }
        }
        else
        {
            // debugging 
            //console_log("Could not insert the OrderModel into the database...");
            $result = false;
        }
        
        // check if all insertions successful, then commit changes
        if ($result)
        {            
            //console_log("All insertions successful!\nCommitting changes...");
            $this->orderDao->commit();
            $this->orderLineItemDao->commit();
        }
        else
        {
            //console_log("Rolling back transaction...");
            $this->orderDao->rollback();
            $this->orderLineItemDao->rollback();
        }
        
        // -- reset transaction mode
        $this->orderDao->transactionMode(false);
        $this->orderLineItemDao->transactionMode(false);
        return $result;
    }
    
    /**
     * Attaches all the line items for a particular order to
     * its representative OrderModel
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param OrderModel $order The order you want to attach line items for
     */
    private function attachOrderLineItems(OrderModel $order)
    {
        // first, retrieve all line items for this order
        $query = new QueryObject();
        $query->addCriterion("orderId", MySqlOperator::EQUALS(), $order->getId());
        $lineItems = $this->orderLineItemDao->find($query);
        
        if (!empty($lineItems))
        {
            foreach($lineItems as $item)
            {
                /** @var $item \inc\models\OrderLineItemModel */
                $order->addItem($item);
            }
        }
        else
        {
            // no line items found -- this should never happen
            console_log("Error! No line items found for Order ID {$order->getId()}");
        }
    }
    
    // PRIVATE METHODS
    // -- conversion methods
    private function convertShoppingCartItemToOrderLineItem(
        ShoppingCartLineItemModel $cartItem
    ) : OrderLineItemModel
    {
        // instantiate variables
        $lineItem = new OrderLineItemModel();
        
        $productId = $cartItem->getProductId();
        $qty = $cartItem->getQuantity();
        $userId = $cartItem->getUserId();
        
        // debugging
        //console_log("Converting cart item...");
        //console_log("Product ID: $productId\nQuantity: $qty\nUser ID: $userId");
        
        $lineItem->setProductId($productId);
        $lineItem->setQuantity($qty);
        
        // debugging
        //console_log("Order Line Item properties set!");
        $tempId = $lineItem->getTempId();
        $productId = $lineItem->getProductId();
        $qty = $lineItem->getQuantity();
        //console_log("Temp ID: $tempId\nProduct ID: $productId\nQuantity: $qty\nUser ID: $userId");
        
        return $lineItem;
    }
    
    // -- calculation methods
    private function calculateSubtotal(OrderModel $order) : float
    {
        // initialize variables
        $subtotal = 0;
        $productService = new ProductService();
        
        foreach($order->getItems() as $lineItem)
        {
            // resolve the product via the database
            /** @var $product ProductModel */
            $product = $productService->findById($lineItem->getProductId());
            $subtotal += $product->getPrice() * $lineItem->getQuantity();
        }
        
        // set & return the calculated subtotal
        $order->setSubtotal($subtotal);
        return $order->getSubtotal();
    }
    
    private function calculateShipping(OrderModel $order) : float
    {
        // standard shipping = $10.00
        $stdShipping = 10.0;
        
        if ($order->getSubtotal() > 150.0)
        {
            // shipping is over $150
            // -- waive shipping costs
            $shipping = 0.0;
        }
        else
        {
            $shipping = $stdShipping;
        }
        
        $order->setShipping($shipping);
        return $order->getShipping();
    }
    
    private function calculateGrandTotal(OrderModel $order)
    {
        // tally up the subtotal plus any shipping
        $subtotal = $order->getSubtotal();
        $shipping = $order->getShipping();
        $order->setGrandTotal($subtotal + $shipping);
        return $order->getGrandTotal();
    }
}

