<?php
namespace inc\business;

use inc\models\ShoppingCartModel;
use inc\models\UserModel;
use inc\data\ShoppingCartDao;
use inc\models\ShoppingCartLineItemModel;
use inc\security\CurrentUser;

class CartService
{
    // INSTANCE VARIABLES
    /** @var $dao ShoppingCartDao */
    private $dao;
    
    // CONSTRUCTOR
    public function __construct()
    {
        $this->dao = new ShoppingCartDao();
    }
    
    // PUBLIC METHODS 
    
    /**
     * Returns a ShoppingCartModel that represents all of
     * the items in a user's cart for a particular user.
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param UserModel $user
     * @return NULL|\inc\models\ShoppingCartModel
     */
    public function getCartForUser(UserModel $user)
    {
        return $this->dao->getCartForUserId($user->getId());
    }
    
    public function saveCart(ShoppingCartModel $cart)
    {
        foreach ($cart as $cartItem)
        {
            /** @var $cartItem \inc\models\ShoppingCartLineItemModel */
            $this->dao->updateCartItem(
                $cartItem->getUserId(), 
                $cartItem->getProductId(), 
                $cartItem->getQuantity()
            );
        }
    }
    
    
    public function saveCartItem(ShoppingCartLineItemModel $cartItem)
    {
        // debugging
        //console_log("Calling CartService::saveCartItem(\$cartItem)...");
        //console_log("Product ID = " . $cartItem->getProductId());
        
        return $this->dao->updateCartItem(
            $cartItem->getUserId(), 
            $cartItem->getProductId(),
            $cartItem->getQuantity()
        );
    }
    
    /**
     * Increments the quantity of a product in the user's cart by 1.
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param ShoppingCartLineItemModel $cartItem
     * @return bool
     */
    public function incrementCartItem(ShoppingCartLineItemModel $cartItem) : bool
    {
        return $this->dao->incrementCartItem(
            $cartItem->getUserId(),
            $cartItem->getProductId()
        );
    }
    
    /**
     * Decrements the quantity of a product in the user's cart by 1.
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param ShoppingCartLineItemModel $cartItem
     * @return bool
     */
    public function decrementCartItem(ShoppingCartLineItemModel $cartItem) : bool
    {
        return $this->dao->decrementCartItem(
            $cartItem->getUserId(),
            $cartItem->getProductId()
            );
    }
    
    /**
     * Removes an item from the user's cart
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param ShoppingCartLineItemModel $cartItem
     * @return bool
     */
    public function removeCartItem(ShoppingCartLineItemModel $cartItem) : bool
    {  
        return $this->dao->deleteById($cartItem->getId());
    }
    
    /**
     * Clears a given user's shopping cart
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param mixed $user
     * @return bool
     */
    public function deleteCartForUser($user) : bool
    {
        // instantiate variables
        $dao = new ShoppingCartDao();
        
        if ($user instanceof CurrentUser)
        {
            return $dao->deleteCartForUserId($user->getUserId());
        }
        elseif ($user instanceof UserModel)
        {
            return $dao->deleteCartForUserId($user->getId());
        }
        return false;
    }
}

