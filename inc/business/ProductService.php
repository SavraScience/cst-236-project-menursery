<?php
namespace inc\business;

use inc\data\VarietyDao;
use inc\data\VegetationTypeDao;
use inc\data\SubtypeDao;
use inc\data\BloomPeriodDao;
use inc\data\FlowerColorDao;
use inc\data\ZoneDao;
use inc\data\ProductDao;
use misd\data\QueryObject;
use misd\data\MySqlOperator;
use misd\data\MySqlCriterionType;
use inc\models\OrderLineItemModel;

require_once ('inc/business/BusinessService.php');

class ProductService extends BusinessService
{

    // CONSTRUCTOR
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 
     * {@inheritDoc}
     * @see \inc\business\BusinessService::getAll()
     */
    public function getAll()
    {
        $dao = new ProductDao();
        return $dao->findAll();
    }
    
    public function find($searchString)
    {
        // initialize variables
        $dao = new ProductDao();
        $crit = new QueryObject();
        
        // add criteria
        $crit->addCriterion("description", MySqlOperator::LIKE(), $searchString);
        $crit->addCriterion("vegetationType", MySqlOperator::LIKE(), $searchString, MySqlCriterionType::OR);
        
        return $dao->find($crit);
    }
    
    /**
     * (non-PHPdoc)
     *
     * @see \inc\business\BusinessService::findById()
     */
    public function findById($id)
    {
        $dao = new ProductDao();
        return $dao->findById($id);
    }

    /**
     * (non-PHPdoc)
     *
     * @see \inc\business\BusinessService::deleteById()
     */
    public function deleteById($id)
    {
        $dao = new ProductDao();
        return $dao->deleteById($id);
    }

    /**
     * (non-PHPdoc)
     *
     * @see \inc\business\BusinessService::update()
     */
    public function update($product)
    {
        $dao = new ProductDao();
        return $dao->update($product);
    }

    // CHILD TABLE FUNCTIONS
    public function getAllVegetationTypes()
    {   
        $dao = new VegetationTypeDao();
        return $dao->findAll();
    }
    
    public function getAllSubtypes()
    {
        $dao = new SubtypeDao();
        return $dao->findAll();
    }
    
    public function getSubtypeByVegetationTypeName(string $vegTypeName)
    {
        $dao = new SubtypeDao();
        return $dao->findAllByVegetationType($vegTypeName);
    }
    
    public function getAllVarieties()
    {
        $dao = new VarietyDao();
        return $dao->findAll();
    }
    
    public function getVarietyByVegetationTypeName(string $vegTypeName)
    {
        $dao = new VarietyDao();
        return $dao->findAllByVegetationType($vegTypeName);
    }
    
    public function getAllFlowerColors()
    {
        $dao = new FlowerColorDao();
        return $dao->findAll();
    }
    
    public function getAllBloomPeriods()
    {
        $dao = new BloomPeriodDao();
        return $dao->findAll();
    }
    
    public function getAllZones()
    {
        $dao = new ZoneDao();
        return $dao->findAll();
    }
    
    public function resolveOrderLineItemToProduct(OrderLineItemModel $lineItem)
    {
        $productId = $lineItem->getProductId();
        return $this->findById($productId);
    }
}

