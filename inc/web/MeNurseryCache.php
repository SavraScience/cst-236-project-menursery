<?php
namespace inc\web;

require_once ('misd/web/SessionCache.php');

use misd\web\SessionCache;

class MeNurseryCache extends SessionCache
{
    // session cache constants
    public const SESSKEY_BLOOM_PERIODS  = 'bloomPeriods';
    public const SESSKEY_FLOWER_COLORS  = 'flowerColors';
    public const SESSKEY_PRODUCTS       = 'products';
    public const SESSKEY_SUBTYPES       = 'subtypes';
    public const SESSKEY_VARIETIES      = 'varieties';
    public const SESSKEY_USERS          = 'users';
    public const SESSKEY_VEG_TYPES      = 'vegetationTypes';
    public const SESSKEY_ZONES          = 'zones';
    public const SESSKEY_SHOPPING_CART  = 'shoppingCart';
    public const SESSKEY_ORDERS         = 'orders';
}

