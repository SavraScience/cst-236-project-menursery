<?php
namespace inc\security;

// imports
use inc\models\UserModel;
use misd\security\CurrentUserInterface;

class CurrentUser implements CurrentUserInterface
{
    // INSTANCE VARIABLES
    /** @var $userId string */
    private $userId;
    /** @var $username string */
    private $username;
    /** @var $firstName string */
    private $firstName;
    /** @var $lastName string */
    private $lastName;
    /** @var $role int */
    private $role;
    
    // CONSTRUCTOR
    public function __construct(UserModel $user) 
    {
        // TODO: Add email?
        $this->userId = $user->getId();
        $this->username = $user->getUsername();
        $this->firstName = $user->getFirstName();
        $this->lastName = $user->getLastName();
        $this->role = $user->getUserRole();
    }
    
    // ACCESSOR/MUTATOR METHODS
    public function getUserId()
    {
        return $this->userId;
    }
    
    public function getUsername() : string
    {
        return $this->username;
    }
    
    public function getUserRole()
    {
        return $this->role;
    }

    public function getFirstName() : string
    {
        return $this->firstName;
    }

    public function getLastName() : string
    {
        return $this->lastName;
    }
}

