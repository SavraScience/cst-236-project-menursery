<?php
    
/**
 * @author Michael Mason
 * @copyright 2019 Mason Innovative Software Design
 */

// IMPORTS
require_once 'autoloader.php';
use misd\security\SecurityService;
use misd\web\Controller;
use misd\web\Webpage;

// PAGE DEFINITIONS
// --page constants
define("HOME_PAGE", "Home");
define("ABOUT_PAGE", "About");
define("PRODUCTS_PAGE", "Products");
define("LOGIN_PAGE", "Login");
define("REGISTER_PAGE", "Register");

// include webpages and their paths here:
$pages = array();
$pages[HOME_PAGE] = new Webpage(HOME_PAGE, 'index.php');
$pages[ABOUT_PAGE] = new Webpage(ABOUT_PAGE, 'about.php');
$pages[PRODUCTS_PAGE] = new Webpage(PRODUCTS_PAGE, 'products.php');

// login and registration links ONLY
$authPages = array();
$authPages[LOGIN_PAGE] = new Webpage(LOGIN_PAGE, 'login.php');
$authPages[REGISTER_PAGE] = new Webpage(REGISTER_PAGE, 'register.php');

// now print each page as a navigation link to the screen...
?>
<nav>
    <ul class="nav" style="float: left;">
    	<?php 
    	
    	   # Dynamically builds the links on the page
    	   foreach($pages as $page)
    	   {
    	        // initialize variables
    	        $cssClass = "nav-link";
    	        $pageName = $page->getName();
    	        $pageUrl = "#";
    	        
    	        
    	        // conditional property values
                if (Webpage::isCurrentPage($page))
                {
                    $cssClass .= " currentpage";
                }
                else
                {
                    $pageUrl = $page->getPath();
                }
                
                // build link element
                $link = <<<ML
                    <a href="$pageUrl" class="$cssClass"><li>$pageName</li></a>
ML;
    	       echo $link;
    	       
    	   }
    	?>	
    </ul>
    <div style="float: right;">
        <ul class="nav">
        	<?php
        	   # Dynamically builds login/logout and register links on the page
        	   if (SecurityService::isCurrUserLoggedIn())
        	   {
        	       // get URL resources
        	       $urlOrderHistory = Controller::resolvePath("orders/order-history.php");
        	       $urlLogout = Controller::resolvePath("logout.php");
        	       $urlAdmin = Controller::resolvePath("admin/admin-panel.php");
        	       $urlManageUsers = Controller::resolvePath("admin/displayUsers.php");
        	       $urlManageProducts = Controller::resolvePath("admin/displayProducts.php");
        	       $urlSalesReports = Controller::resolvePath("admin/sales.php");
        	       
        	       // if administrator...
        	       if (SecurityService::isCurrUserAdmin())
        	       {
        	           // ...display admin link
        	           $link = <<<ML
                            <div class="dropdown-container">
                                <a href="$urlAdmin" class="nav-link"><li>Admin</li></a>
                                <div class="dropdown-mnu">
                                    <a href="$urlManageUsers" class="dropdown-link"><li>Manage Users</li></a>
                                    <a href="$urlManageProducts" class="dropdown-link"><li>Manage Products</li></a>   
                                </div>
                            </div>
    ML;
        	           echo $link;
        	       }
                    
        	       if (!SecurityService::isCurrUserAdmin())
        	       {
        	           $link = <<<ML
        	               <a href="$urlOrderHistory" class="nav-link"><li>Order History</li></a>
ML;
    	               echo $link;
        	       }
        	       // display logout link
        	       $link = <<<ML
                    <a href="$urlLogout" class="nav-link"><li>Logout</li></a>
ML;
        	       echo $link;
        	   }
        	   else
        	   {
        	       // display login and registration links
            	   foreach($authPages as $page)
            	   {
            	       // initialize variables
            	       $cssClass = "nav-link";
            	       $pageName = $page->getName();
            	       $pageUrl = "#";
            	       
            	       // conditional property values
            	       if (Webpage::isCurrentPage($page))
            	       {
            	           $cssClass .= " currentpage";
            	       }
            	       else
            	       {
            	           $pageUrl = $page->getPath();
            	       }
            	       
            	       // build link element
            	       $link = <<<ML
                            <a href="$pageUrl" class="$cssClass"><li>$pageName</li></a>
ML;
            	       echo $link;
            	   }
        	   }
        	?>	    	
        </ul>
        <?php if (SecurityService::isCurrUserLoggedIn() && !SecurityService::isCurrUserAdmin()) {?>
        	<div class="cart">
				<?php echo Controller::requireOnce('loadCartIcon.php'); ?>
        	</div>
        <?php }?>
    </div>
    <div style="clear: left ;"></div>
</nav>