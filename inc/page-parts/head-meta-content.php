<?php 
    
    // imports
    use misd\web\Controller;

?>
<title><?php echo $_ENV[Controller::CURR_PAGE_TITLE]; ?></title>

<?php // change href path to remove MeNursery after you have deployed to the web ?>

<!-- jQuery library -->
<script src="/me-nursery/js/jquery-3.4.1.min.js"></script>

<!-- Bootstrap libraries -->
<link rel="stylesheet" type="text/css" href="/me-nursery/bootstrap-4.3.1-dist/css/bootstrap.min.css">
<script type="text/javascript" src="/me-nursery/bootstrap-4.3.1-dist/js/bootstrap.bundle.min.js"></script>

<!-- Font Awesome Library -->
<!-- 
<script src="https://kit.fontawesome.com/e00c7cb561.js" crossorigin="anonymous"></script>
 -->
 
<!-- Custom CSS File -->
<link rel="stylesheet" type="text/css" href="/me-nursery/css/main.css?nocache=true">

<!-- META TAGS -->
<meta name="viewport" content="width=device-width, initial-scale=1">