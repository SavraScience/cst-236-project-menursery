<?php
    
    // imports
    use inc\business\CartService;
    use inc\business\ProductService;
    use inc\data\UserDao;
    use inc\web\MeNurseryCache;
    use misd\security\SecurityService;
    use misd\web\Controller;
    
    // initialize variables
    $currUser = null;
    $cart = null;

    // check to see if the user is logged in, and if so,
    // retrieve their shopping cart
    if (SecurityService::isCurrUserLoggedIn() && !SecurityService::isCurrUserAdmin())
    {
        // get current user
        $currUser = SecurityService::getCurrentUser();
        
        if (!is_null($currUser))
        {
            // resolve the current user to a UserModel
            $userDao = new UserDao();
            $user = $userDao->findById($currUser->getUserId());
            
            // load all of the CurrentUser's cart items
            $service = new CartService();
            $cart = $service->getCartForUser($user);
            MeNurseryCache::register(MeNurseryCache::SESSKEY_SHOPPING_CART, $cart);
        }
        else
        {
            // debugging
            //console_log("User is logged in, but could not retrieve current user");
        }
    }
    else
    {
        // debugging
        //console_log("Current user is not logged in");
    }
?>

<div id="products-container">
	<?php 

	// load all products from the database
	$service = new ProductService();
	$products = $service->getAll();
	
	// register products in session cache
	MeNurseryCache::register(MeNurseryCache::SESSKEY_PRODUCTS, $products);
	
	if (!empty($products))
	{
         // get currency formatter
         $locale = 'en_US';
         $currCode = 'USD';
         $fmt = new NumberFormatter($locale, NumberFormatter::CURRENCY); 
	       
	     // print all products to the screen as Bootstrap cards
    	 foreach ($products as $product)
    	 {
    	     /** @var $product \inc\models\ProductModel */
    	     $timid = $product->getTempId();
    	     $price = $product->getPrice();
    	     $priceFmt = $fmt->formatCurrency($price, $currCode);
    	     $productDesc = $product->getDescription();
    	     $vegTypeDesc = $product->getVegetationType()->getDescription();
    	     
    	     // check to see if product is in current user's cart
    	     $inCart = false;
    	     if (!is_null($cart))
    	     {
    	         $cartItem = $cart->getItem($product->getId());
    	         if (!is_null($cartItem)) $inCart = true;
    	     }
    	     
    	     // optional styling
    	     $addlClass = "";
    	     if ($inCart) $addlClass = " in-cart";
    	     
    	     echo <<<ML
                <div id="$timid" class="card d-flex$addlClass">
                    <div class="card-body">
                        <h5 class="card-title">$productDesc</h5>
ML;
    	     // convert image (blob) to base64-encoded string and display
    	     $productImage = $product->getImage();
    	     if ($productImage)
    	     {
    	         $productImage = base64_encode($productImage);
    	         echo <<<ML
                      <img src="data:image/jpg;base64, $productImage" height="200" width="200" class="product-img" />
ML;
    	     }
    	     
    	     // display price vegetation type
    	     echo <<<ML
                <div class="d-flex product-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-4"><label class="product-lbl"><strong>Price:</strong></label></div>
                            <div class="col-8"><span class="product-val"><strong>$priceFmt</strong></span></div>
                        </div>                    
                        <div class="row">
                            <div class="col-4"><label class="product-lbl">Type:</label></div>
                            <div class="col-8"><span class="product-val">$vegTypeDesc</span></div>
                        </div>
ML;
    	     // (optional) display subtype
    	     if (!empty($product->getSubtype()) && !empty($product->getSubtype()->getDescription()))
    	     {
    	         $subtypeDesc = $product->getSubtype()->getDescription();
    	         echo <<<ML
                    <div class="row">
                        <div class="col-4"><label class="product-lbl">Subtype:</label></div>
                        <div class="col-8"><span class="product-val">$subtypeDesc</span></div>
                    </div>
ML;
    	     }
    	     
    	     // (optional) display variety
    	     if (!empty($product->getVariety()) && !empty($product->getVariety()->getDescription()))
    	     {
    	         $varietyDesc = $product->getVariety()->getDescription();
    	         echo <<<ML
                    <div class="row">
                        <div class="col-4"><label class="product-lbl">Variety:</label></div>
                        <div class="col-8"><span class="product-val">$varietyDesc</span></div>
                    </div>
ML;
    	     }
    	     
    	     // END OF card-body
    	     echo <<<ML
                    </div> <!-- END class="product-body" -->
    	           </div> <!-- END class="container" -->
                </div> <!-- END class="card-body" -->
ML;         
             // add-to-cart buttons
    	     if (SecurityService::isCurrUserLoggedIn() && !SecurityService::isCurrUserAdmin())
    	     {     	         
    	         // default button class, message, and disabled status
    	         $btnMsg = "Add to Cart";
    	         $btnClass = " btn-success add-to-cart-btn";
    	         $btnDisabled = "";
    	         if ($inCart)
    	         {
    	             // change button class & message
    	             $btnMsg = "In Cart";
    	             $btnClass = " btn-warning add-to-cart-btn";
    	             $btnDisabled = " disabled";
    	         }
    	         
    	         echo <<<ML
                    <div class="d-flex flex-column">
                        <form action="addToCart-handler.php" method="post" 
                            class="d-flex flex-column align-items-end justify-content-end flex-grow-1 cart-item-form"
                        >
                            <input name="timid" type="hidden" value="$timid" />
                            <button type="button" class="btn$btnClass" style=""$btnDisabled>$btnMsg</button>
                        </form>
                    </div>
ML;
    	     }
    	     // END OF card
    	     echo '</div> <!-- END class="card" -->';
    	 }
	}
	?>
</div>

<!-- scripts -->
<script src="<?php echo Controller::resolvePath("js/products.js"); ?>"></script>