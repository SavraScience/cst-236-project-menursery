<?php

// imports
use misd\web\Controller;

// configuration file
require_once "_config/a_config.php";

// page metadata
$_ENV[Controller::CURR_PAGE_ID] = "Forbidden";
$_ENV[Controller::CURR_PAGE_TITLE] = "MeNusery - Unauthorized Access";

// get the previous page
$prevPage = "javascript:history.go(-1)";
if(isset($_SERVER['HTTP_REFERER'])) $prevPage = $_SERVER['HTTP_REFERER'];
?>

<!DOCTYPE html>
<html>
	<head>
		<?php 
		  Controller::includeOnce("inc/page-parts/head-meta-content.php");
        ?>
	</head>
	<body>
		<div class="page-wrap">
		
    		<!-- HEADER -->
    		<?php Controller::requireOnce('inc/page-parts/header.php'); ?>
    		
    		<!-- NAVIGATION -->
    		<?php Controller::requireOnce('inc/page-parts/nav.php'); ?>
    		
		</div>
		
		<!-- PAGE CONTENT -->
		<div class="page-wrap">
    		<h2>Forbidden &mdash; Unauthorized Access</h2>
    		<p>Sorry, but you don't have sufficient rights to view this page!</p>
    		<!-- back button -->
    		<form>
    			<button type="submit" formaction="<?php echo $prevPage; ?>" class="btn btn-primary">Go Back</button>
    		</form>		
		</div>
		<!-- FOOTER -->
		<?php Controller::includeOnce('inc/page-parts/footer.php'); ?>			
	</body>
</html>