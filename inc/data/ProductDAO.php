<?php
namespace inc\data;

// imports
use misd\data\Dao;
use inc\models\ProductModel;
use misd\data\DataTranslator;
use misd\data\DataTableMapping;

/**
 * Data Access Object for the User table
 * @author Michael Mason
 * @copyright 2019 Mason Innovative Software Design
 */
class ProductDao extends Dao
{   
    // CONSTANTS
    // -- table name
    private const TBL_NAME = 'product';
    
    // -- field names
    private const FLD_NAME_ID = 'product_id';
    private const FLD_NAME_DATE_ADDED = 'date_added';
    private const FLD_NAME_VEGTYPE_ID = 'veg_type_id';
    private const FLD_NAME_SUBTYPE_ID = 'subtype_id';
    private const FLD_NAME_VARIETY_ID = 'variety_id';
    private const FLD_NAME_BLOOMPD_ID = 'bloom_pd_id';
    private const FLD_NAME_FLOWERCOL_ID = 'flower_color_id';
    private const FLD_NAME_PRODUCT_DESC = 'product_desc';
    private const FLD_NAME_HEIGHT = 'height';
    private const FLD_NAME_HEIGHT_UNIT = 'height_unit';
    private const FLD_NAME_SPREAD = 'spread';
    private const FLD_NAME_SPREAD_UNIT = 'spread_unit';
    private const FLD_NAME_PRICE = 'price';
    private const FLD_NAME_PRODUCT_IMG = 'product_img';
    
    /**
     *
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $tableName The name of the table in
     * the database that corresponds to the class/entity
     * you wish to perform CRUD operations on
     */
    public function __construct()
    {
        parent::__construct(self::TBL_NAME, new ProductModel());
    }
    
    // IMPLEMENTED METHODS
    final function setMapProperties(DataTranslator &$translator)
    {
        // map object/class properties of the model to the database
        $translator->addTableMapping(
            "id", 
            self::FLD_NAME_ID, 
            self::TBL_NAME,
            "i",
            DataTableMapping::KEYTYPE_PRIMARY_KEY
        );
        $translator->addTableMapping(
            "dateAdded", 
            self::FLD_NAME_DATE_ADDED, 
            self::TBL_NAME
        );
        $translator->addTableMapping(
            "vegetationType", 
            self::FLD_NAME_VEGTYPE_ID, 
            self::TBL_NAME, 
            "i",
            DataTableMapping::KEYTYPE_FOREIGN_KEY, 
            array("veg_type" => "veg_type_id"), 
            new VegetationTypeDao()
        );
        $translator->addTableMapping(
            "subtype", 
            self::FLD_NAME_SUBTYPE_ID, 
            self::TBL_NAME, 
            "i",
            DataTableMapping::KEYTYPE_FOREIGN_KEY,
            array("subtype" => "subtype_id"),
            new SubtypeDao()
        );
        $translator->addTableMapping(
            "variety", 
            self::FLD_NAME_VARIETY_ID, 
            self::TBL_NAME, 
            "i",
            DataTableMapping::KEYTYPE_FOREIGN_KEY,
            array("variety" => "variety_id"),
            new VarietyDao()
        );
        $translator->addTableMapping(
            "bloomPeriod", 
            self::FLD_NAME_BLOOMPD_ID, 
            self::TBL_NAME, 
            "i", 
            DataTableMapping::KEYTYPE_FOREIGN_KEY,
            array("bloom_pd" => "bloom_pd_id"),
            new BloomPeriodDao()
        );
        $translator->addTableMapping(
            "flowerColor", 
            self::FLD_NAME_FLOWERCOL_ID, 
            self::TBL_NAME, 
            "i",
            DataTableMapping::KEYTYPE_FOREIGN_KEY,
            array("flower_color" => "flower_color_id"),
            new FlowerColorDao()
        );
        $translator->addTableMapping(
            "description", 
            self::FLD_NAME_PRODUCT_DESC, 
            self::TBL_NAME
        );
        $translator->addTableMapping(
            "height", 
            self::FLD_NAME_HEIGHT, 
            self::TBL_NAME
        );
        $translator->addTableMapping(
            "heightUnit", 
            self::FLD_NAME_HEIGHT_UNIT, 
            self::TBL_NAME
        );
        $translator->addTableMapping(
            "spread", 
            self::FLD_NAME_SPREAD, 
            self::TBL_NAME
        );
        $translator->addTableMapping(
            "spreadUnit", 
            self::FLD_NAME_SPREAD_UNIT, 
            self::TBL_NAME
        );
        $translator->addTableMapping(
            "price", 
            self::FLD_NAME_PRICE, 
            self::TBL_NAME
        );
        $translator->addTableMapping(
            "image", 
            self::FLD_NAME_PRODUCT_IMG, 
            self::TBL_NAME,
            "b"
        );
    }
    
    /**
     * {@inheritDoc}
     * @see \misd\data\Dao::findById()
     */
    public function findById($id)
    {
        $record = parent::findById($id);
        if (!is_null($record))
        {
            return $this->instantiateObject($record);
        }
    }
    
    /**
     * {@inheritDoc}
     * @see \misd\data\Dao::getAll()
     */
    public function findAll()
    {
        $rsProducts = parent::findAll();
        return $this->instantiateObjects($rsProducts);
    }
    
    /**
     * {@inheritDoc}
     * @see \misd\data\Dao::find()
     */
    public function find($queryObject)
    {
        $rsProducts = parent::find($queryObject);
        return $this->instantiateObjects($rsProducts);
    }
}

