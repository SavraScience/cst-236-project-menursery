<?php
namespace inc\data;

require_once ('misd/data/Dao.php');

use misd\data\Dao;
use misd\data\DataTranslator;
use misd\data\DatabaseManager;
use Exception;
use inc\models\VarietyModel;
use misd\data\DataTableMapping;

/**
 *
 * @author djsav
 *        
 */
class VarietyDao extends Dao
{

    // CONSTANTS
    // -- table name
    private const TBL_NAME = 'variety';
    
    // -- field names
    private const FLD_NAME_ID = 'variety_id';
    private const FLD_NAME_VEGTYPE_ID = 'veg_type_id';
    private const FLD_NAME_DESC = 'variety_desc';
    
    /**
     *
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $tableName
     *            The name of the table in
     *            the database that corresponds to the class/entity
     *            you wish to perform CRUD operations on
     */
    public function __construct()
    {
        parent::__construct(self::TBL_NAME, new VarietyModel());
    }
    
    // IMPLEMENTED METHODS
    final function setMapProperties(DataTranslator &$translator)
    {
        // map object/class properties of the model to the database
        $translator = &parent::getDataTranslator();
        $translator->addTableMapping(
            "id", 
            self::FLD_NAME_ID, 
            self::TBL_NAME
        );
        $translator->addTableMapping(
            "description", 
            self::FLD_NAME_DESC, 
            self::TBL_NAME
        );
        $translator->addTableMapping(
            "vegetationType", 
            self::FLD_NAME_VEGTYPE_ID, 
            self::TBL_NAME, 
            "i",
            DataTableMapping::KEYTYPE_FOREIGN_KEY,
            array("veg_type" => "veg_type_id"),
            new VegetationTypeDao()
        );
    }
    
    /**
     * {@inheritDoc}
     * @see \misd\data\Dao::getById()
     */
    public function findById($id)
    {
        $record = parent::findById($id);
        if (!is_null($record))
        {
            return $this->instantiateObject($record);
        }
    }
    
    /**
     * {@inheritDoc}
     * @see \misd\data\Dao::getAll()
     */
    public function findAll()
    {
        $rsVarieties = parent::findAll();
        return $this->instantiateObjects($rsVarieties);
    }
    
    public function findAllByVegetationType($nameOrId)
    {
        // build WHERE clause and parameter binding type
        $whereClause = "WHERE ";
        $paramType = "";
        if (is_int($nameOrId))
        {
            $whereClause .= self::FLD_NAME_VEGTYPE_ID . "?";
            $paramType = "i";
        }
        elseif (is_string($nameOrId))
        {
            $whereClause .= "veg.veg_type_desc = ?";
            $paramType = "s";
        }
        else
        {
            return null;
        }
        
        // build SQL Statement
        $sql = <<<ML
            SELECT *
            FROM variety JOIN veg_type AS veg USING(veg_type_id)
            $whereClause
            ORDER BY variety_desc ASC
            ;
ML;
            
            // debugging
            //console_log("SQL Statement = \n'$sql'");
            //console_log("Value of ? = $nameOrId");
            
            try
            {
                DatabaseManager::persistConnection($this->conn);
                
                // create a prepared statement
                $stmt = $this->conn->prepare($sql);
                $stmt->bind_param($paramType, $nameOrId);
                $stmt->execute();
                
                // check to see if anything was returned
                $result = $stmt->get_result();
                if ($result->num_rows > 0)
                {
                    // records found
                    //console_log("Records found!");
                    $rsVarieties = $result->fetch_all(MYSQLI_ASSOC);
                    return $this->instantiateObjects($rsVarieties);
                }
                else
                {
                    // no records found
                    //console_log("No records found...");
                    return null;
                }
            }
            catch (Exception $ex)
            {
                console_log($ex->getMessage());
            }
            finally
            {
                // close results & statement
                $result->close();
                $stmt->close();
            }
    }
    
    public function find($queryObject)
    {
        $rsVarieties = parent::find($queryObject);
        return $this->instantiateObjects($rsVarieties);
    }
}

