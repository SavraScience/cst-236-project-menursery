<?php
namespace inc\data;

require_once ('misd/data/Dao.php');

use misd\data\Dao;
use misd\data\DataTranslator;
use inc\models\OrderLineItemModel;
use misd\data\DataTableMapping;

class OrderLineItemDao extends Dao
{
    // CONSTANTS
    
    // -- table name
    //const TBL_NAME = 'app_user';
    private const TBL_NAME = "order_line_item";
    
    // -- field names
    private const FLD_NAME_ID = 'line_item_id';
    private const FLD_NAME_ORDER_ID = 'order_id';
    private const FLD_NAME_PRODUCT_ID = 'product_id';
    private const FLD_NAME_QTY = 'qty';
    
    /**
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     */
    public function __construct()
    {
        parent::__construct(self::TBL_NAME, new OrderLineItemModel());
    }

    /**
     * (non-PHPdoc)
     *
     * @see \misd\data\Dao::setMapProperties()
     */
    protected function setMapProperties(DataTranslator &$translator)
    {
        /* ID */
        $translator->addTableMapping(
            "id", 
            self::FLD_NAME_ID, 
            self::TBL_NAME,
            "i",
            DataTableMapping::KEYTYPE_PRIMARY_KEY
        );
        
        /* ORDER ID */
        $translator->addTableMapping(
            "orderId",
            self::FLD_NAME_ORDER_ID,
            self::TBL_NAME,
            "i",
            DataTableMapping::KEYTYPE_FOREIGN_KEY,
            array('order' => 'order_id')
            );
        
        /* PRODUCT ID */
        $translator->addTableMapping(
            "productId",
            self::FLD_NAME_PRODUCT_ID,
            self::TBL_NAME,
            "i",
            DataTableMapping::KEYTYPE_FOREIGN_KEY,
            array('product' => 'product_id')
            );
        
        /* QUANTITY */
        $translator->addTableMapping(
            "quantity",
            self::FLD_NAME_QTY,
            self::TBL_NAME,
            "i"
            );
    }
    
    /**
     * {@inheritDoc}
     * @see \misd\data\Dao::findById()
     */
    public function findById($id)
    {
        $record = parent::findById($id);
        if (!is_null($record))
        {
            return $this->instantiateObject($record);
        }
    }
    
    /**
     * {@inheritDoc}
     * @see \misd\data\Dao::getAll()
     */
    public function findAll()
    {
        $rsLineItems = parent::findAll();
        return $this->instantiateObjects($rsLineItems);
    }
    
    /**
     * {@inheritDoc}
     * @see \misd\data\Dao::find()
     */
    public function find($queryObject)
    {
        $rsLineItems = parent::find($queryObject);
        return $this->instantiateObjects($rsLineItems);
    }
}