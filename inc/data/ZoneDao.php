<?php
namespace inc\data;

require_once ('misd/data/Dao.php');

use inc\models\ZoneModel;
use misd\data\Dao;
use misd\data\DataTranslator;

/**
 *
 * @author djsav
 *        
 */
class ZoneDao extends Dao
{
    // CONSTANTS
    // -- table name
    private const TBL_NAME = 'zone';
    
    // -- field names
    private const FLD_NAME_ID = 'zone_id';
    private const FLD_NAME_VAL = 'zone_val';
    
    /**
     *
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $tableName The name of the table in
     * the database that corresponds to the class/entity
     * you wish to perform CRUD operations on
     */
    public function __construct()
    {
        parent::__construct(self::TBL_NAME, new ZoneModel());
    }
    
    // IMPLEMENTED METHODS
    final function setMapProperties(DataTranslator &$translator)
    {
        // map object/class properties of the model to the database
        $translator = &parent::getDataTranslator();
        $translator->addTableMapping("id", self::FLD_NAME_ID, self::TBL_NAME);
        $translator->addTableMapping("zone", self::FLD_NAME_VAL, self::TBL_NAME);
    }
    
    /**
     * {@inheritDoc}
     * @see \misd\data\Dao::getById()
     */
    public function findById($id)
    {
        $record = parent::findById($id);
        if (!is_null($record))
        {
            return $this->instantiateObject($record);
        }
    }
    
    /**
     * {@inheritDoc}
     * @see \misd\data\Dao::getAll()
     */
    public function findAll()
    {
        $rsZones = parent::findAll();
        return $this->instantiateObjects($rsZones);
    }
    
    public function find($queryObject)
    {
        $rsZones = parent::find($queryObject);
        return $this->instantiateObjects($rsZones);
    }
}

