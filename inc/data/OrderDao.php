<?php
namespace inc\data;

require_once ('misd/data/Dao.php');

use inc\models\OrderModel;
use misd\data\Dao;
use misd\data\DataTranslator;
use misd\data\DataTableMapping;

class OrderDao extends Dao
{
    // CONSTANTS
    
    // -- table name
    private const TBL_NAME = "`order`";
    
    // -- field names
    private const FLD_NAME_ID = 'order_id';
    private const FLD_NAME_USR_ID = 'user_id';
    private const FLD_NAME_DATE = 'order_date';
    private const FLD_NAME_SUBTOTAL = 'subtotal';
    private const FLD_NAME_SHIPPING = 'shipping';
    private const FLD_NAME_GRANDTOTAL = 'total';
    
    /**
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     */
    public function __construct()
    {
        parent::__construct(self::TBL_NAME, new OrderModel());
    }

    /**
     * (non-PHPdoc)
     *
     * @see \misd\data\Dao::setMapProperties()
     */
    protected function setMapProperties(DataTranslator &$translator)
    {
        /* ID */
        $translator->addTableMapping(
            'id', 
            self::FLD_NAME_ID, 
            self::TBL_NAME,
            'i',
            DataTableMapping::KEYTYPE_PRIMARY_KEY
        );
        
        /* ORDER DATE */
        $translator->addTableMapping(
            'date',
            self::FLD_NAME_DATE,
            self::TBL_NAME,
            's'
        );
        
        /* USER [ID] (FK) */
        $translator->addTableMapping(
            'userId',
            self::FLD_NAME_USR_ID,
            self::TBL_NAME,
            'i',
            DataTableMapping::KEYTYPE_FOREIGN_KEY,
            array('app_user' => 'app_user_id')
        );
        
        /* SUBTOTAL */
        $translator->addTableMapping(
            'subtotal',
            self::FLD_NAME_SUBTOTAL,
            self::TBL_NAME,
            'd'
        );
        
        /* SHIPPING */
        $translator->addTableMapping(
            'shipping',
            self::FLD_NAME_SHIPPING,
            self::TBL_NAME,
            'd'
        );
        
        /* [GRAND] TOTAL */
        $translator->addTableMapping(
            'grandTotal',
            self::FLD_NAME_GRANDTOTAL,
            self::TBL_NAME,
            'd'
        );
    }
    
    /**
     * {@inheritDoc}
     * @see \misd\data\Dao::findById()
     */
    public function findById($id)
    {
        $record = parent::findById($id);
        if (!is_null($record))
        {
            return $this->instantiateObject($record);
        }
    }
    
    /**
     * {@inheritDoc}
     * @see \misd\data\Dao::getAll()
     */
    public function findAll()
    {
        $rsOrders = parent::findAll();
        return $this->instantiateObjects($rsOrders);
    }
    
    /**
     * {@inheritDoc}
     * @see \misd\data\Dao::find()
     */
    public function find($queryObject)
    {
        $rsOrders = parent::find($queryObject);
        return $this->instantiateObjects($rsOrders);
    }
}

