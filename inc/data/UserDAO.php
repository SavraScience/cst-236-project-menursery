<?php

namespace inc\data;

// imports
require_once 'autoloader.php';
require_once 'inc/misc-functions.php';
use inc\models\UserModel;
use misd\data\Dao;
use misd\data\DataTableMapping;
use misd\data\DataTranslator;

/**
 * Data Access Object for the User table
 * @author Michael Mason
 * @copyright 2019 Mason Innovative Software Design
 */
class UserDao extends Dao
{
    // CONSTANTS
    
    // -- table name
    //const TBL_NAME = 'app_user';
    private const TBL_NAME = 'app_user';
    
    // -- field names
    //const FLD_NAME_ID = 'app_user_id';
    private const FLD_NAME_ID = 'app_user_id';
    //const FLD_NAME_DATE_CREATED = 'date_created';
    private const FLD_NAME_DATE_CREATED = 'date_created';
    //const FLD_NAME_ROLE_ID = 'user_role_id';
    private const FLD_NAME_ROLE_ID = 'user_role_id';
    //const FLD_NAME_EMAIL = 'email';
    private const FLD_NAME_EMAIL = 'email';
    //const FLD_NAME_USERNAME = 'username';
    private const FLD_NAME_USERNAME = 'username';
    //const FLD_NAME_PASSWORD = 'pwd';
    private const FLD_NAME_PASSWORD = 'pwd';
    //const FLD_NAME_FIRSTNAME = 'first_name';
    private const FLD_NAME_FIRSTNAME = 'first_name';
    //const FLD_NAME_LASTNAME = 'last_name';
    private const FLD_NAME_LASTNAME = 'last_name';
    
    // CONSTRUCTOR
    function __construct()
    {
        parent::__construct(self::TBL_NAME, new UserModel());
    }
    
    // IMPLEMENTED METHODS
    final function setMapProperties(DataTranslator &$translator)
    {
        // map object/class properties of the model to the database
        $translator = &parent::getDataTranslator();
        $translator->addTableMapping(
            "id", 
            self::FLD_NAME_ID, 
            self::TBL_NAME,
            "i",
            DataTableMapping::KEYTYPE_PRIMARY_KEY
        );
        $translator->addTableMapping(
            "dateCreated", 
            self::FLD_NAME_DATE_CREATED, 
            self::TBL_NAME
        );        
        $translator->addTableMapping(
            "userRole", 
            self::FLD_NAME_ROLE_ID, 
            self::TBL_NAME, 
            "i"
        );
        $translator->addTableMapping(
            "email", 
            self::FLD_NAME_EMAIL, 
            self::TBL_NAME
        );
        $translator->addTableMapping(
            "username", 
            self::FLD_NAME_USERNAME, 
            self::TBL_NAME
        );
        $translator->addTableMapping(
            "password", 
            self::FLD_NAME_PASSWORD, 
            self::TBL_NAME
        );
        $translator->addTableMapping(
            "firstName", 
            self::FLD_NAME_FIRSTNAME, 
            self::TBL_NAME
        );
        $translator->addTableMapping(
            "lastName", 
            self::FLD_NAME_LASTNAME, 
            self::TBL_NAME
        );
    }

    /**
     * {@inheritDoc}
     * @see \misd\data\Dao::findById()
     */
    public function findById($id)
    {
        $record = parent::findById($id);
        if (!is_null($record))
        {
            return $this->instantiateObject($record);
        }
    }
    
    /**
     * {@inheritDoc}
     * @see \misd\data\Dao::findAll()
     */
    public function findAll()
    {
        $rsUsers = parent::findAll();
        return $this->instantiateObjects($rsUsers);
    }
    
    public function find($queryObject)
    {
        $rsUsers = parent::find($queryObject);
        return $this->instantiateObjects($rsUsers);
    }
}

