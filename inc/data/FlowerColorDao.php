<?php
namespace inc\data;

require_once ('misd/data/Dao.php');

use misd\data\Dao;
use misd\data\DataTranslator;
use inc\models\FlowerColorModel;
use misd\data\DataTableMapping;

/**
 *
 * @author djsav
 *        
 */
class FlowerColorDao extends Dao
{
    // CONSTANTS
    // -- table name
    private const TBL_NAME = 'flower_color';
    
    // -- field names
    private const FLD_NAME_ID = 'flower_color_id';
    private const FLD_NAME_DESC = 'color_desc';
    
    /**
     *
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $tableName The name of the table in
     * the database that corresponds to the class/entity
     * you wish to perform CRUD operations on
     */
    public function __construct()
    {
        parent::__construct(self::TBL_NAME, new FlowerColorModel());
    }
    
    // IMPLEMENTED METHODS
    final function setMapProperties(DataTranslator &$translator)
    {
        // map object/class properties of the model to the database
        $translator->addTableMapping(
            "id", 
            self::FLD_NAME_ID, 
            self::TBL_NAME,
            "i",
            DataTableMapping::KEYTYPE_PRIMARY_KEY
        );
        $translator->addTableMapping(
            "color", 
            self::FLD_NAME_DESC, 
            self::TBL_NAME
        );
    }
    
    /**
     * {@inheritDoc}
     * @see \misd\data\Dao::getById()
     */
    public function findById($id)
    {
        $record = parent::findById($id);
        if (!is_null($record))
        {
            return $this->instantiateObject($record);
        }
    }
    
    /**
     * {@inheritDoc}
     * @see \misd\data\Dao::getAll()
     */
    public function findAll()
    {
        $rsFlowerColors = parent::findAll();
        return $this->instantiateObjects($rsFlowerColors);
    }
    
    public function find($queryObject)
    {
        $rsFlowerColors = parent::find($queryObject);
        return $this->instantiateObjects($rsFlowerColors);
    }
}

