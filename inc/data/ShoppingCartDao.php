<?php
namespace inc\data;

require_once ('misd/data/Dao.php');

use misd\data\Dao;
use misd\data\DatabaseManager;
use inc\models\ShoppingCartLineItemModel;
use misd\data\DataTableMapping;
use inc\models\ShoppingCartModel;
use misd\data\DataTranslator;

class ShoppingCartDao extends Dao
{
    // CONSTANTS
    
    // -- table name
    private const TBL_NAME = 'shopping_cart';
    
    // -- field names
    private const FLD_NAME_ID = 'cart_item_id';
    private const FLD_NAME_USR_ID = 'user_id';
    private const FLD_NAME_PRODUCT_ID = 'product_id';
    private const FLD_NAME_QTY = 'qty';
    
    // CONSTRUCTOR
    /**
     *
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $tableName
     *            The name of the table in
     *            the database that corresponds to the class/entity
     *            you wish to perform CRUD operations on
     */
    public function __construct()
    {
        parent::__construct(self::TBL_NAME, new ShoppingCartLineItemModel());
    }

    /**
     * (non-PHPdoc)
     *
     * @see \misd\data\Dao::setMapProperties()
     */
    protected function setMapProperties(DataTranslator &$translator)
    {
        // map object/class properties of the model to the database
        $translator->addTableMapping(
            "id", 
            self::FLD_NAME_ID, 
            self::TBL_NAME,
            "i",
            DataTableMapping::KEYTYPE_PRIMARY_KEY
        );
        $translator->addTableMapping(
            "userId", 
            self::FLD_NAME_USR_ID, 
            self::TBL_NAME,
            "i",
            DataTableMapping::KEYTYPE_FOREIGN_KEY,
            array("app_user" => "app_user_id")
        );
        $translator->addTableMapping(
            "productId", 
            self::FLD_NAME_PRODUCT_ID, 
            self::TBL_NAME,
            "i",
            DataTableMapping::KEYTYPE_FOREIGN_KEY,
            array("product" => "product_id")
        );
        $translator->addTableMapping(
            "quantity", 
            self::FLD_NAME_QTY, 
            self::TBL_NAME
        );
    }
    
    /**
     * {@inheritDoc}
     * @see \misd\data\Dao::findById()
     */
    public function findById($id)
    {
        $record = parent::getById($id);
        if (!is_null($record))
        {
            return $this->instantiateObject($record);
        }
    }
    
    /**
     * {@inheritDoc}
     * @see \misd\data\Dao::findAll()
     */
    public function findAll()
    {
        $rsCartItems = parent::findAll();
        return $this->instantiateObjects($rsCartItems);
    }
    
    public function find($queryObject)
    {
        $rsCartItems = parent::find($queryObject);
        return $this->instantiateObjects($rsCartItems);
    }
    
    /**
     * Returns a Shopping Cart for a particular user
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param int $id The ID of the user for which you want
     * to retrieve the shopping cart for
     */
    public function getCartForUserId(int $id)
    {
        // initialize variables
        $shoppingCart = null;
        
        // build SQL Statement
        $tbl = self::TBL_NAME;
        $fldUserId = self::FLD_NAME_USR_ID;
        $sql = <<<ML
            SELECT *
            FROM $tbl
            WHERE $fldUserId = ?
            ;
ML;
        DatabaseManager::persistConnection($this->conn);
        
        // create & execute a prepared statement
        $stmt = $this->conn->prepare($sql);
        $stmt->bind_param("i", $id);
        $stmt->execute();
        
        $rsCartItems = $stmt->get_result();
        
        if ($rsCartItems !== false)
        {
            $cartItemArray = $this->instantiateObjects($rsCartItems);
            $shoppingCart = new ShoppingCartModel();
            $shoppingCart->setItems($cartItemArray);
        }
        
        $stmt->close();
        
        return $shoppingCart;
    }
    
    public function deleteCartForUserId(int $id) : bool
    {
        $tbl = self::TBL_NAME;
        $fldUserId = self::FLD_NAME_USR_ID;
        $sql = <<<ML
            DELETE
            FROM $tbl
            WHERE $fldUserId = ?
            ;
ML;
        DatabaseManager::persistConnection($this->conn);
        
        // create, execute & close a prepared statement
        $stmt = $this->conn->prepare($sql);
        $stmt->bind_param("i", $id);
        $deleted = $stmt->execute();
        $stmt->close();
        
        return $deleted;
    }
    
    public function updateCartItem(int $userId, int $productId, int $qty)
    {
        // DECLARATIONS
        $procName = "usp_shopping_cart_update";
        
        // build SQL Statement using Stored Procedure
        $sql = "CALL $procName(?, ?, ?, @updated);";
        
        DatabaseManager::persistConnection($this->conn);
        
        // create, execute & close a prepared statement
        $stmt = $this->conn->prepare($sql);
        $stmt->bind_param("iii", $userId, $productId, $qty);
        $updated = $stmt->execute();
        
        /*
        if ($stmt->errno > 0)
        {
            // debugging
            console_log("Error No " . $stmt->errno . " : " . $stmt->error);
        }
        */
        
        $stmt->close();
        
        return $updated;
    }
    
    public function incrementCartItem() : bool
    {
        // DECLARATIONS
        $procName = "usp_shopping_cart_increment_item";
        
        // build SQL Statement using Stored Procedure
        $sql = "CALL $procName(?, ?, @updated);";
        
        DatabaseManager::persistConnection($this->conn);
        
        // create, execute & close a prepared statement
        $stmt = $this->conn->prepare($sql);
        $stmt->bind_param("ii", $userId, $productId);
        
        $updated = $stmt->execute();
        $stmt->close();
        
        // get the result
        $sql = "SELECT @updated";
        
        // create, execute & close a prepared statement
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $stmt->bind_result($updated);
        $stmt->fetch();
        
        $stmt->close();
        
        return $updated;
    }
    
    public function decrementCartItem() : bool
    {
        // DECLARATIONS
        $procName = "usp_shopping_cart_decrement_item";
        
        // build SQL Statement using Stored Procedure
        $sql = "CALL $procName(?, ?, @updated);";
        
        DatabaseManager::persistConnection($this->conn);
        
        // create, execute & close a prepared statement
        $stmt = $this->conn->prepare($sql);
        $stmt->bind_param("ii", $userId, $productId);
        
        $updated = $stmt->execute();
        $stmt->close();
        
        // get the result
        $sql = "SELECT @updated";
        
        // create, execute & close a prepared statement
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $stmt->bind_result($updated);
        $stmt->fetch();
        
        $stmt->close();
        
        return $updated;
    }
}