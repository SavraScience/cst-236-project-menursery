<?php
namespace inc\models;

// imports
use misd\models\AbstractObjectModel;
use misd\security\SecurityService;

class ShoppingCartModel extends AbstractObjectModel
{
    // INSTANCE VARIABLES
    private $userId;
    
    /** @var $items ShoppingCartLineItemModel[] */
    private $items;
    
    // CONSTRUCTOR
    public function __construct($id = null)
    {
        parent::__construct($id);
        
        // initialize variables
        $this->items = array();
        $this->userId = null;
    }
    
    public function size() : int
    {
        return count($this->items);
    }
    
    // PUBLIC ACCESSOR/MUTATOR METHODS
    public function getUserId()
    {
        return $this->userId;
    }
    public function setUserId(int $userId)
    {
        $this->userId = $userId;
        
        // set User ID for each item in cart
        foreach ($this->items as $cartItem)
        {
            $cartItem->setUserId($userId);
        }
    }
    
    // PUBLIC METHODS
    /**
     * Clears all of the items from the shopping cart
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     */
    public function clear()
    {
        $this->items = array();
    }
    
    /**
     * Attempts to retrieve a given item in the shoppping
     * cart's array of items by a product's ID
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param int $productId
     * @return \inc\models\ShoppingCartLineItemModel|NULL
     */
    public function getItem(int $productId)
    {
        // initialize variables
        $returnVal = null;
        
        // attempt to locate the item in the array
        //console_log("Attempting to retrieve product #$productId...");
        if (isset($this->items[$productId]))
        {
            //console_log("Product #$productId found!");
            $returnVal = $this->items[$productId];
        }
        else
        {
            //console_log("Product #$productId not found...");
        }
        
        // debugging
        //console_log("<pre>" . print_r($this->items, true) . "</pre>");
        
        return $returnVal;
    }
    
    /**
     * Returns an array of ShoppingCartItem that constitutes
     * the shopping cart
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return array Array of ShoppingCartItem
     */
    public function getItems() : array
    {
        return $this->items;
    }
    
    /**
     * Sets the array of ShoppingCartItem that constitutes
     * the shopping cart
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param array $shoppingCartItems
     */
    public function setItems(array $shoppingCartItems)
    {
        // clear the current list, if exists
        $this->items = array();        
        
        // add each item in the array to the shopping cart using its own index
        foreach($shoppingCartItems as $cartItem)
        {
            /** @var $cartItem ShoppingCartLineItemModel */
            $this->addItem(
                $cartItem->getProductId(), 
                $cartItem->getQuantity(), 
                $cartItem->getId()
            );
        }
    }
    
    /**
     * Adds a given quantity of a new product to the shopping cart 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param int $productId
     * @param int $qty
     * @param int $cartItemId (Optional)
     * @param int $userId (Optional)
     */
    public function addItem(
        int $productId, 
        int $qty, 
        int $cartItemId = null    )
    {
        // attempt to find the product in the existing shopping cart
        $product = $this->getItem($productId);
        
        if (!is_null($product))
        {
            // existing product found
            // -- set new quantity
            $currQty = $product->getQuantity();
            $product->setQuantity($currQty + $qty);
        }
        else
        {
            // existing product not found
            // -- create a new item and add to the cart
            $newProduct = new ShoppingCartLineItemModel($cartItemId);
            $newProduct->setProductId($productId);
            $newProduct->setQuantity($qty);
            $newProduct->setUserId(SecurityService::getCurrentUser()->getUserId());
            $this->items[$productId] = $newProduct;
        }
    }
    
    /**
     * Removes an item from the shopping cart entirely,
     * regardless of its quantity
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param int $productId
     */
    public function removeItem(int $productId)
    {
        if (isset($this->items[$productId]))
        {
            unset($this->items[$productId]);
        }
    }

    /**
     * Increments a given product's quantity in the shopping cart,
     * if it exists; otherwise, simply adds a quantity of 1 of the
     * given product to the shopping cart
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param int $productId
     */
    public function incrementQuantity(int $productId)
    {
        // attempt to find the product in the existing shopping cart
        $product = &$this->getItem($productId);
        
        if (!is_null($product))
        {
            // existing product found
            // -- set new quantity
            $product->incrementQuantity();
        }
        else
        {
            // existing product not found
            // -- create a new item and add to the cart
            $newProduct = new ShoppingCartLineItemModel();
            $newProduct->setProductId($productId);
            $newProduct->setQuantity(1);
            $newProduct->setUserId(SecurityService::getCurrentUser()->getUserId());
            $this->items[$productId] = $newProduct;
        }
    }
    
    /**
     * Decrements a given product's quantity in the shopping cart,
     * if it exists
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param int $productId
     */
    public function decrementQuantity(int $productId)
    {
        // attempt to find the product in the existing shopping cart
        $product = &$this->getItem($productId);
        
        if (!is_null($product))
        {
            // existing product found
            // -- set new quantity
            $product->decrementQuantity();
            if ($product->getQuantity() == 0)
            {
                // remove the item from the shopping cart
                unset($this->items[$productId]);
            }        
        }
        else
        {
            // existing product not found
            // -- do nothing
        }
    }
    
    /**
     * Resolves an item in the shopping cart given an ID
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param mixed $id
     * @return \inc\models\ShoppingCartLineItemModel|NULL
     */
    public function resolveItem($id)
    {
        // debugging
        //console_log("Comparing ID to " . $this->size() . " items...");
        
        // compare ID to each cart item in the cart
        foreach($this->items as $cartItem)
        {
            // compare actual IDs
            $cartItemId = $cartItem->getId();
            //console_log("Comparing ID '$id' to Cart Item ID '$cartItemId'");
            if ($id === $cartItemId)
            {
                // item found
                return $cartItem;
            }
            else
            {
                // compare temporary IDs
                $cartItemTempId = $cartItem->getTempId();
                //console_log("Comparing ID '$id' to Cart Item temporary ID '$cartItemTempId'");
                if ($id === $cartItemTempId)
                {
                    // item found
                    return $cartItem;
                }
            }
        }
        
        // item could not be found
        //console_log("Returning null");
        return null;
    }
    
    /**
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     */
    public function __toString() : string
    {
        // initialize variables
        $retVal = "<div>Products in Cart<br />" .
                  "----------------</div>\n";
       
       foreach($this->items as $cartItem)
       {
           $productId = $cartItem->getProductId();
           $qty = $cartItem->getQuantity();
           $retVal .= "<span>Product #$productId x $qty</span><br />\n";
       }
       return $retVal;
    }
}

