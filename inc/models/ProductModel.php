<?php
namespace inc\models;

// imports
use misd\models\AbstractObjectModel;


class ProductModel extends AbstractObjectModel
{
    // CONSTANTS
    public const FEET = 1;
    public const INCHES = 2;
    
    // INSTANCE VARIABLES
    private $dateAdded;
    
    /** @var $vegetationType VegetationTypeModel */
    private $vegetationType;
    
    /** @var $subtype SubtypeModel */
    private $subtype;
    
    /** @var $variety VarietyModel */
    private $variety;
    
    /** @var $bloomPeriod BloomPeriodModel */
    private $bloomPeriod;
    
    /** @var $flowerColor FlowerColorModel */
    private $flowerColor;
    
    // TODO: Come back to
    private $zones;
    
    private $description;
    private $height;
    private $heightUnit;
    private $spread;
    private $spreadUnit;
    private $price;
    private $image;
    
    // CONSTRUCTOR
    public function __construct(
        $id = null,
        $vegTypeId = null,
        $subTypeId = null,
        $varietyId = null,
        $bloomPdId = null,
        $flowerColId = null,
        $description = null,
        $height = null,
        $heightUnit = 1,
        $spread = null,
        $spreadUnit = 1,
        $price = null,
        $image = null,
        $zones = null
    )
    {
        parent::__construct($id);
        $this->vegetationType = new VegetationTypeModel($vegTypeId);
        $this->subtype = new SubtypeModel($subTypeId);
        $this->variety = new VarietyModel($varietyId);
        $this->bloomPeriod = new BloomPeriodModel($bloomPdId);
        $this->flowerColor = new FlowerColorModel($flowerColId);
        $this->description = $description;
        $this->height = $height;
        $this->heightUnit = $heightUnit;
        $this->spread = $spread;
        $this->spreadUnit = $spreadUnit;
        $this->price = $price;
        $this->image = $image;
        $this->zones = $zones;
    }
    
    // ACCESSOR/MUTATOR METHODS
    /**
     * @return mixed
     */
    public function getDateAdded()
    {
        return $this->dateAdded;
    }

    /**
     * @return mixed
     */
    public function getVegetationType() : ?VegetationTypeModel
    {
        return $this->vegetationType;
    }

    /**
     * @return mixed
     */
    public function getSubtype() : ?SubtypeModel
    {
        return $this->subtype;
    }

    /**
     * @return mixed
     */
    public function getVariety() : ?VarietyModel
    {
        return $this->variety;
    }

    /**
     * @return mixed
     */
    public function getBloomPeriod() : ?BloomPeriodModel
    {
        return $this->bloomPeriod;
    }

    /**
     * @return mixed
     */
    public function getFlowerColor() : ?FlowerColorModel
    {
        return $this->flowerColor;
    }

    /**
     * @return mixed
     */
    public function getDescription() : ?string
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getHeight() : ?int
    {
        return $this->height;
    }

    /**
     * @return mixed
     */
    public function getHeightUnit()
    {
        $unit = $this->heightUnit;
        if (is_string($unit))
        {
            return $this->resolveUnit($unit);
        }
        return $unit;
    }
    
    /**
     * @return mixed
     */
    public function getSpread() : ?int
    {
        return $this->spread;
    }

    /**
     * @return mixed
     */
    public function getSpreadUnit()
    {
        $unit = $this->spreadUnit;
        if (is_string($unit))
        {
            return $this->resolveUnit($unit);
        }
        return $unit;}
    
    /**
     * @return mixed
     */
    public function getPrice() : ?float
    {
        return $this->price;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $dateAdded
     */
    public function setDateAdded($dateAdded)
    {
        $this->dateAdded = $dateAdded;
    }

    /**
     * @param mixed $vegetationType
     */
    public function setVegetationType(?VegetationTypeModel $vegetationType)
    {
        $this->vegetationType = $vegetationType;
    }

    /**
     * @param mixed $subtype
     */
    public function setSubtype(?SubtypeModel $subtype)
    {
        $this->subtype = $subtype;
    }

    /**
     * @param mixed $variety
     */
    public function setVariety(?VarietyModel $variety)
    {
        $this->variety = $variety;
    }

    /**
     * @param mixed $bloomPeriod
     */
    public function setBloomPeriod(?BloomPeriodModel $bloomPeriod)
    {
        $this->bloomPeriod = $bloomPeriod;
    }

    /**
     * @param mixed $flowerColor
     */
    public function setFlowerColor(?FlowerColorModel $flowerColor)
    {
        $this->flowerColor = $flowerColor;
    }

    /**
     * @param mixed $description
     */
    public function setDescription(?string $description)
    {
        $this->description = $description;
    }

    /**
     * @param mixed $height
     */
    public function setHeight(?int $height)
    {
        $this->height = $height;
    }

    /**
     * @param mixed $heightUnit
     */
    public function setHeightUnit($heightUnit)
    {
        $this->heightUnit = $heightUnit;
    }
    
    /**
     * @param mixed $spread
     */
    public function setSpread(?int $spread)
    {
        $this->spread = $spread;
    }

    /**
     * @param mixed $spreadUnit
     */
    public function setSpreadUnit($spreadUnit)
    {
        $this->spreadUnit = $spreadUnit;
    }
    
    /**
     * @param mixed $price
     */
    public function setPrice(?float $price)
    {
        $this->price = $price;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }
    
    // PRIVATE METHODS
    private function resolveUnit(string $unit) : int
    {
        switch ($unit)
        {
            case "feet":
                return self::FEET;
            case "inches":
                return self::INCHES;
            default:
                return 0;
        }
    }
    
    // OVERRIDDEN METHODS
    public function __toString()
    {
        return (is_null($this->description)) ? "" : $this->description;
    }
}

