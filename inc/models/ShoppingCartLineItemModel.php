<?php
namespace inc\models;

require_once ('misd/models/AbstractObjectModel.php');

use misd\models\AbstractObjectModel;

class ShoppingCartLineItemModel extends AbstractObjectModel
{
    // INSTANCE VARIABLES
    private $userId;
    private $productId;
    private $quantity;
    
    // CONSTRUCTOR
    public function __construct($id = null)
    {
        parent::__construct($id);
        $this->userId = null;
        $this->productId = null;
        $this->quantity = 0;
    }
    
    // PUBLIC ACCESSOR/MUTATOR METHODS
    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @param int|NULL $productId
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    }

    /**
     * @param int|NULL $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }
    
    // PUBLIC METHODS
    /**
     * Increments this product's quantity in the shopping cart
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     */
    public function incrementQuantity() : void
    {
        $this->quantity++;
    }
    
    /**
     * Decrements this product's quantity in the shopping cart
     * 
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     */
    public function decrementQuantity() : void
    {
        if ($this->quantity > 0)
            $this->quantity--;
    }
}

