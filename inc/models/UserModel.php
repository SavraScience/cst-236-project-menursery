<?php
namespace inc\models;

// imports
use misd\models\AbstractObjectModel;

/**
 * 
 * @author Michael Mason
 * @copyright 2019 Mason Innovative Software Design
 */
class UserModel extends AbstractObjectModel
{
    // -- user roles
    const ROLE_SITE_ADMIN = 1;
    const ROLE_USER = 2;
    
    // INSTANCE VARIABLES
    private $dateCreated;
    private $userRole;
    private $email;
    private $username;
    private $password;
    private $firstName;
    private $lastName;
    
    // CONSTRUCTOR
    public function __construct(int $id = null)
    {
        parent::__construct($id);
        
        // set defaults
        $this->userRole = self::ROLE_USER;
    }
    
    // PUBLIC PROPERTIES
    /**
     * @return string The user's e-mail address
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string The user's username
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return string The user's password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return string The user's first name
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @return string The user's last name
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @return int The user's site role. This will either be
     * a site administrator or a regular user (buyer)
     */
    public function getUserRole()
    {
        return $this->userRole;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @param int $role An integer value representing the
     * user's role on the site (Site Administrator or User)
     */
    public function setUserRole($userRole)
    {
        $this->userRole = $userRole;
    }

    /**
     * @param string $email The user's email address
     */
    public function setEmail(string $email)
    {
        // validation
        
        
        $this->email = $email;
    }

    /**
     * @param string $username The user's username
     */
    public function setUsername(string $username)
    {
        // validation
        
        
        $this->username = $username;
    }

    /**
     * @param string $password The user's password
     */
    public function setPassword(string $password)
    {
        // validation
        
        
        $this->password = $password;
    }

    /**
     * @param string $firstName The user's first name
     */
    public function setFirstName(string $firstName)
    {
        // validation
        
        
        $this->firstName = $firstName;
    }

    /**
     * @param string $lastName The user's last name
     */
    public function setLastName(string $lastName)
    {
        // validation
        
        
        $this->lastName = $lastName;
    }
    
    
    // OVERRIDDEN METHODS
    public function __toString()
    {
        return <<< ML
            <p>
            [UserModel class]<br>\n
            ID: $this->id<br>\n
            First Name: $this->firstName<br>\n
            Last Name: $this->lastName<br>\n
            Username: $this->username<br>\n
            Password: $this->password<br>\n
            
            </p>
ML;
    }
    
}

?>