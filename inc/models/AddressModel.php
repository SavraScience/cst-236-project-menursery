<?php
namespace inc\models;

use misd\models\AbstractObjectModel;

class AddressModel extends AbstractObjectModel
{
    // CONSTANTS
    //const SHIPPING_ADDR = 
    
    // INSTANCE VARIABLES
    private $addressType;
    private $addressLine1;
    private $addressLine2;
    private $city;
    private $state;
    private $zip;
    private $default;
    
    // CONSTRUCTOR
    public function __construct($id = null) 
    {
        parent::__construct($id);
    }
    
    // ACCESSOR/MUTATOR METHODS
    /**
     * @return mixed
     */
    public function getAddressType() : string
    {
        return $this->addressType;
    }

    /**
     * @return mixed
     */
    public function getAddressLine1() : string
    {
        return $this->addressLine1;
    }

    /**
     * @return mixed
     */
    public function getAddressLine2() : string
    {
        return $this->addressLine2;
    }

    /**
     * @return mixed
     */
    public function getCity() : string
    {
        return $this->city;
    }

    /**
     * @return mixed
     */
    public function getState() : string
    {
        return $this->state;
    }

    /**
     * @return mixed
     */
    public function getZip() : string
    {
        return $this->zip;
    }

    /**
     * @return mixed
     */
    public function getDefault() : string
    {
        return $this->default;
    }

    /**
     * @param mixed $addressType
     */
    public function setAddressType($addressType)
    {
        $this->addressType = $addressType;
    }

    /**
     * @param mixed $addressLine1
     */
    public function setAddressLine1($addressLine1)
    {
        $this->addressLine1 = $addressLine1;
    }

    /**
     * @param mixed $addressLine2
     */
    public function setAddressLine2($addressLine2)
    {
        $this->addressLine2 = $addressLine2;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @param mixed $zip
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * @param mixed $default
     */
    public function setDefault($default)
    {
        $this->default = $default;
    }
}

