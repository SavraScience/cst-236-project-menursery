<?php
namespace inc\models;

require_once ('misd/models/AbstractObjectModel.php');

use misd\models\AbstractObjectModel;

class OrderLineItemModel extends AbstractObjectModel
{
    // INSTANCE VARIABLES
    private $orderId;
    private $productId;
    private $quantity;
    
    // FULL CONSTRUCTOR
    public function __construct($id = null)
    {
        $varArgs = func_get_args();
        switch(func_num_args())
        {
            case 0:
                
                // empty constructor
                // -- call parent constructor
                parent::__construct(null);
                break;
                
            case 1:
                
                // parent constructor
                parent::__construct($id);
                break;
                
            case 4:
                
                // parent constructor
                parent::__construct($id);
                
                // obtain arguments
                $id = $varArgs[0];
                $orderId = $varArgs[1];
                $productId = $varArgs[2];
                $qty = $varArgs[3];
                
                // constructor
                self::__construct4(
                    $id, 
                    $orderId, 
                    $productId, 
                    $qty
                );
                break;
        }
    }
    
    private function __construct4(
        int $id, 
        int $orderId, 
        int $productId, 
        int $qty
    )
    {
        parent::__construct($id);
        $this->orderId = $orderId;
        $this->productId = $productId;
        $this->quantity = $qty;
    }
    
    // PUBLIC ACCESSOR/MUTATOR METHODS
    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @param int|NULL $productId
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    }

    /**
     * @param int|NULL $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }
}

