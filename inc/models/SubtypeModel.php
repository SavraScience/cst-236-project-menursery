<?php
namespace inc\models;

require_once ('misd/models/AbstractObjectModel.php');

use misd\models\AbstractObjectModel;

class SubtypeModel extends AbstractObjectModel
{    
    // INSTANCE VARIABLES
    
    /** @var $vegetationType VegetationTypeModel */
    private $vegetationType;
    private $description;

    // CONSTRUCTOR
    public function __construct($id = null, $vegTypeId = null)
    {
        parent::__construct($id);
        $this->vegetationType = new VegetationTypeModel($vegTypeId);
        $this->description = null;
    }
    
    // ACCESSOR/MUTATOR METHODS
    /**
     * @return \inc\models\VegetationTypeModel
     */
    public function getVegetationType() : ?VegetationTypeModel
    {
        return $this->vegetationType;
    }

    /**
     * @return mixed
     */
    public function getDescription() : ?string
    {
        return $this->description;
    }

    /**
     * @param \inc\models\VegetationTypeModel $vegetationType
     */
    public function setVegetationType(VegetationTypeModel $vegetationType)
    {
        $this->vegetationType = $vegetationType;
    }

    /**
     * @param mixed $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }
    
    // STATIC METHODS
    public static function compare(AbstractObjectModel $subtype1, AbstractObjectModel $subtype2) : bool
    {
        //console_log("Comparing using SubtypeModel::compare()...");
        if ($subtype1 instanceof SubtypeModel && $subtype2 instanceof SubtypeModel)
        {
            /** @param $subtype1 \SubtypeModel */ /** @param $subtype2 \SubtypeModel */
            /*
            if (parent::compare($subtype1, $subtype2) || 
                (VegetationTypeModel::compare($subtype1->vegetationType, $subtype2->vegetationType) && 
                $subtype1->description == $subtype2->description)    
               ) return true;
            */
            if (parent::compare($subtype1, $subtype2) || $subtype1->description == $subtype2->description)
                return true;
        }
        return false;
    }
    
    // OVERRIDDEN METHODS
    public function __toString() : string
    {
        return (is_null($this->description)) ? "" : $this->description;
    }
}

