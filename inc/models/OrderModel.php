<?php
namespace inc\models;

require_once ('misd/models/AbstractObjectModel.php');

use misd\models\AbstractObjectModel;

class OrderModel extends AbstractObjectModel
{
    // INSTANCE VARIABLES
    private $date;
    /** @var $items OrderLineItemModel[] */
    private $items;
    private $size;
    private $subtotal;
    private $shipping;
    private $grandTotal;
    private $userId;
    
    // CONSTRUCTOR
    public function __construct($id = null)
    {
        parent::__construct($id);
        
        // initialize variables
        $this->date = null;
        $this->items = array();
        $this->size = 0;
        $this->subtotal = 0.0;
        $this->shipping = 0.0;
        $this->grandTotal = 0.0;
    }
    
    // PUBLIC PROPERTIES
    public function setOrderId($orderId)
    {
        $this->id = $orderId;
        
        // update all order line items
        foreach ($this->items as $lineItem)
        {
            $lineItem->setOrderId($orderId);
        }
    }
    
    /**
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return int
     */
    public function size() : int
    {
        /*
        if (!is_null($this->items))
        {
            return count($this->items);
        }
        */
        return $this->size;
    }
    
    /**
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param mixed $orderDate
     */
    public function setDate($orderDate)
    {
        $this->date = $orderDate;
    }
    
    /**
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }
    
    /**
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param OrderLineItemModel[] $items
     */
    public function setItems(array $items)
    {
        $this->items = $items;
        $this->size = count($this->items);
    }
    
    /**
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return OrderLineItemModel[]
     */
    public function getItems() : array
    {
        return $this->items;
    }
    
    /**
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param float $subtotal
     */
    public function setSubtotal(float $subtotal)
    {
        $this->subtotal = $subtotal;
    }
    
    /**
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return float
     */
    public function getSubtotal() : float
    {
        return $this->subtotal;
    }
    
    /**
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param float $shipping
     */
    public function setShipping(float $shipping)
    {
        $this->shipping = $shipping;
    }
    
    /**
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return float
     */
    public function getShipping() : float
    {
        return $this->shipping;   
    }
    
    /**
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param float $grandTotal
     */
    public function setGrandTotal(float $grandTotal)
    {
        $this->grandTotal = $grandTotal;
    }
    
    /**
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return float
     */
    public function getGrandTotal() : float
    {
        return $this->grandTotal;
    }
    
    /**
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param int $userId
     */
    public function setUserId(int $userId)
    {
        $this->userId = $userId;
    }
    
    /**
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @return int
     */
    public function getUserId() : int
    {
        return $this->userId;
    }
    
    // PUBLIC METHODS
    /**
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param OrderLineItemModel $item
     */
    public function addItem(OrderLineItemModel $item)
    {        
        $this->items[$item->getProductId()] = $item;
        $this->size++;
    }
}

