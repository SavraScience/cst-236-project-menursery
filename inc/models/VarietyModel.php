<?php
namespace inc\models;

require_once ('misd/models/AbstractObjectModel.php');

use misd\models\AbstractObjectModel;

/**
 *
 * @author djsav
 *        
 */
class VarietyModel extends AbstractObjectModel
{
    // INSTANCE VARIABLES
    
    /** @var $vegetationType VegetationTypeModel */
    private $vegetationType;
    private $description;
    
    // CONSTRUCTOR
    public function __construct($id = null, $vegTypeId = null)
    {
        parent::__construct($id);
        $this->vegetationType = new VegetationTypeModel($vegTypeId);
        $this->description = null;
    }
    
    // ACCESOR/MUTATOR METHODS
    /**
     * @return \inc\models\VegetationTypeModel
     */
    public function getVegetationType() : ?VegetationTypeModel
    {
        return $this->vegetationType;
    }

    /**
     * @return mixed
     */
    public function getDescription() : ?string
    {
        return $this->description;
    }

    /**
     * @param \inc\models\VegetationTypeModel $vegetationType
     */
    public function setVegetationType(VegetationTypeModel $vegetationType)
    {
        $this->vegetationType = $vegetationType;
    }

    /**
     * @param mixed $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    // STATIC METHODS
    public static function compare(AbstractObjectModel $variety1, AbstractObjectModel $variety2) : bool
    {
        //console_log("Comparing using VarietyModel::compare()...");
        if ($variety1 instanceof VarietyModel && $variety2 instanceof VarietyModel)
        {
            /** @param $variety1 VarietyModel */ /** @param $variety2 VarietyModel */
            /*
            if (parent::compare($variety1, $variety2) ||
                (VegetationTypeModel::compare($variety1->vegetationType, $variety2->vegetationType) &&
                    $variety1->description == $variety2->description)
                )
                return true;
            */
            if (parent::compare($variety1, $variety2) || $variety1->description == $variety2->description)
                return true;
        }
        return false;
    }
    
    // OVERRIDDEN METHODS
    public function __toString() : string
    {
        return (is_null($this->description)) ? "" : $this->description;
    }
}

