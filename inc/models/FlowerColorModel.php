<?php
namespace inc\models;

require_once ('misd/models/AbstractObjectModel.php');

use misd\models\AbstractObjectModel;

class FlowerColorModel extends AbstractObjectModel
{
    // INSTANCE VARIABLES
    private $color;
    
    // CONSTRUCTOR
    public function __construct($id = null)
    {
        parent::__construct($id);
    }
    
    // ACCESSOR/MUTATOR METHODS
    /**
     * @return mixed
     */
    public function getColor() : string
    {
        return $this->color;
    }

    /**
     * @param mixed $color
     */
    public function setColor(string $color)
    {
        $this->color = $color;
    }

    // STATIC METHODS
    public static function compare(AbstractObjectModel $color1, AbstractObjectModel $color2) : bool
    {
        //console_log("Comparing using FlowerColorModel::compare()...");
        if ($color1 instanceof FlowerColorModel && $color2 instanceof FlowerColorModel)
        {
            /** @param $color1 \FlowerColorModel */ /** @param $color2 \FlowerColorModel */
            if (parent::compare($color1, $color2) || $color1->color == $color2->color)
                return true;
        }
        return false;
    }
    
    // OVERRIDDEN METHODS
    public function __toString() : string
    {
        return (is_null($this->color)) ? "" : $this->color;
    }
}

