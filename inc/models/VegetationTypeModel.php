<?php
namespace inc\models;

use misd\models\AbstractObjectModel;

class VegetationTypeModel extends AbstractObjectModel
{
    // INSTANCE VARIABLES
    private $description;
    
    // CONSTRUCTOR
    public function __construct($id = null)
    {
        parent::__construct($id);
    }
    
    // ACCESSOR/MUTATOR METHODS
    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    // STATIC FUNCTIONS
    public static function compare(AbstractObjectModel $vegType1, AbstractObjectModel $vegType2) : bool
    {
        //console_log("Comparing using VegetationTypeModel::compare()...");
        if ($vegType1 instanceof VegetationTypeModel && $vegType2 instanceof VegetationTypeModel)
        {
            /** @param $vegType1 \VegetationTypeModel */ /** @param $vegType2 \VegetationTypeModel */
            if (parent::compare($vegType1, $vegType2) || $vegType1->description == $vegType2->description)
                return true;
        }
        return false;
    }
    
    // OVERRIDDEN METHODS
    public function __toString() : string
    {
        return (is_null($this->description)) ? "" : $this->description;
    }
}

