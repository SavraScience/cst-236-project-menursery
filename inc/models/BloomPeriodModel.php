<?php
namespace inc\models;

require_once ('misd/models/AbstractObjectModel.php');

use misd\models\AbstractObjectModel;

class BloomPeriodModel extends AbstractObjectModel
{
    // INSTANCE VARIABLES
    private $description;
    
    // CONSTRUCTOR
    public function __construct($id = null)
    {
        parent::__construct($id);
    }
    
    // ACCESSOR/MUTATOR METHODS
    /**
     * @return mixed
     */
    public function getDescription() : string
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    // STATIC METHODS
    public static function compare(AbstractObjectModel $bloom1, AbstractObjectModel $bloom2) : bool
    {
        //console_log("Comparing using BloomPeriodModel::compare()...");
        if ($bloom1 instanceof BloomPeriodModel && $bloom2 instanceof BloomPeriodModel)
        {
            /** @param $bloom1 \BloomPeriodModel */  /** @param $bloom2 \BloomPeriodModel */
            if (parent::compare($bloom1, $bloom2) || $bloom1->description == $bloom2->description)
                return true;
        }
        return false;
    }
    
    // OVERRIDDEN METHODS
    public function __toString() : string
    {
        return (is_null($this->description)) ? "" : $this->description;
    }
}

