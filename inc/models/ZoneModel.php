<?php
namespace inc\models;

require_once ('misd/models/AbstractObjectModel.php');

use misd\models\AbstractObjectModel;

class ZoneModel extends AbstractObjectModel
{
    // INSTANCE VARIABLES
    private $zone;
    
    // CONSTRUCTOR
    public function __construct($id = null)
    {
        parent::__construct($id);
    }
    
    // ACCESSOR/MUTATOR METHODS
    /**
     * @return mixed
     */
    public function getZone() : int
    {
        return $this->zone;
    }

    /**
     * @param mixed $zone
     */
    public function setZone(int $zone)
    {
        $this->zone = $zone;
    }

    // STATIC METHODS
    public static function compare(AbstractObjectModel $zone1, AbstractObjectModel $zone2) : bool
    {
        //console_log("Comparing using ZoneModel::compare()...");
        if ($zone1 instanceof ZoneModel && $zone2 instanceof ZoneModel)
        {
            /** @param $zone1 \ZoneModel */ /** @param $zone2 \ZoneModel */
            if (parent::compare($zone1, $zone2) || $zone1->zone == $zone2->zone)
                return true;
        }
        return false;
    }
    
    // OVERRIDDEN METHODS
    public function __toString() : string
    {
        return (is_null($this->zone)) ? "" : $this->zone;
    }
}

