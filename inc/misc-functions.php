<?php

    function array_toString(array $objects)
    {
        $toString = "";
        if (is_array($objects))
        {
            foreach ($objects as $key => $val)
            {
                $type = gettype($val);
                $toString .= "['$key'] => ($type)";
                
                if ($type != "array")
                {
                    $toString .= " $val \n";
                }
                else
                {
                    $toString .= "\n" . array_toString($val) . "\n";
                }
            }
            return $toString;
        }
        else
        {
            return null;
        }
    }
    
    /**
     * Prints a JavaScript console.log($output) to the page,
     * which effectively logs the outpout to the browser window. 
     * 
     * Source: https://stackify.com/how-to-log-to-console-in-php/
     * 
     * @author Kim Sia
     * @param string $output
     * @param boolean $with_script_tags
     */
    function console_log(string $output, bool $with_script_tags = true) {
        $js_code = 'console.log(' . json_encode($output, JSON_HEX_TAG) .
        ');';
        if ($with_script_tags) {
            $js_code = '<script>' . $js_code . '</script>';
        }
        echo $js_code;
    }
    
    /**
     * Replaces Windows backslashes with webserver-safe
     * forward-slashes for path resolution
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $path
     * @return mixed
     */
    function switchSlashes(string $path)
    {
        return str_replace('\\', '/', $path);
    }
    
    /**
     * Tests to see if an object can be converted to a string
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param mixed $var The object or variable you want to test
     * @return bool
     */
    function stringConvertable($var) : bool
    {
        if (is_scalar($var))
        {
            return true;
        }
        elseif (is_array($var))
        {
            return false;
        }
        elseif (is_object($var))
        {
            return method_exists($var, '__toString');
        }
        else
        {
            return false;
        }
    }
    
    /**
     * Utilizes PHP cURL library to retrieve file data from a URL.
     * This can then be used in conjunction with file_put_contents
     * to copy a file from the internet to the local server.
     * 
     * Source: https://www.geeksforgeeks.org/saving-an-image-from-url-in-php/
     * 
     * @author feduser
     * @param string $url The URL of the resource you want to copy locally
     * @return mixed
     */
    function file_get_contents_curl(string $url)
    {
        console_log("Attempting to retrieve data stream from URL: '$url'");
        
        // intialize cURL and set options
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_URL, $url);
        
        // retrieve the data
        $data = curl_exec($ch);
        
        // debugging
        $result = curl_getinfo($ch);
        foreach ($result as $key => $msg)
        {
            if (!is_null($msg))
            {
                console_log("[$key] => $msg");
            }
            
        }
        
        // close the cURL channel
        curl_close($ch);
        
        return $data;
    }
    
    /**
     * 
     * Partial Code Source: https://thisinterestsme.com/downloading-files-curl-php/
     * Partial Code Author: thisinterestsmeblog@gmail.com
     *  
     * @author Michael Mason
     * @copyright 2019 Mason Innovative Software Design
     * @param string $url
     * @param string $saveToPath
     * @throws Exception
     * @return string|NULL
     */
    function save_external_file_curl(string $url, string $saveToPath = null)
    {
        console_log("Attempting to retrieve data stream from URL: '$url'");
        
        // get URL file metadata
        $fileInfo = pathinfo($url);
        if (isset($fileInfo['extension']))
        {
            // determine source file'e extension
            $ext = $fileInfo['extension'];
            console_log("Extension for image = $ext");
            
            // determine destination filepath
            if (is_null($saveToPath)) $saveToPath = "temp.$ext";
            
            // initialize the file handler
            $fileIo = fopen($saveToPath, 'w+');
            
            if ($fileIo != false)
            {
                // intialize cURL and set options
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_TIMEOUT, 20); // timeout after 20 seconds
                curl_setopt($ch, CURLOPT_FILE, $fileIo);
                
                //curl_setopt($ch, CURLOPT_HEADER, FALSE);
                //curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                //curl_setopt($ch, CURLOPT_URL, $url);
                
                // retrieve the data
                curl_exec($ch);
                
                // check the HTTP status code
                $result = curl_getinfo($ch);
                $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                
                // debugging
                foreach ($result as $key => $msg)
                {
                    if (!is_null($msg))
                    {
                        console_log("[$key] => $msg");
                    }
                }
                 
                if ($statusCode == 200)
                {
                    console_log("File downloaded...");
                    return $saveToPath;
                }
                else
                {
                    console_log("Something went wrong while attempting to download the file..." .
                        "\nStatus Code: $statusCode");
                    return null;
                }
                
                // check for errors
                if (curl_errno($ch))
                {
                    throw new Exception(curl_error($ch));
                }
                
                // close the cURL channel
                curl_close($ch);
                
                // close the file handler
                fclose($fileIo);
            }
            else
            {
                throw new Exception("Error instantiating file handler... Aborting save...");
                console_log("Error instantiating file handler... Aborting save...");
            }
        }
        else
        {
            throw new Exception("Could not determine source file extension... Aborting save...");
            console_log("Could not determine source file extension! Aborting save...");
        }
    }
    
    function isValidImageType($pathOrExtension) : bool
    {
        // TODO: Implement!
        
        // -- if begins with period and it is the only period
        
        // else get file extension
        
        return true;
    }
?>